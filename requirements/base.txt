Django==4.2.11
# Configuration
django-configurations==2.4.1
django-secure
django-cache-url==3.4.4
dj-database-url==2.1.0
django-environ==0.11.2
whitenoise==6.5.0
django-enumfields==2.1.1

# Forms
django-braces==1.15.0
crispy-bootstrap3
django-crispy-forms==2.0
django-floppyforms==1.9.0

# Models
django-model-utils==4.3.1 # 4.2.0 supports Django 3.1 and 3.2

# Images
Pillow # wagtail 2.8.1 has requirement Pillow<7.0.0,>=4.0.0  https://python-pillow.org/

# For user registration, either via email or social
# Well-built with regular release cycles!
django-allauth==0.57.0 # 0.56 dropped support for Django 3.1

# For the persistance stores
psycopg2-binary==2.9.8

# Unicode slugification
unicode-slugify==0.1.5
django-autoslug==1.9.9

# Time zones support
pytz==2023.3.post1

# Redis support
django-redis==5.3.0
redis==5.0.1

# Useful things
django-avatar==7.1.1 # django-avatar 7.1.1 depends on Pillow>=8.4.0

# Git revision
dealer==2.1.0

# Wagtail
django-compressor==4.4
wagtail>=5.1,<5.2
wagtailnews==4.0.1
django-sendfile2==0.7.0 # required by wagtail SendFileView to delegate file serving to nginx

# Your custom requirements go here
django-tinymce==3.6.1
sorl-thumbnail==12.9.0
django-cities-light==3.9.2
django-autocomplete-light==3.9.7
django-filter
django-haystack[elasticsearch]==3.2.1
lxml==4.9.3
git+https://github.com/bletourmy/django-terms.git@django-4.0-support#egg=django-terms
django-robots==6.1
# urllib3==2.1.0 # py2neo 4.3.0 has requirement urllib3<1.25,>=1.23
# py2neo reached end of life and has been removed from pypi - so temporarily get it from original repository
git+https://github.com/neo4j-contrib/py2neo.git@2021.2.3#egg=py2neo # https://github.com/neo4j-contrib/py2neo
simplejson==3.19.1
django-extensions==3.2.3
jupyterlab==4.2.3
django-simple-captcha==0.5.18
XlsxWriter
djangorestframework==3.14.0
markdown==3.4.4       # Markdown support for the browsable API.
django-cors-headers==4.2.0
openpyxl==3.1.2
rdflib==7.0.0
SPARQLWrapper==2.0.0
#llama support for ontology extraction using llm
llama-index
html2text
# ontogpt
