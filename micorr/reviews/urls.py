from rest_framework import routers
from django.urls import path, include, re_path

from .views import CannedTextTypeViewSet, CannedTextViewSet, ReviewLogViewSet, EditorReviewViewSet, PeerReviewViewSet, \
    PublicationViewSet, UserViewSet, NgReviewsAppView

router = routers.DefaultRouter()

app_name = "reviews"

router.register(r'canned_texts', CannedTextTypeViewSet)
router.register(r'canned_text_types', CannedTextViewSet)
router.register(r'review_logs', ReviewLogViewSet)
router.register(r'editor_reviews', EditorReviewViewSet, basename='editorreview')
router.register(r'peer_reviews', PeerReviewViewSet)
router.register(r'publications', PublicationViewSet)
router.register(f'reviewers', UserViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    re_path('^.*', NgReviewsAppView.as_view(), name='app'),
]
