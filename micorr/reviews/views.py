from artefacts.models import Publication
from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.core.mail import EmailMultiAlternatives
from django.db.models import Q
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.filters import BaseFilterBackend
from rest_framework.response import Response
from micorr.users.models import User

from .models import EditorReview, CannedTextType, CannedText, ReviewLog, PeerReview
from .serializers import CannedTextTypeSerializer, CannedTextSerializer, ReviewLogSerializer, EditorReviewSerializer, \
    PeerReviewSerializer, PublicationSerializer, UserSerializer


class IsPeerReviewerFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(groups__name__in=['Peer Reviewers'])


class EditorPermission(permissions.BasePermission):
    """
    to restrict access to EditorReviewViewSet
    Global permission for members of "Main administrator" group.
    and detail view access only for selected peer reviewers and authors.
    """

    def has_permission(self, request, view):
        if request.user and request.user.groups.filter(name='Main administrator').exists():
            return True
        if request.user and request.user.is_authenticated and request.method in ['GET', 'OPTIONS']:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.user and request.user.is_authenticated:
            if request.user.groups.filter(name='Main administrator').exists():
                return True
            if obj.publication.artefact.object.user==request.user:
                return True
            return obj.peer_reviews.filter(user=request.user).exists()
        return False


# Django Rest framework viewsets

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    # queryset = User.objects.filter(groups__name__in=['Peer Reviewers'])
    queryset = User.objects.filter(groups__name__in=['MiCorr Editors'])

    @action(methods=['get'], detail=False)
    def current_user(self, request, *args, **kwargs):
        user = request.user
        serialized_user = UserSerializer(user)
        return Response(serialized_user.data)


class CannedTextTypeViewSet(viewsets.ModelViewSet):
    serializer_class = CannedTextTypeSerializer
    queryset = CannedTextType.objects.all()


class CannedTextViewSet(viewsets.ModelViewSet):
    serializer_class = CannedTextSerializer
    queryset = CannedText.objects.all()


class ReviewLogViewSet(viewsets.ModelViewSet):
    serializer_class = ReviewLogSerializer
    queryset = ReviewLog.objects.all()

    def get_queryset(self):
        user: User = self.request.user
        queryset = ReviewLog.objects.none()
        if user and user.is_authenticated:
            if user.groups.filter(name='Main administrator').exists():
                # "Main administrator" members have access to all editor reviews
                queryset = ReviewLog.objects.all()
            else:
                # returns only editorreview  records related to authenticated user's publications or peer_review
                # and only for single record (self.detail for author-review or editor-review)
                if self.detail:
                    pass
                # in case of list view for author component only (when requested with query_params set)
                else:
                    queryset = ReviewLog.objects.filter((Q(resource_content_type__model='editorreview') & (
                        Q(editor_review__publication__artefact__object__user=user))) | (
                                                            Q(resource_content_type__model='peerreview') & Q(peer_review__user=user)))
        return queryset


    @action(methods=['put'], detail=True)
    def email_to(self, request, *args, **kwargs):
        user = request.user
        object = self.get_object()
        recipient_email = request.data.get('email')
        # only sending email to already registered user
        recipient = User.objects.get(email=recipient_email)
        if object.resource_content_type.model == 'editorreview':
            artefact_name = str(object.resource.publication.artefact)
            review_link = f'/reviews/author-review/{object.resource_id}'
        else:
            artefact_name = str(object.resource.editor_review.publication.artefact)
            review_link = f'/reviews/peer-review/{object.resource_id}'
        review_link = request.build_absolute_uri(review_link)
        context = {'object': object, 'user': user, 'recipient': recipient, 'review_link': review_link, 'artefact_name': artefact_name}
        context.update(self.get_serializer_context())

        text_message = get_template('reviews/email/reviewlog_notify.txt').render(context=context)
        html_message = get_template('reviews/email/reviewlog_notify.html').render(context=context)

        msg = EmailMultiAlternatives(f'MiCorr {artefact_name} review notification', text_message, settings.DEFAULT_FROM_EMAIL,
                                     (recipient.email,), reply_to=(user.email,))
        msg.attach_alternative(html_message, "text/html")
        msg.send()

        return Response({})


class EditorReviewViewSet(viewsets.ModelViewSet):
    serializer_class = EditorReviewSerializer
    permission_classes = [EditorPermission]
    filterset_fields = ['publication', 'publication__artefact__object__user']

    def get_queryset(self):
        user = self.request.user
        queryset = EditorReview.objects.none()
        if user and user.is_authenticated:
            if user.groups.filter(name='Main administrator').exists():
                # "Main administrator" members have access to all editor reviews
                queryset = EditorReview.objects.all()
            else:
                # returns only editorreview  records related to authenticated user's publications or peer_review
                # and only for single record (self.detail for author-review or editor-review)
                if self.detail:
                    pk = int(self.kwargs.get('pk'))
                    queryset = EditorReview.objects.filter(
                        Q(pk=pk) & (Q(publication__artefact__object__user=user) | Q(peer_reviews__user=user)))
                # in case of list view for author component only (when requested with query_params set)
                elif 'publication__artefact__object__user' in self.request.query_params:
                    queryset = EditorReview.objects.filter(publication__artefact__object__user=user)
        return queryset

class PeerReviewViewSet(viewsets.ModelViewSet):
    serializer_class = PeerReviewSerializer
    queryset = PeerReview.objects.all()
    filterset_fields = ['editor_review', 'user', 'selected_for_author']

    def get_queryset(self):
        user = self.request.user
        queryset = PeerReview.objects.none()
        if user and user.is_authenticated:
            if user.groups.filter(name='Main administrator').exists():
                # "Main administrator" members have access to all peer reviews
                queryset = PeerReview.objects.all()
            else:
                # returns the list of user's own peer review only if is member of Peer Reviewers group
                if user.groups.filter(name='Peer Reviewers').exists():
                    queryset = user.peerreview_set.all()
                # In order to give access to review reports we also need to add:
                queryset |= PeerReview.objects.filter(selected_for_author=True, editor_review__publication__artefact__object__user=user)
        return queryset

class PublicationViewSet(viewsets.ModelViewSet):
    serializer_class = PublicationSerializer
    queryset = Publication.objects.all()


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('reviews.add_peerreview', raise_exception=True) , name='dispatch')
class NgReviewsAppView(TemplateView):
    template_name = 'reviews/index.html'
