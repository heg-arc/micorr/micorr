# Generated by Django 3.0.14 on 2022-11-25 15:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0003_add_reviewlog_user'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='reviewlog',
            options={'ordering': ['order', '-created']},
        ),
        migrations.AddField(
            model_name='peerreview',
            name='selected_for_author',
            field=models.BooleanField(default=False, help_text='Report selected for sending to author'),
        ),
    ]
