from artefacts.models import Publication, Artefact, Object
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from rest_framework.filters import BaseFilterBackend
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer, ListSerializer
from rest_framework import serializers
from micorr.users.models import User

from .models import EditorReview, CannedTextType, CannedText, ReviewLog, PeerReview


class UserSerializer(ModelSerializer):
    groups = StringRelatedField(many=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'first_name', 'last_name', 'name', 'groups']


class ObjectSerializer(ModelSerializer):
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Object
        fields = ['id', 'name', 'user']


class ArtefactSerializer(ModelSerializer):
    url = serializers.URLField(source='get_absolute_url', read_only=True)
    object = ObjectSerializer(many=False, read_only=True)

    class Meta:
        model = Artefact
        fields = ['id', 'object', 'description', 'url', 'published']


class PublicationSerializer(ModelSerializer):
    artefact = ArtefactSerializer(many=False, read_only=False)
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Publication
        fields = ['id', 'created', 'modified', 'user', 'decision_taken', 'artefact']


class CannedTextTypeSerializer(ModelSerializer):
    class Meta:
        model = CannedTextType
        fields = '__all__'


class CannedTextSerializer(ModelSerializer):
    class Meta:
        model = CannedText
        fields = '__all__'


class ReviewLogFilteredListSerializer(ListSerializer):

    def to_representation(self, data):
        user = self.context['request'].user

        if user and user.is_authenticated:
            if user.groups.filter(name='Main administrator').exists():
                # "Main administrator" members have access to all ReviewLogs
                pass
            else:
                # returns only ReviewLog  records related to authenticated user's publications or peer_review
                # but the ACCEPTED records referencing peer reviewers information

                # always keep first accepted log
                first_accepted = data.filter(status=ReviewLog.Status.REVIEW_ACCEPTED).order_by('created')[:1]
                # keep remaining accepted logs only for peerreview as they contains reviewer contact information
                # which must not be disclosed to Authors
                filtered_accepted = data.filter(resource_content_type__model='peerreview',
                                                status=ReviewLog.Status.REVIEW_ACCEPTED).order_by('created')[1:]
                data = (data.exclude(
                    status=ReviewLog.Status.REVIEW_ACCEPTED) | first_accepted | filtered_accepted).order_by('-created')
                data = data.filter((Q(resource_content_type__model='editorreview') & (
                    Q(editor_review__publication__artefact__object__user=user))) | (
                                       Q(resource_content_type__model='peerreview') & Q(peer_review__user=user)))
        return super(ReviewLogFilteredListSerializer, self).to_representation(data)



class ReviewLogSerializer(ModelSerializer):
    status = serializers.ChoiceField(
        choices=ReviewLog.Status.choices)
    user = UserSerializer(many=False, read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='user')

    class Meta:
        list_serializer_class = ReviewLogFilteredListSerializer
        model = ReviewLog
        fields = '__all__'

    def create(self, validated_data):
        instance = super().create(validated_data)
        if instance.resource_content_type.model == 'editorreview' and instance.status == ReviewLog.Status.PUBLISHED:
            instance.resource.publication.approve()
        return instance



class EditorReviewSerializer(ModelSerializer):
    review_logs = ReviewLogSerializer(many=True, read_only=True)
    user = UserSerializer(many=False, read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='user')
    publication = PublicationSerializer(many=False, read_only=True)
    content_type = serializers.SerializerMethodField()

    class Meta:
        model = EditorReview
        fields = '__all__'

    def get_content_type(self, obj):
        return ContentType.objects.get_for_model(obj).id


class PeerReviewSerializer(ModelSerializer):
    review_logs = ReviewLogSerializer(many=True, read_only=True)
    user = UserSerializer(many=False, read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='user')
    content_type = serializers.SerializerMethodField()
    publication = PublicationSerializer(many=False, read_only=True, source='editor_review.publication')

    class Meta:
        model = PeerReview
        fields = '__all__'

    def get_content_type(self, obj):
        return ContentType.objects.get_for_model(obj).id
