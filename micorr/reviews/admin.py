from django.contrib import admin

from .models import EditorReview, CannedTextType, CannedText, ReviewLog, PeerReview


# Register your models here.

@admin.register(CannedTextType)
class CannedTextTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(CannedText)
class CannedTextAdmin(admin.ModelAdmin):
    pass


@admin.register(ReviewLog)
class ReviewLogAdmin(admin.ModelAdmin):
    pass


@admin.register(EditorReview)
class EditorReviewAdmin(admin.ModelAdmin):
    pass


@admin.register(PeerReview)
class PeerReviewAdmin(admin.ModelAdmin):
    pass

