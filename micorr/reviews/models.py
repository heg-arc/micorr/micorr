from django.conf import settings
from django.db import models
from django.utils.functional import cached_property
from django_extensions.db.models import TimeStampedModel
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation

from artefacts.models import Artefact, Publication

from micorr.reviews import get_reviews_reports_storage_path, get_reviews_artefacts_storage_path


class CannedTextType(TimeStampedModel):
    name = models.TextField(help_text="Type of canned text (rejection motive, correction)")

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class CannedText(TimeStampedModel):
    type = models.ForeignKey(CannedTextType, on_delete=models.deletion.CASCADE)
    content = models.TextField()

    class Meta:
        ordering = ['content']

    def __str__(self):
        return f'{str(self.type)} -> "{self.content[:10]}"'


class ReviewLog(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.deletion.CASCADE, blank=True, null=True,
                      help_text='ReviewLog user')
    resource_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    resource_id = models.PositiveIntegerField()
    resource = GenericForeignKey('resource_content_type', 'resource_id')
    order = models.IntegerField(default=0)
    comment = models.TextField()

    class Status(models.TextChoices):
        SENT = 'SNT', 'Sent'
        RECEIVED = 'RVD', 'Received'
        REVIEW_ACCEPTED = 'ACD', 'Review Accepted'
        REJECTED = 'RJD', 'Rejected'
        COMPLETED = 'CPD', 'Completed'
        CANCELED = 'CCD', 'Canceled'

        MINOR_REVISION_REQUESTED = 'MIN', 'Minor revision'
        MAJOR_REVISION_REQUESTED = 'MAJ', 'Major revision'
        PUBLICATION_APPROVED = 'APD', 'Publication Approved'
        AUTHORIZED = 'AUD', 'Authorized'
        PUBLISHED = 'PUD', 'Published'

    status = models.CharField(max_length=3, default=Status.SENT, choices=Status.choices)

    class Meta:
        ordering = ['order','-created']

    def __str__(self):
        return f"{self.resource} - {self.status}"

    def get_all_logs(self, status=None, action_type=None, order_by='order'):
        if status and not isinstance(status, list):
            status = [status]
        qs = ReviewLog.objects.filter(
            resource_id=self.resource_id,
            resource_content_type=self.resource_content_type).order_by(order_by)
        if status:
            qs = qs.filter(status__in=status)
        if action_type:
            qs = qs.filter(action_type=action_type)
        return qs


class EditorReview(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.deletion.CASCADE, blank=True, null=True,
                             help_text='Editor user')
    publication = models.ForeignKey(Publication, on_delete=models.deletion.CASCADE,
                                    help_text='Publication submitted for review')
    artefact_snapshot = models.FileField(help_text="Artefact printout at time of submission",
                                         upload_to=get_reviews_artefacts_storage_path)

    review_logs = GenericRelation(ReviewLog, content_type_field='resource_content_type',
                                  object_id_field='resource_id', related_query_name='editor_review')

    @cached_property
    def status(self):
        last_log = self.review_logs.last()
        if last_log:
            return last_log.status


class PeerReview(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.deletion.CASCADE, blank=True, null=True,
                             help_text='Reviewer user')
    editor_review = models.ForeignKey(EditorReview, on_delete=models.deletion.CASCADE, related_name='peer_reviews')
    report = models.FileField(help_text="Review report", null=True, blank=True,
                              upload_to=get_reviews_reports_storage_path)
    selected_for_author = models.BooleanField(default=False, help_text='Report selected for sending to author')
    review_logs = GenericRelation(ReviewLog, content_type_field='resource_content_type',
                                  object_id_field='resource_id', related_query_name='peer_review')

    @cached_property
    def status(self):
        last_log = self.review_logs.last()
        if last_log:
            return last_log.status
