def get_reviews_reports_storage_path(instance, filename):
    return '/'.join(['reviews', str(instance.id), 'reports', filename])


def get_reviews_artefacts_storage_path(instance, filename):
    return '/'.join(['reviews', str(instance.id), 'artefacts', filename])
