# -*- coding: utf-8 -*-


from django.urls import  re_path

from . import views

app_name = 'users'

urlpatterns = [
    # URL pattern for the UserListView
    re_path(
        r'^$',
        view=views.UserListView.as_view(),
        name='list'
    ),

    # URL pattern for the UserRedirectView
    re_path(
        r'^~redirect/$',
        view=views.UserRedirectView.as_view(),
        name='redirect'
    ),

    # URL pattern for the UserDetailView
    re_path(
        r'^(?P<username>[\w.@+-]+)/$',
        view=views.UserDetailView.as_view(),
        name='detail'
    ),

    # URL pattern for the UserUpdateView
    re_path(
        r'^~update/$',
        view=views.UserUpdateView.as_view(),
        name='update'
    ),

]
