from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'micorr.users'
    label = 'users'
    verbose_name = "MiCorr Users"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
