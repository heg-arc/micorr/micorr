import base64
import re
from collections import OrderedDict, defaultdict
from contextlib import contextmanager
from itertools import zip_longest

import io
import xlsxwriter
from py2neo import Graph, NodeMatcher
import time
import datetime
from py2neo import Node, Relationship
import uuid
from django.db import transaction as pg_transaction
from django.conf import settings
from artefacts.models import Artefact, Stratigraphy

from statistics import mean
import logging

# Get an instance of a logger
from urllib.request import urlopen

logger = logging.getLogger(__name__)

class Neo4jDAO:
    # Initialisation de la connexion avec le serveur
    def __init__(self):
        neo4j_auth = settings.NEO4J_AUTH.split(':')
        if len(neo4j_auth)!=2:
            raise ValueError('NEO4J_AUTH environment variable expected format user:pass')
        self.url = "%s://%s:%s@%s/db/data" % (
        settings.NEO4J_PROTOCOL, neo4j_auth[0], neo4j_auth[1], settings.NEO4J_HOST)
        self.graph = Graph(self.url)
        self.tx = None

    def begin(self):
        self.tx = self.graph.begin()
        # forcing auto commit
        # self.tx =self.graph
        return self.tx
    # commit des transaction. Si rien n'est precise, autocommit
    def commit(self):
        if self.tx:
            self.tx.commit()
        self.tx = None

    def rollback(self):
        if self.tx:
            self.tx.rollback()
        self.tx = None

    @contextmanager
    def transaction(self, *args, **kwds):
        tx=self.begin()
        try:
            yield tx
        except Exception as e:
            logger.error('Unhandled ({}) in transaction'.format(e), exc_info=True)
            self.rollback()
            raise
        else:
            self.commit()

    def match_node(self,  *labels, **properties):
        """
        replacement for NodeMatcher.match
        using transaction
        :param labels: node labels to match
        :param properties: set of property keys and values to match
        :return: NodeMatch instance
        """
        matcher = NodeMatcher(self.tx)
        # caution NodeMatcher (replacement for v3 NodeSelector and v2 find_one api)
        # is intended to be initialized with a graph object
        # (see https://py2neo.org/v4/matching.html)
        # in our context (py2neo 4.3 and match call) duck typed tx used instead works
        # double check compatibility in case of any change.
        return matcher.match(*labels, **properties)

    def getStratigraphiesByUser(self, user_id, order_by='description'):
        stratigraphies = self.tx.run(
            """MATCH (n:Stratigraphy) WHERE n.user_uid=$user_id RETURN n ORDER BY n.%s
                UNION
                MATCH (n:Stratigraphy)<-[:has_write_access]-(u:User {uid:$user_id})  RETURN n ORDER BY n.%s
            """ % (order_by, order_by),
            user_id=user_id)
        stratigraphies_list = []
        for stratigraphy in stratigraphies:
            if stratigraphy['n']['timestamp']:
                timestamp = datetime.datetime.fromtimestamp(float(stratigraphy['n']['timestamp'])).strftime('%Y-%m-%d %H:%M:%S')
            else:
                timestamp = None
            stratigraphies_list.append(
                {'date': stratigraphy['n']['date'], 'uid': stratigraphy['n']['uid'],
                 'description': stratigraphy['n']['description'],
                 'artefact_uid': stratigraphy['n']['artefact_uid'],
                 'user_uid':stratigraphy['n']['user_uid'],
                 'timestamp': timestamp})
        return stratigraphies_list

    def getStratigraphyUser(self, stratigraphy):
        user_uid = self.tx.evaluate(
            "MATCH (n:Stratigraphy) WHERE n.uid=$stratigraphy RETURN n.user_uid", stratigraphy=stratigraphy)
        if user_uid:
            try:
                user_id = int(user_uid)
            except TypeError:
                user_id = None
        else:
            user_id = None
        return user_uid

    def setStratigraphyUser(self, stratigraphy, user_id):
        n = self.tx.evaluate("MATCH (n:`Stratigraphy`) WHERE n.uid=$stratigraphy RETURN n", stratigraphy=stratigraphy)
        if n is None:
            return
        n['user_uid'] = user_id
        n['creator_uid'] = user_id
        self.tx.push(n)

    def delStratigraphyUser(self, stratigraphy):
        n=self.tx.evaluate("MATCH (n:`Stratigraphy`) WHERE n.uid=$stratigraphy REMOVE n.user_uid RETURN n", stratigraphy=stratigraphy)

    def get_all_shares(self, stratigraphy):
        return self.tx.run("""
            MATCH (s:Stratigraphy {uid:$stratigraphy})<-[:has_write_access]-(user:User)
            RETURN user.uid AS user_id, user.email AS email order by user.datetime""", stratigraphy=stratigraphy).data()

    def get_all_shares(self, stratigraphy):
        return self.tx.run("""
            MATCH (s:Stratigraphy {uid:$stratigraphy})<-[:has_write_access]-(user:User)
            RETURN user.uid AS user_id, user.email AS email order by user.datetime""", stratigraphy=stratigraphy).data()


    def share_stratigraphy(self, user_id, stratigraphy, recipient_user_id = None, recipient_email=None ):
        # share stratigraphy (only if it belongs to user_id)
        if recipient_user_id:
             # with an already registered user
            self.tx.run("""
                MERGE (u:User {uid:$uid})
                ON CREATE SET u.datetime = datetime.transaction(), u.email = $recipient_email
                 WITH u
                MATCH (s:Stratigraphy {uid:$stratigraphy, user_uid:$user_id})
                MERGE (s)<-[:has_write_access]-(u)""", stratigraphy=stratigraphy, uid=recipient_user_id,
                        recipient_email=recipient_email, user_id=user_id)
        elif recipient_email:
            # with a user not yet registered
            self.tx.run("""
                MERGE (u:User {email:$recipient_email})
                ON CREATE SET u.datetime = datetime.transaction()
                 WITH u
                MATCH (s:Stratigraphy {uid:$stratigraphy, user_uid:$user_id})
                MERGE (s)<-[:has_write_access]-(u)""", stratigraphy=stratigraphy, recipient_email=recipient_email)

    def delete_stratigraphy_share(self, user_id, stratigraphy, recipient_user_id):
        # (only stratigraphy belongs to user_id)
        self.tx.run("""
        MATCH (s:Stratigraphy {uid:$stratigraphy, user_uid:$user_id})<-[share:has_write_access]-(user:User {uid:$recipient_user_id})
        DELETE share""", stratigraphy=stratigraphy, recipient_user_id=recipient_user_id, user_id=user_id)


    def updateStratigraphyDescription(self, stratigraphy, description):
        n = self.tx.evaluate("MATCH (n:`Stratigraphy`) WHERE n.uid=$stratigraphy RETURN n", stratigraphy=stratigraphy)
        n['description'] = description
        self.tx.push(n)

    def getStratigraphyElement(self, stratigraphy_uid):
        """
         get main element of first Metal stratum in stratigraphy  (uid:stratigraphy_uid}

        :param stratigraphy_uid
        :return: main element uid/symbol
        """
        return self.tx.evaluate(
            """
                MATCH (sg:Stratigraphy)-[r:POSSESSES]->(st:Strata)-[:IS_CONSTITUTED_BY]->(c:Characteristic)-[:BELONGS_TO]->(f:Family {uid:"natureFamily"}),
                (st)-[:INCLUDES]->(ctn:Container)-->(e:Element),(ctn)-[:BELONGS_TO]->(ctn_f:Family {uid:"mCompositionMainElements"})
                 where c.uid="mCharacteristic" and sg.uid=$uid
                RETURN e.uid as main_element,toInteger(replace(st.uid, (sg.uid+"_Strata"), "")) as strata_num
                order by -strata_num limit 1
            """, uid=stratigraphy_uid)


    # retourne tous les details d'une stratigraphie, caracteristiques, sous-caracteristiques et interfaces
    # @params le nom de la stratigraphie
    # @returns tous les details de la stratigraphie voulue
    def getStratigraphyDetails(self, stratigraphy_uid):
        # on cherche d'abord toutes les strates
        strata_records = self.tx.run(
            """MATCH (sg:Stratigraphy)-[r:POSSESSES]->(st:Strata) where sg.uid=$uid
               RETURN sg.description as description, st.uid as uid,
                toInteger(replace(st.uid, $sg_strata_prefix, "")) as strata_num
                order by strata_num""",
            uid=stratigraphy_uid,sg_strata_prefix= stratigraphy_uid +"_Strata").data()
        # todo optimization something like the following query would be enough
        # stratigraphy_records = self.tx.run(
        # MATCH (sg:Stratigraphy)-[:POSSESSES]->(st:Strata),(st)-[:IS_CONSTITUTED_BY]->(csc)-[b:BELONGS_TO]->(f:Family) where sg.uid=$uid
        # OPTIONAL MATCH (st)-[:HAS_UPPER_INTERFACE]->(ui:Interface)-[:IS_CONSTITUTED_BY]->(uicsc)-[:BELONGS_TO]->(uif:Family)
        # return sg,st,csc,f,ui,uicsc,uif
        # , uid=stratigraphy_uid)
        stratigraphy = {'uid':stratigraphy_uid,
                        'description': strata_records[0]['description'] if len(strata_records) else None,
                        'strata':[]
                        }
        logger.debug (stratigraphy_uid)

        # pour chaque strates on va faire une requete
        for strata in strata_records:
            st = {'name': strata['uid'], 'characteristics': '', 'subcharacteristics': '', 'interfaces': '',
                  'children': '', 'containers': defaultdict(list), 'variables': {}}
            logger.debug ("***" + strata['uid'])

            # Chaque strates a des caracteristiques
            charactList = self.tx.run(
                """MATCH (n:Strata)-[r:IS_CONSTITUTED_BY]->(c:Characteristic)-[b:BELONGS_TO]->(f:Family) where n.uid=$strata_uid
                   RETURN c,f ORDER by c.order,c.uid
                """, strata_uid=strata['uid'])
            logger.debug ("======Characteristic")
            # c.uid as uid, f.uid as family, c.name as real_name, c.visible as visible, c.order as order
            clist = []
            for charact in charactList:
                cp=charact['c']
                fp=charact['f']
                if fp['uid']=='elementFamily':
                        clist.append({'name': cp['uid'], 'family': fp['uid'], 'real_name': cp['name'],
                                  'symbol': cp['symbol'], 'category':cp['category'], 'order':0, 'visible': True})
                else:
                    clist.append({'name': cp['uid'], 'family': fp['uid'], 'real_name': cp['name'],
                              'order': cp['order'], 'visible': cp['visible'], 'color':cp['color']})
                logger.debug ("         " + cp['uid'])

            # Chaque strate a des sous-caracteristiques
            logger.debug ("======subCharacteristic")
            sc_records = self.tx.run(
                """
                MATCH (s:Strata {uid:$strata_uid})-[r:IS_CONSTITUTED_BY]->(sc:SubCharacteristic)<-[:HAS_SPECIALIZATION*1..2]-(:Characteristic)-[:BELONGS_TO]->(f:Family)
                 RETURN sc.uid as name, sc.name as real_name, f.uid as family order by name""",
                strata_uid=strata['uid']).data()
            slist=[]
            name_seen ={}
            for r in sc_records:
                if r['name'] not in name_seen:
                    name_seen[r['name']]=True
                    slist.append(r)


            logger.debug ("======Components")

            def convert_elem_or_cpnd(elem_or_cpnd, amount=None):
                # to be consistent with getAllCharacteristic name/uid renaming scheme
                # and remaining client code we still need to "convert" the original Element and Compound Characteristics
                # before returning to client
                hacked_characteristic = elem_or_cpnd.copy()
                hacked_characteristic['real_name'] = hacked_characteristic['name']
                hacked_characteristic['name'] = hacked_characteristic['uid']
                del hacked_characteristic['uid']
                if amount is not None:
                    hacked_characteristic['amount'] = amount
                return hacked_characteristic

            component_records = self.tx.run(
                """MATCH (sg:Stratigraphy {uid:$stratigraphy_uid})-[POSSESSES]->(s:Strata {uid:$strata_uid})-[r:INCLUDES]->(cpnt:Component)
                    OPTIONAL MATCH (cpnt)-[r_const_or_incl]->(char_or_ctn)-[:BELONGS_TO]->(family:Family)
                    OPTIONAL MATCH (char_or_ctn)-[ce_r:IS_CONSTITUTED_BY]->(elem_or_cpnd:Characteristic)
                   RETURN s.uid,id(cpnt),char_or_ctn, family.uid, ce_r, elem_or_cpnd
                   ORDER BY s.uid, id(cpnt), family.uid, ce_r.order, char_or_ctn.uid
                """, strata_uid=strata['uid'],stratigraphy_uid=stratigraphy_uid)
            secondary_components = [{'characteristics': [], 'subCharacteristics': [], 'containers': defaultdict(list)}]
            for record in component_records:
                # Caution bug fix line below
                # to verify  if the OPTIONAL MATCH char_or_ctn is in a record
                # we must test with get method and a default value such as None
                # testing if record['char_or_ctn'] would fail in case of Container node (that has no properties)
                # so for ex. bool((_14056:Container {})) == False due to the empty dict of property even if an actual
                # Container node is returned in the record and we would then skip actual Container content
                # from elem_or_cpnd
                if record.get('char_or_ctn',None) is not None:
                    if 'SubCharacteristic' in record['char_or_ctn'].labels:
                        secondary_components[0]['subCharacteristics'].append(
                            {'name': record['char_or_ctn']['uid'], 'real_name': record['char_or_ctn']['name']})
                    elif 'Characteristic' in record['char_or_ctn'].labels:
                        secondary_components[0]['characteristics'].append(
                            {'name': record['char_or_ctn']['uid'], 'family': record['family.uid'],
                             'real_name': record['char_or_ctn']['name'],
                             'order': record['char_or_ctn']['order'], 'visible': record['char_or_ctn']['visible']})
                    elif 'Container' in record['char_or_ctn'].labels and record['elem_or_cpnd']:
                        secondary_components[0]['containers'][record['family.uid']].append(
                            convert_elem_or_cpnd(record['elem_or_cpnd'], record['ce_r']['amount'] if 'amount' in record['ce_r'] else None))

                logger.debug("         " + str(record['char_or_ctn']))

            if len(secondary_components[0]['characteristics']) or len(
                secondary_components[0]['subCharacteristics']) or len(secondary_components[0]['containers']):
                st['secondaryComponents'] = secondary_components

            logger.debug ("======Containers")
            container_records = self.tx.run(
                """MATCH (s:Strata { uid:$strata_uid })-[r:INCLUDES]->(c:Container)-[ce_r:IS_CONSTITUTED_BY]->(elem_or_cpnd:Characteristic)
                   MATCH (c)-[:BELONGS_TO]->(f:Family) RETURN s,c,ce_r,elem_or_cpnd,f ORDER BY f.uid, ce_r.order""",
                strata_uid=strata['uid'])
            for r in container_records:
                st['containers'][r['f']['uid']].append(convert_elem_or_cpnd(r['elem_or_cpnd'], r['ce_r']['amount'] if 'amount' in r['ce_r'] else None))

            logger.debug("======Variables")
            variable_records = self.tx.run(
                """MATCH (s:Strata { uid:$strata_uid })-[r:HAS]->(v:Variable)
                   MATCH (v)-[:BELONGS_TO]->(f) RETURN s,v,f ORDER BY f.uid""",
                strata_uid=strata['uid'])
            for r in variable_records:
                st['variables'][r['f']['uid']]=r['v']['value']  # r['v'].copy() => whole dict


            # Chaque strates a des interfaces
            logger.debug("======interface")
            interface_uid = self.tx.evaluate(
                """MATCH (s:Strata)-[r:HAS_UPPER_INTERFACE]->(n:Interface) WHERE s.uid=$strata_uid
                   RETURN n.uid as uid ORDER BY uid""", strata_uid=strata['uid'])

            if interface_uid and len(interface_uid) > 0:
                # caution only one interface per stratum (the UPPER_INTERFACE)
                # plural here is for historical multiple interfaces or was intended for the list of interface's characteristics - todo continue refactor / rename interfaces -> interface in backend and frontend
                st['interfaces'] = {'name': interface_uid}
                st['interfaces']['characteristics'] = self.tx.run(
                    """MATCH (i:Interface)-[r:IS_CONSTITUTED_BY]->(c:Characteristic)-[b:BELONGS_TO]->(f:Family) WHERE i.uid=$interface_uid
                       RETURN c.uid as name, c.name as real_name, f.uid as family ORDER BY name""", interface_uid=interface_uid).data()
            else:
                st['interfaces'] = {}

            st['characteristics'] = clist
            st['subcharacteristics'] = slist


            #Recuperation des enfants

            childList = self.tx.run(
                """MATCH (a:Strata)-[r:IS_PARENT_OF]->(b:Strata) where a.uid=$strata_uid
                   RETURN b.uid as uid ORDER BY uid""", strata_uid=strata['uid'])

            st['children'] = [
                {'name': child['uid'],
                 'characteristics': self.tx.run(
                     """MATCH (n:Strata)-[r:IS_CONSTITUTED_BY]->(c:Characteristic)-[b:BELONGS_TO]->(f:Family) where n.uid=$child_uid
                        RETURN c.uid as name, c.name as real_name, f.uid as family ORDER BY name""", child_uid=child['uid']).data()}
                for child in childList
            ]
            stratigraphy['strata'].append(st)
        return stratigraphy

    # retourne la liste de toutes les caracteristiques, sous-caracteristiques et sous-sous-caracteristiques
    # @params
    # @returns toutes les caracteristiques et sous-caracteristiques de la base
    def getAllCharacteristic(self):
        all_characteristics = self.tx.run("""
         MATCH (f:Family)
         OPTIONAL MATCH (f)<-[:SHOWS]-(fg:FamilyGroup)
         OPTIONAL MATCH (f)-[ilo:IS_LIST_OF]->(lof:Family)
         OPTIONAL MATCH (f)<-[:BELONGS_TO]-(c:Characteristic)
         OPTIONAL MATCH (f)<-[:HAS_FAMILY]-(nf:Nature)
         OPTIONAL MATCH (c)-[:HAS_SPECIALIZATION]->(sc:SubCharacteristic)
         OPTIONAL MATCH (sc)-[:HAS_SPECIALIZATION]->(ssc:SubCharacteristic)
         OPTIONAL MATCH (c)<-[:HAS]-(nc:Nature)
         OPTIONAL MATCH (sc)<-[:HAS]-(nsc:Nature)
         RETURN f,fg,ilo,lof,c,sc,ssc, nc, nsc, nf
         ORDER BY fg.order,f.order, f.name, c.order, sc.name, ssc.name
        """)
        # here we get a flat list of records including all family, Characteristic, SubCharacteristic, SubCharacteristic
        # convert it into hierarchical lists of f -> c -> sc -> ssc, setting field names as expected by api client
        # we use OrderedDicts to build the "lists" and easily create or update list items from redundant elements in records
        family_dic = OrderedDict()
        for record in all_characteristics:
            f_uid = record['f']['uid']
            lof_uid = record['lof']['uid'] if record.get('lof',None) else None
            is_list_of={'family':lof_uid, 'upto':record['ilo']['upto'], 'filter':record['ilo']['filter'], 'amount_unit':record['ilo'].get('amount_unit')} if lof_uid else None
            if f_uid not in family_dic:
                family_dic[f_uid] = record['f'].copy()
                family_dic[f_uid].update({'family': f_uid, 'familyGroup':record['fg'],
                                     'characteristics': OrderedDict(), 'IS_LIST_OF':is_list_of, 'natures':[]})
            if record['nf'] and record['nf']['uid'] not in family_dic[f_uid]['natures']:
                    family_dic[f_uid]['natures'].append(record['nf']['uid'])
            if record['c']:
                c_uid = record['c']['uid']
                if c_uid not in family_dic[f_uid]['characteristics']:
                    c_dic = {'name': c_uid, 'real_name': record['c']['name'], 'subcharacteristics': OrderedDict(),
                             'natures':[]}
                    if f_uid == 'elementFamily':
                        c_dic.update({'symbol': record['c']['symbol'], 'category': record['c']['category'], 'order': 0, 'visible': True})
                    else:
                        c_dic.update({'description': record['c']['description'], 'order': record['c']['order'], 'visible': record['c']['visible'],
                                      'image_url': record['c']['image_url'], 'optgroup': record['c']['optgroup'],
                                      'color':record['c']['color']})
                        if record['c']['code']:
                            c_dic.update({'code': record['c']['code']})
                    family_dic[f_uid]['characteristics'][c_uid] = c_dic
                if record['nc'] and record['nc']['uid'] not in c_dic['natures']:
                    c_dic['natures'].append(record['nc']['uid'])
            if record['sc']:
                sc_uid = record['sc']['uid']
                if sc_uid not in family_dic[f_uid]['characteristics'][c_uid]['subcharacteristics']:
                    family_dic[f_uid]['characteristics'][c_uid]['subcharacteristics'][sc_uid] = \
                                                            {'name': sc_uid,
                                                             'sub_real_name': record['sc']['name'],
                                                             'description': record['sc']['description'],
                                                             'variable': record['sc']['variable'],
                                                             'unit': record['sc']['unit'],
                                                             'subcharacteristics': [],
                                                             'natures':[]
                                                             }
                if record['nsc'] and record['nsc']['uid'] not in family_dic[f_uid]['characteristics'][c_uid]['subcharacteristics'][sc_uid]['natures']:
                    family_dic[f_uid]['characteristics'][c_uid]['subcharacteristics'][sc_uid]['natures'].append(record['nsc']['uid'])

                if record['ssc']:
                    ssc_uid = record['ssc']['uid']
                    family_dic[f_uid]['characteristics'][c_uid]['subcharacteristics'][sc_uid][
                        'subcharacteristics'].append({'name': ssc_uid, 'subsub_real_name': record['ssc']['name']})
        # now convert the dictionaries into lists as expected by api client
        for f_uid, f in list(family_dic.items()):
            for c_uid, c in list(f['characteristics'].items()):
                c['subcharacteristics'] = list(c['subcharacteristics'].values())
            f['characteristics'] = list(f['characteristics'].values())
        return list(family_dic.values())



    # Ajout d'une stratigraphie
    # @params nom de l'artefact et description de la stratigraphie de la stratigraphie
    # @returns true si ajout, false si refus d'ajout
    def addStratigraphy(self, artefact, stratigraphy):
        name = str(uuid.uuid1())
        if self.stratigraphyExists(stratigraphy) > 0:
            insertOk = False
        else:
            insertOk = name
            self.tx.evaluate("CREATE(stratigraphy:Stratigraphy{uid:$stratigraphy_uid, timestamp: $timestamp, date:$date, artefact_uid:$artefact_uid, label:'stratigraphy', description:$description})",
            stratigraphy_uid=name, timestamp=time.time(), date=time.strftime("%Y-%m-%d"), artefact_uid=artefact,description=stratigraphy)
            self.tx.evaluate(
                "MATCH (a:Artefact),(s:Stratigraphy) WHERE a.uid = $artefact AND s.uid=$name CREATE (a)-[:IS_REPRESENTED_BY]->(s)",
                artefact=artefact, name=name)

        return insertOk

    # retourne la liste de tous les artefacts
    # @params
    # @returns liste de tous les artefacts
    def getAllArtefacts(self):
        return self.tx.run("MATCH (p:Artefact) RETURN p.uid AS name order by p.uid").data()

    # retourne la liste de toutes les stratigraphies pour un artefact
    # @params
    # @returns
    def getStratigraphyByArtefact(self, artefact):
        return self.tx.run(
            """MATCH (n:`Artefact`)-[:`IS_REPRESENTED_BY`]->(m) where n.uid=$artefact
             RETURN m.uid as name, m.description as description""", artefact=artefact).data()

    # verifie si une stratigraphie existe ou pas
    # @params nom de la stratigraphie
    # @returns True si existe, False sinon
    def stratigraphyExists(self, stratigraphy):
        nbo = self.tx.evaluate(
            "MATCH (n:`Stratigraphy`) WHERE n.uid=$stratigraphy RETURN count(n) as nbo", stratigraphy=stratigraphy)
        return nbo > 0

    # supprime toutes les strates et sous strates d'une stratigraphie
    # @params nom de la stratigraphie
    # @returns
    def deleteAllStrataFromAStratigraphy(self, stratigraphy):
        # supression des strates enfants, interfaces, components et containers
        self.tx.run("""
            MATCH (sg:Stratigraphy {uid:$stratigraphy})
            OPTIONAL MATCH (sg)-[:POSSESSES]->(parent_st:Strata)-[:IS_PARENT_OF]->(child_st:Strata)
            DETACH DELETE child_st
            WITH sg MATCH (sg)-[:POSSESSES]->(st:Strata)
            OPTIONAL MATCH (st)-[:HAS_UPPER_INTERFACE]->(i:Interface)
            OPTIONAL MATCH (st)-[:INCLUDES]->(c) WHERE c:Component OR c:Container
            OPTIONAL MATCH (c)-[:INCLUDES]->(ctn:Container)
            OPTIONAL MATCH (st)-[:HAS]->(v:Variable)
            DETACH DELETE c,i,st,v,ctn
        """, stratigraphy=stratigraphy)
        # optional "WHERE c:Component OR c:Container" is there to be explicit on types of Node INCLUDED

    # # supprime une stratigraphie
    # @params nom de la stratigraphie
    # @returns
    def deleteStratigraphy(self, stratigraphy):
        self.tx.run(
            "match (a:Artefact)-[i:IS_REPRESENTED_BY]->(s:Stratigraphy) where s.uid = $stratigraphy optional match (s)-[r]->()  delete i, r, s",
            stratigraphy=stratigraphy)
        return {'res': 1}

    # cree une interface
    # @params uid de la strate et uid de l'interface
    # @returns
    def createInterface(self, strata_uid, interface_uid):
        self.tx.run("""
                MATCH(s:Strata {uid:$strata_uid})
                CREATE (s)-[:HAS_UPPER_INTERFACE]->(interface:Interface {uid:$interface_uid,date:$date})
                """, strata_uid=strata_uid, interface_uid=interface_uid, date=time.strftime("%Y-%m-%d") )

    # attache une caracteristique a une interface
    # @params nom de l'interface et nom de la caracteristique
    # @returns
    def attachCharacteristicToInterface(self, interface, characteristic):
        self.tx.run("""MATCH (a:Interface),(b) WHERE a.uid = $interface AND b.uid=$characteristic
                       CREATE (a)-[r:IS_CONSTITUTED_BY]->(b)""", interface=interface, characteristic=characteristic)

    # attache une caracteristique a une strate
    # @params nom de la strate et nom de la caracteristique
    # @returns
    def attachCharacteristicToStrata(self, strata, c_uid, label="Characteristic"):
      self.tx.run("""
      MATCH (s:Strata),(c:%s) WHERE s.uid =$strata AND c.uid=$c_uid
      CREATE (s)-[r:IS_CONSTITUTED_BY]->(c)""" %(label,), strata=strata, c_uid=c_uid, label=label)

    # wip trying to use stratum_node object and subgraph
    # instead of MATCHING again by name but not working properly yet
    def attachCharacteristicToStratumNode(self, stratum_node, c_uid, label="Characteristic"):
        c = Node(label, uid=c_uid)
        stc = Relationship(stratum_node, "IS_CONSTITUTED_BY", c)
        subgraph = stratum_node | stc | c
        self.tx.merge(subgraph, label, "uid")

    # attache une sous-caracteristique a une strate
    # @params nom de la strate et nom de la sous-caracteristique
    # @returns
    def attachSubCharacteristicToStrata(self, strata, sub_characteristic):
        self.attachCharacteristicToStrata(strata, sub_characteristic, "SubCharacteristic")

    # attache une caracteristique ou sous-caracteristique a un node (Strata, Interface, ...)
    # @params nom de la strate etnom de la caracteristique, sous-caracteristique
    # @returns
    def attachCharacteristic(self, node_uid, characteristic_uid):
        self.tx.run("""MATCH (n),(c)
                                WHERE n.uid = $node_uid AND c.uid=$characteristic_uid
                                CREATE (a)-[r:IS_CONSTITUTED_BY]->(b)""",
                                  node_uid=node_uid,characteristic=characteristic_uid)

    # cree une strate
    # @params nom de la strate et nom de la stratigraphie
    # @returns Strata node py2neo object
    def createStrata(self, strata_uid, stratigraphy_uid):
        return self.tx.evaluate("""
               CREATE(strata:Strata{uid:$strata_uid, date:$today, stratigraphy_uid: $stratigraphy_uid})
               WITH strata
               MATCH (stgy:Stratigraphy {uid:$stratigraphy_uid})
               CREATE (stgy)-[:POSSESSES]->(strata) RETURN strata""",
                             strata_uid=strata_uid, today=time.strftime("%Y-%m-%d"), stratigraphy_uid=stratigraphy_uid)

    # cree une strate enfant
    # @params nom de la strate et nom de la strate parent
    # @returns
    def createChildStrata(self, strata, parentstrata):
        self.tx.run("CREATE(strata:Strata{uid:$strata ,date:$today, label:'strata'})",
                       strata=strata, today=time.strftime("%Y-%m-%d"))
        self.tx.run(
            """MATCH (a:Strata),(b:Strata) WHERE a.uid =$strata AND b.uid=$parentstrata
               CREATE (b)-[:IS_PARENT_OF]->(a)""", strata=strata, parentstrata=parentstrata)

    def create_secondary_components(self, stratum_node, components):
        """
        :param stratum_node:
        :param components:
        :return:
        """
        if not len(components):
            return
        for i, component in enumerate(components):
            c = Node("Component", order=i)
            stc = Relationship(stratum_node, "INCLUDES", c)
            self.tx.create(c | stratum_node | stc)

            def find_and_attach_node_to_component(label,key,value,component):
                node = self.match_node(label, **{key: value}).first()
                # alternative syntaxes
                # node = self.match_node(label, uid=value).first()
                # node = self.match_node(label).where("{}={}".format(key, value)).first()
                if node:
                    self.tx.create(Relationship(component, "IS_CONSTITUTED_BY", node))
                else:
                    logger.debug("{}: with {}={} not found".format(label, key, value))

            for characteristic in component['characteristics']:
                find_and_attach_node_to_component('Characteristic','uid', characteristic['name'], c)

            for sub_characteristic in component['subCharacteristics']:
                find_and_attach_node_to_component('SubCharacteristic', 'uid', sub_characteristic['name'], c)

            if len(component['containers']):
                self.create_containers(c, component['containers'])

    def create_containers(self, parent_node, containers):
        """
        :param parent_node: (stratum or component node)
        :param containers: dictionary of Elements/Compound list
        :return:
        """
        if not len(containers):
            return
        for family, elements in list(containers.items()):
            c = Node("Container")
            stc_rel = Relationship(parent_node, "INCLUDES", c)

            cf = self.match_node("Family", uid=family).first()
            if not cf:
                logger.error('missing family "{}" in graph db. elements not saved'.format(family))
                continue
            fc_rel = Relationship(c, "BELONGS_TO", cf)
            logger.debug('{} {} {} {} {}'.format(c, parent_node, stc_rel, cf, fc_rel))
            self.tx.create(c | parent_node | stc_rel | cf | fc_rel)

            if len(elements):
                for i,e in enumerate(elements):
                    element = self.match_node("Characteristic", uid=e['name']).first()
                    if element:
                        is_constituted_by = Relationship(c, "IS_CONSTITUTED_BY", element, order=i)
                        if 'amount' in e:
                            is_constituted_by['amount'] = e['amount']
                        self.tx.create(is_constituted_by)
                    else:
                        logger.debug ("create_containers(): Characteristic :{} not found - not added to container {}".format(e['name'], c))
                        logger.error("Characteristic :{} not found - not added to container {}".format(e['name'], c))

    def create_variables(self, parent_node, variables):
        """
        :param parent_node: (stratum)
        :param variables: dictionary of variables
        :return:
        """
        if not len(variables):
            return
        for uid, value in list(variables.items()):
            v = Node("Variable", value=value)
            pv_rel = Relationship(parent_node, "HAS", v)
            vf = self.match_node("Family", uid=uid).first() or self.match_node("SubCharacteristic", uid=uid).first()
            if not vf:
                logger.error('missing parent node "{}" in graph db. variable not saved'.format(uid))
                continue
            fv_rel = Relationship(v, "BELONGS_TO", vf)
            logger.debug('{} {} {} {} {}'.format(v, parent_node, pv_rel, vf, fv_rel))
            self.tx.create(v | parent_node | pv_rel | vf | fv_rel)

    # retourne le nombre de strates pour une stratigraphie
    # @params nom de la stratigraphie
    # @returns nombre de strates pour cette stratigraphie
    def getNbStratasByStratigraphy(self, stratigraphy):
        return self.tx.evaluate("MATCH (n:Stratigraphy)-[p:POSSESSES]->(s:Strata) where n.uid=$stratigraphy RETURN  count(s) as nb")

    # sauvegarde toutes les strates d'une stratigraphie
    # @params details d'une stratigraphie au format json
    # @returns 1 si ok
    def save(self, data):
        stratigraphy_name = data['stratigraphy']
        logger.debug('save:%s',stratigraphy_name)
        # on supprime entierement toutes les strates de l'ancienne stratigraphie pour en creer une nouvelle
        # on fait ca pour partir sur une base vierge et on reconstruit de graphe a chaque sauvegarde
        self.deleteAllStrataFromAStratigraphy(stratigraphy_name)

        # on parcourt toutes les strates
        for i,stratum in enumerate(data['stratas']):
            stratum_name = "{}_Strata{}".format(stratigraphy_name,i+1)
            stratum_node = self.createStrata(stratum_name, stratigraphy_name)

            # pour chaque strate on attache une caracteristique ou sous caracteristique
            for characteristic in stratum['characteristics']:
                if characteristic:
                    self.attachCharacteristicToStrata(stratum_name, characteristic['name'])
            for sc in stratum['subCharacteristics']:
                if sc:
                    self.attachSubCharacteristicToStrata(stratum_name, sc['name'])

            # pour chaque strate on cree une interface et on y attache des caracteristiques
            interface_name = stratum_name + "_interface" + str(i + 1)
            self.createInterface(stratum_name, interface_name)
            for interface in stratum['interfaces']:
                if len(interface) > 0:
                    self.attachCharacteristicToInterface(interface_name, interface['name'])


            #Pour chaque strate on attache les strates enfants
            for s in stratum['children']:
                if len(s) > 0:
                    child_name = s['name']
                    self.createChildStrata(child_name, stratum_name)

                    for c in s['characteristics']:
                        if c:
                            self.attachCharacteristicToStrata(child_name, c['name'])
                    for sc in s['subCharacteristics']:
                        if sc:
                            self.attachSubCharacteristicToStrata(stratum_name, sc['name'])

            self.create_secondary_components(stratum_node, stratum['secondaryComponents'])
            self.create_containers(stratum_node, stratum['containers'])
            self.create_variables(stratum_node, stratum['variables'])

        return {'res': 1}

    @pg_transaction.atomic
    def find_similar(self, request, source_stratigraphy_data, user_id):
        """
         find similar using gds cypher projection of public stratigraphies
         for strata similarity calculation

        :param source_stratigraphy_data: POST request data including
            client frontend stratigraphy data
        :return: list of similar stratigraphy in format suitable for display by angular ModalShowSimilarStrataCtrl
        """
        start_wait = datetime.datetime.now()
        """
         synchronize access to find_similar due to its neo4j memory and global resources used
         (e.g 'strata' cypher projection and TMP_HAS_WEIGHTED_CHARACTERISTIC relations)
         as we have to use multiple neo4j transactions inside find_similar we use a postgres lock on
        an arbitrary unique record in django db.
        """
        exclusive_access = Stratigraphy.objects.pk=Stratigraphy.objects.select_for_update().order_by('pk').first()
        end_wait = datetime.datetime.now()
        logger.info(f'find_similar waited {end_wait-start_wait} for exclusive access')
        source_uid = source_stratigraphy_data['stratigraphy']
        weights = source_stratigraphy_data['weights']
        search = source_stratigraphy_data['search']
        export = source_stratigraphy_data['export']
        match_strata_by_class = source_stratigraphy_data['match_strata_by_class']

        # verify if stratigraphy is already saved
        if not self.match_node('Stratigraphy', uid=source_uid).exists():
            self.save(source_stratigraphy_data)
        for gds_graph_name in ['fullstrata', 'strata']:
            if self.tx.evaluate("""RETURN  gds.graph.exists($gds_graph_name)""", gds_graph_name=gds_graph_name):
                self.tx.run("CALL gds.graph.drop($gds_graph_name)", gds_graph_name=gds_graph_name)

        if search['private']:
            own_stratigraphies = self.getStratigraphiesByUser(user_id)
            # Add stratigraphy information from django model
            # we may have many Stratigraphy objects in django db (corresponding to multiple versions of an artefact or multiple artefacts) for one actual stratigraphy in neo4j db
            own_stratigraphies_qs = Stratigraphy.objects.filter(uid__in=[s['uid'] for s in own_stratigraphies]).order_by('uid','-modified').distinct('uid').prefetch_related('section__artefact')
        else:
            own_stratigraphies_qs = Stratigraphy.objects.none()
        if search['public']:
            published_stratigraphies_qs = Stratigraphy.objects.exclude(section_id=None).exclude(section__artefact__published=False).prefetch_related('section__artefact').order_by('id')
        else:
            published_stratigraphies_qs = Stratigraphy.objects.none()
        stratigraphy_search_list = [s for s in own_stratigraphies_qs.union(published_stratigraphies_qs) ]
        stratigraphy_search_list_uids = [s.uid for s in stratigraphy_search_list]
        if source_uid not in stratigraphy_search_list_uids:
            stratigraphy_search_list = [Stratigraphy.objects.filter(uid=source_uid).first()] + stratigraphy_search_list
            stratigraphy_uids = [source_uid] + stratigraphy_search_list_uids
        else:
            stratigraphy_uids = stratigraphy_search_list_uids

        stratigraphy_uids_to_objects = {s.uid if s else source_uid: s for s in stratigraphy_search_list}
        nb_tmp_relations_deleted = self.tx.evaluate("""
            MATCH ()-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->()
            DETACH DELETE r
            RETURN count(r)
        """)
        logger.info(f'{nb_tmp_relations_deleted} TMP_HAS_WEIGHTED_CHARACTERISTIC relations deleted')
        nb_tmp_characteristic_deleted = self.tx.evaluate("""
            MATCH (tc:TmpCharacteristic)
            DETACH DELETE tc
            RETURN count(tc)
        """)
        logger.info(f'{nb_tmp_characteristic_deleted} TmpCharacteristic nodes deleted')

        # list of characteristic Family weights for Jaccard weighted similarity score
        # stored in modalShowSimilarStrata.js (see extraction in MetalPAT_similarity.ipynb)
        # values updated by user and sent in source_stratigraphy_data
        list_of_f2w_map = [{'uid': w['uid'], 'weight': w['value'], 'amount':w.get('amount')} for w in weights if w['value']]
        tmp_has_weighted_characteristic = self.tx.run(
            """
                // retrieve filtered characteristic weighted relationships
                UNWIND $list_of_f2w_map as fw
                UNWIND $stratigraphy_uids as suid
                MATCH (s:Stratigraphy {uid:suid})-[:POSSESSES]->(sa:Strata)
                    CALL {
                        WITH sa,fw
                          MATCH (sa)-[r:INCLUDES]->(n:Container)-[:IS_CONSTITUTED_BY]->(c)-[:BELONGS_TO]->(cf:Family {uid:'colourFamily'}), (n)-[:BELONGS_TO]->(f:Family)  WHERE f.uid=fw.uid
                          MATCH (c)-[:IS_IN_CLASS]->(cc:ColourClass)
                          MERGE (cc)-[:COMBINED]->(tc:TmpCharacteristic {uid:f.uid + '_' + cc.uid})<-[:COMBINED]-(f)
                        RETURN sa.uid,fw.uid,tc as c,fw.weight
                        UNION
                        WITH sa,fw
                          MATCH (sa)-[r:INCLUDES]->(n:Container)-[:IS_CONSTITUTED_BY]->(c), (n)-[:BELONGS_TO]->(f:Family)  WHERE fw.amount IS null AND f.uid=fw.uid AND not (c)-[:BELONGS_TO]->(:Family {uid:'colourFamily'})
                          MERGE (c)-[:COMBINED]->(tc:TmpCharacteristic {uid:f.uid + '_' + c.uid})<-[:COMBINED]-(f)
                        RETURN sa.uid,fw.uid,tc as c,fw.weight
                        UNION
                        WITH sa,fw
                         MATCH (sa)-[:IS_CONSTITUTED_BY]->(c), (c)-[:BELONGS_TO]->(f:Family)  WHERE f.uid=fw.uid
                        RETURN sa.uid,fw.uid,c,fw.weight
                        UNION
                        WITH sa,fw
                            MATCH (sa)-[:INCLUDES]->(cpt:Component)-[r:INCLUDES]->(n:Container)-[:IS_CONSTITUTED_BY]->(c), (n)-[:BELONGS_TO]->(f:Family)  WHERE f.uid=fw.uid
                            MERGE (c)-[:COMBINED]->(tc:TmpCharacteristic {uid:f.uid + '_' + c.uid})<-[:COMBINED]-(f)
                        RETURN sa.uid,fw.uid,tc as c,fw.weight
                        UNION
                        WITH sa,fw
                        MATCH (sa)-[:INCLUDES]->(n:Container)-[icb:IS_CONSTITUTED_BY]->(e:Element), (n)-[:BELONGS_TO]->(f:Family {uid:fw.uid})-[:HAS]->(c:VariableClass)
                         WHERE  fw.amount IS NOT null AND icb.amount IS NOT null AND (icb.amount > c.min  AND (c.max IS null OR icb.amount <= c.max))
                         MERGE (e)-[:COMBINED]->(tc:TmpCharacteristic {uid: c.uid + '_' + e.symbol })<-[:COMBINED]-(c)
                        RETURN sa.uid,fw.uid,tc as c,fw.weight
                        UNION
                        WITH sa,fw
                          MATCH (sa)-[:HAS]->(v:Variable)-[:BELONGS_TO]->(f:Family)-[:HAS]->(c:VariableClass)  WHERE f.uid=fw.uid AND v.value IS NOT null AND (v.value > c.min  AND (c.max IS null OR v.value <= c.max))
                        RETURN sa.uid,fw.uid,c,fw.weight
                    }
                MERGE (sa)-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
                ON CREATE
                    SET r.weight = fw.weight
                ON MATCH
                    SET r.weight = fw.weight
                RETURN s.uid, sa.uid, fw.uid, c.uid, fw.weight
            """, stratigraphy_uids=stratigraphy_uids, list_of_f2w_map=list_of_f2w_map)
        # for rel in tmp_has_weighted_characteristic:
        #     if rel['s.uid'] in ['c7ab70aa-25d7-11e6-a0a6-000c29148083', '5e7030dc-25da-11e6-9e7f-000c29148083']:
        #         logger.info(rel)
        logger.info(f'find_similar update tmp_has_weighted_characteristic relations stats: {tmp_has_weighted_characteristic.stats()}')
        nb_tmp_relations=self.tx.evaluate("""
            MATCH ()-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->() RETURN count(r)
        """)
        logger.info(f'{nb_tmp_relations} tmp_has_weighted_characteristic relations merged')
        # we must commit the transaction for gds to see newly created relations
        self.commit()
        self.begin()
        nb_tmp_relations=self.tx.evaluate("""
            MATCH ()-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->() RETURN count(r)
        """)
        logger.info(f'after commit {nb_tmp_relations} tmp_has_weighted_characteristic relations merged')


        # Cypher projection version
        # we create a bipartite graph with Strata and Characteristic "like" nodes
        # linked by TMP_HAS_WEIGHTED_CHARACTERISTIC relationships only

        project_strata = self.tx.run(f"""
        CALL gds.graph.create.cypher('strata',
             //node query:
            "UNWIND {stratigraphy_uids} as suid
              MATCH (s:Stratigraphy {{uid:suid}})-[:POSSESSES]->(sa:Strata)
              RETURN id(sa) AS id, labels(sa) AS labels
              UNION
              MATCH (sa)-[rwc:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
              RETURN distinct id(c) AS id, labels(c) AS labels",
              //relationship query:
              "UNWIND {stratigraphy_uids} as suid
                MATCH (s:Stratigraphy {{uid:suid}})-[:POSSESSES]->(sa:Strata)-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
                RETURN id(sa) AS source, id(c) AS target, type(r) AS type, r.weight AS weight",
              {{validateRelationships:True}})
            YIELD
              graphName AS graph, nodeQuery, nodeCount AS nodes, relationshipCount AS rels""")
        result=project_strata.next()
        logger.info(f'find similar project_strata {result["nodes"]} nodes {result["rels"]} rels created')
        """
             "MATCH (n)-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(m)
               RETURN id(n) AS source, id(m) AS target, type(r) AS type, r.weight AS weight",

              "UNWIND {stratigraphy_uids} as suid
                MATCH (s:Stratigraphy {{uid:suid}})-[:POSSESSES]->(sa:Strata)-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
                RETURN id(sa) AS source, id(c) AS target, type(r) AS type, r.weight AS weight",

        """

        # calculate and fetch the similarity
        similarity_records = self.tx.run("""
        CALL gds.nodeSimilarity.stream('strata',
        {relationshipWeightProperty:'weight', topK:1000})
            YIELD node1, node2, similarity
            RETURN node1, node2, similarity
            ORDER BY similarity DESCENDING
        """)
        # store (the undirected) similarity score in a dict for all selected strata
        strata_similarity = {frozenset({r['node1'], r['node2']}): r['similarity'] for r in similarity_records}

        stratigraphies = self.tx.run(f"""
        UNWIND {stratigraphy_uids} as suid
            MATCH (s:Stratigraphy {{uid:suid}})-[:POSSESSES]->(sa:Strata)
            WITH s, count(sa) as nbsa
        RETURN s.uid as stratigraphy_uid, nbsa as stratum"""
                                     ).data()

        # retrieve list of strata ids for each stratigraphy
        sgy_strata_records = self.tx.run("""
            UNWIND $stratigraphy_uids as suid
                MATCH (s:Stratigraphy {uid:suid})-[:POSSESSES]->(sa:Strata)
                MATCH (sa)-[:IS_CONSTITUTED_BY]->(nc:Characteristic)-[:BELONGS_TO]->(f:Family {uid:'natureFamily'})
                RETURN s.uid as s_uid ,id(sa) as sa_id, sa.uid as sa_uid,toUpper(split(nc.uid,'Char')[0]) as sa_nat,toInteger(split(sa.uid ,'_Strata')[1]) as sa_num
                ORDER by s.uid, sa_num
                """, stratigraphy_uids=stratigraphy_uids)

        # map stratigraphy uid to their strata ids
        sgy_strata_dict = defaultdict(list)
        sgy_strata_uids_dict = defaultdict(list)
        sgy_strata_nat_dict = defaultdict(list)
        for record in sgy_strata_records:
            sgy_strata_dict[record['s_uid']].append(record['sa_id'])
            sgy_strata_uids_dict[record['s_uid']].append(record['sa_uid'])
            # build strata code list e.g.  ['CP1','CP2','M1',...]
            def add_nat(sa_code_list, nat):
                nb_nat = [re.match(r'(\D*)\d+', sa_code).group(1) for sa_code in sa_code_list].count(nat)
                sa_code_list.append(f"{nat}{nb_nat + 1}")
            add_nat(sgy_strata_nat_dict[record['s_uid']], record['sa_nat'])

        if match_strata_by_class:
            # second pass on all stratigraphies to classify their strata to focus strata similarity calculation
            strata_class_dict = {}
            # all Mx, NMMx, CMx in their own unique class M_C, NMM_C, CMM_C
            # 2 CP closest to M in CP_C1, other CP in CP_C2
            for sgy in sgy_strata_nat_dict:
                # traverse lists of strata ids and nature codes in reverse order to calculate the class of each stratum
                distance_to_m = 0
                for sa_idx in range(len(sgy_strata_dict[sgy]) - 1, -1, -1):
                    sa_id = sgy_strata_dict[sgy][sa_idx]
                    sa_nat_code = sgy_strata_nat_dict[sgy][sa_idx]
                    # strip the number to get the nature only
                    sa_nat = re.match(r'(\D*)\d+', sa_nat_code).group(1)
                    if sa_nat == 'CP':
                        # two possible classes here
                        distance_to_m += 1
                        strata_class_dict[sa_id] = 'CP_C1' if distance_to_m < 3 else 'CP_C2'
                    else:
                        # single class for all other natures
                        strata_class_dict[sa_id] = sa_nat + '_C1'

        def calc_stratigraphy_similarity(s1_uid: str, s2_uid: str, stratum_comp=1, export_stat =  None, s1_name=None, s2_name=None):
            """
                calculate a similarity score for the whole stratigraphy based on strata similarity for each stratum of s1 and s2
            :param s1_uid: uid of first stratigraphy
            :param s2_uid: uid of second stratigraphy
            :param stratum_comp: selection of the computation (default 1)
                    - 1: we compare each stratum of first stratigraphy with all strata of the second
                     and keep the best score (possibly matching same stratum multiple times)
                    - 2: we compare each stratum of first stratigraphy only with the stratum in same position in the second
                     if the 2 stratigraphies dont have the same number of strata we stop the comparison
                     and return the averaged score
                    - 3: we compare each stratum of first stratigraphy only with the stratum in same position in the second
                     if the 2 stratigraphies dont have the same number of strata we continue the comparison with 0 scores
            :param export_stat: optional dictionary containing export object one excel worksheet per stratigraphy calc
            :return: similarity score for s1 / s2 stratigraphies
            """
            logger.info(f'calc_stratigraphy_similarity ({s1_uid})--({s2_uid})')
            if s1_uid == s2_uid:
                return 1
            else:
                if export_stat:
                    worksheet = export_stat['worksheet']
                    # write title row
                    worksheet.write(0, 0, 'Similarity of', bold_format)
                    s1_url = f'{request.scheme}://{request.get_host()}/micorr/#/stratigraphy/{ s1_uid }' # ?observationMode={ obs.observationMode }&colourFamily={ obs.colourFamily }'
                    worksheet.write_url(1, 0, s1_url, row_header_format, string=s1_name if s1_name else s1_uid)
                    s2_url = f'{request.scheme}://{request.get_host()}/micorr/#/stratigraphy/{ s2_uid }' # ?observationMode={ obs.observationMode }&colourFamily={ obs.colourFamily }'
                    worksheet.merge_range(0, 1, 0, 1 + len(sgy_strata_dict[s2_uid]),'', merge_format)
                    worksheet.write_url(0, 1, s2_url, merge_format, string=s2_name if s2_name else s2_uid)

                    # write header row with s2 strata uids
                    worksheet.write_row(1, 1, sgy_strata_nat_dict[s2_uid] + ['max'] , row_header_format)
                    max_col = 1+len(sgy_strata_nat_dict[s2_uid])

                    # add s2 image to header row
                    # export to png not working for now todo fix nodejs export png
                    # url = f"{settings.NODE_BASE_URL}exportStratigraphy?name={s2_uid}&width=50&format=png"
                    # url = f"{settings.NODE_BASE_URL}exportStratigraphy?name={s2_uid}&width=50&format=png"
                    # micorr /  # /stratigraphy/5e7030dc-25da-11e6-9e7f-000c29148083?observationMode=BI&colourFamily=colourFamily
                    # image_data = io.BytesIO(urlopen(url).read())
                    # worksheet.insert_image(0,3, url, {'image_data': image_data})
                    # todo test with image url => worksheet.insert_image('B4', 'python.png', {'url': 'https://python.org'})

                    worksheet.write_column(2, 0, sgy_strata_nat_dict[s1_uid], col_header_format)
                    worksheet.set_column(0, 0, max(25, len(s1_name if s1_name else s1_uid)), right_align_format)
                if stratum_comp==1:
                    # we compare each stratum of first stratigraphy with all strata of the second
                    # and keep the best score (possibly matching same stratum multiple times)
                    by_stratum_score = []
                    for s1_sa_idx , s1_sa_id in enumerate(sgy_strata_dict[s1_uid]):
                        similarities = [strata_similarity.get(frozenset({s1_sa_id, s2_sa_id}), 0) if not match_strata_by_class or strata_class_dict[s1_sa_id]==strata_class_dict[s2_sa_id] else 0 for s2_sa_id in
                                        sgy_strata_dict[s2_uid]]
                        max_similarity = max(similarities)
                        by_stratum_score.append(max_similarity)
                        if export_stat:
                            worksheet.write_row(2+s1_sa_idx, 1, similarities +  [max_similarity])

                    # then simply average by stratum similarity for the whole stratigraphy score
                    score = mean(by_stratum_score) if len(by_stratum_score) else 0
                    if export_stat:
                        worksheet.write(3 + s1_sa_idx, max_col, score, bold_format)
                elif stratum_comp==2:
                    # we compare each stratum of first stratigraphy only with the stratum in same position in the second
                    # if the 2 stratigraphies dont have the same number of strata we stop the comparison
                    # and return the averaged score
                    by_stratum_score = [strata_similarity.get(frozenset(sa_ids), 0) for sa_ids in
                                        zip(sgy_strata_dict[s1_uid], sgy_strata_dict[s2_uid])]
                    # then simply average by stratum similarity for the whole stratigraphy score
                    score = mean(by_stratum_score) if len(by_stratum_score) else 0
                elif stratum_comp==3:
                    # we compare each stratum of first stratigraphy only with the stratum in same position in the second
                    # if the 2 stratigraphies dont have the same number of strata we continue the comparison with 0 scores
                    by_stratum_score = [strata_similarity.get(frozenset(sa_ids), 0) for sa_ids in
                                        zip_longest(sgy_strata_dict[s1_uid], sgy_strata_dict[s2_uid], fillvalue=0)]
                    # then average by stratum similarity for the whole stratigraphy score
                    score = mean(by_stratum_score) if len(by_stratum_score) else 0


                logger.info(f'by stratum:{by_stratum_score} => mean:{score}')
                return score

        # now we add extra info (from Artefact objects and similarity dict) to each stratigraphy
        for stratigraphy in stratigraphies:
            stratigraphy['node_base_url'] = settings.NODE_BASE_URL
            # use strata Jaccard Similarity score retrieved from GDS
            stratigraphy['matching100'] = round(
                100 * calc_stratigraphy_similarity(source_uid, stratigraphy['stratigraphy_uid']), 1)

            artefact = stratigraphy_uids_to_objects[stratigraphy['stratigraphy_uid']].section.artefact if \
                stratigraphy_uids_to_objects[stratigraphy['stratigraphy_uid']] else None
            if artefact:
                if artefact.published:
                    selected_artefact = artefact
                else:
                    published_artefact = artefact.object.artefact_set.filter(published=True).first() if search['public'] and artefact and artefact.object else None
                    selected_artefact = published_artefact if published_artefact else artefact
            else:
                selected_artefact=None
                stratigraphy['artefact_object'] = 'Search'
            if selected_artefact:
                stratigraphy['artefact_id'] = selected_artefact.pk
                stratigraphy['artefact_metal_e_1'] = selected_artefact.metal_e_1.symbol if selected_artefact.metal_e_1 else ''
                stratigraphy['artefact_alloy'] = selected_artefact.alloy.name if selected_artefact.alloy else ''
                stratigraphy['artefact_type'] = selected_artefact.type.name if selected_artefact.type else ''
                stratigraphy['artefact_chronology_category'] = selected_artefact.chronology_category.name if selected_artefact.chronology_category else ''
                stratigraphy['artefact_technology'] = selected_artefact.technology.name if selected_artefact.technology else ''
                stratigraphy['artefact_microstructure'] = selected_artefact.microstructure.name if selected_artefact.microstructure else ''
                stratigraphy['artefact_object'] = selected_artefact.object.name if selected_artefact.object else ''

        sorted_stratigraphies = [s for s in sorted(stratigraphies, key=lambda s: s['matching100'], reverse=True) if s['matching100'] > 1 ]
        if export:
            # create in memory Excel file with one sheet per stratigraphy
            # and return its content along with the results in the POST response
            xls_mem_file = io.BytesIO()
            workbook = xlsxwriter.Workbook(xls_mem_file,{'in_memory': True})
            cell_format_wrap = workbook.add_format()
            cell_format_wrap.set_text_wrap()
            bg_color = '#E78115'
            merge_format = workbook.add_format({
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'bg_color': bg_color,
                'font_color':'white'})
            col_header_format = workbook.add_format({
                'bold': True,
                'align': 'right',
                'bg_color': bg_color,
                'font_color':'white'})
            row_header_format = workbook.add_format({
                'bold': True,
                'align': 'right',
                'bg_color': bg_color,
                'font_color':'white'})
            bold_format= workbook.add_format({'bold': True})
            right_align_format = workbook.add_format({'align': 'right'})

            for stratigraphy in sorted_stratigraphies[1:]:
                stratigraphy_worksheet = workbook.add_worksheet()  # stratigraphy['stratigraphy_uid'] >31 too long as a sheet name
                calc_stratigraphy_similarity(source_uid, stratigraphy['stratigraphy_uid'],
                                             export_stat={'worksheet': stratigraphy_worksheet,
                                                          'cell_format_wrap': cell_format_wrap},
                                             s1_name=sorted_stratigraphies[0]['artefact_object'],
                                             s2_name=stratigraphy['artefact_object']
                                             )
                # add full list of characteristics to the worksheet
                shared_characteristics = self.tx.run("""
                              MATCH (s1:Stratigraphy {uid:$s1_uid}), (s2:Stratigraphy {uid:$s2_uid})
                                 MATCH (s1)-[:POSSESSES]->(s1a:Strata)-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
                                 MATCH (s2)-[:POSSESSES]->(s2a:Strata)-[r2:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
                                 RETURN s1a.uid,id(s1a),s2a.uid,id(s2a),id(c) as c_id, c.uid as c_uid, 0 as c1_weight, 0 as c2_weight, r2.weight as match_weight
                                 ORDER BY s1a.uid,s2a.uid, match_weight DESC
                              UNION
                              MATCH (s1:Stratigraphy {uid:$s1_uid}), (s2:Stratigraphy {uid:$s2_uid})
                                  MATCH (s1)-[:POSSESSES]->(s1a:Strata)-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c), (s2)-[:POSSESSES]->(s2a:Strata)
                                  WHERE NOT (s2a)-[:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
                                  RETURN s1a.uid,id(s1a),s2a.uid,id(s2a),id(c) as c_id,c.uid as c_uid, r.weight as c1_weight, 0 as c2_weight, 0 as match_weight
                                  ORDER BY s1a.uid,s2a.uid, c1_weight DESC
                              UNION
                              MATCH (s1:Stratigraphy {uid:$s1_uid}), (s2:Stratigraphy {uid:$s2_uid})
                                  MATCH (s2)-[:POSSESSES]->(s2a:Strata)-[r:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c), (s1)-[:POSSESSES]->(s1a:Strata)
                                  WHERE NOT (s1a)-[:TMP_HAS_WEIGHTED_CHARACTERISTIC]->(c)
                                  RETURN s1a.uid, id(s1a), s2a.uid, id(s2a), id(c) as c_id, c.uid as c_uid, 0 as c1_weight, r.weight as c2_weight,  0 as match_weight
                                  ORDER BY s1a.uid,s2a.uid, c1_weight,c2_weight DESC""",
                                                     s1_uid=source_uid, s2_uid=stratigraphy['stratigraphy_uid'])
                row_idx = 4+len(sgy_strata_dict[source_uid])
                stratigraphy_worksheet.write(row_idx, 0, 'List of characteristics:', bold_format)
                row_idx+=2 # skip one row to allow correct table selection in Excel
                stratigraphy_worksheet.write_row(row_idx, 0, shared_characteristics.keys(),row_header_format)

                def stratum_uid_to_stratum_code(sa_uid):
                    """
                    Use previously built strata codes per stratigraphy dict ( sgy_strata_nat_dict )
                    to replace strata uid by its code
                    :param sa_uid: stratum uid
                    :return: stratum nature code ('CP1', 'M1', etc...)
                    """
                    sg_uid_sa_nb=sa_uid.split('_Strata')
                    sg_uid=sg_uid_sa_nb[0]
                    sa_nb=int(sg_uid_sa_nb[1])
                    return sgy_strata_nat_dict[sg_uid][sa_nb-1]

                for record in shared_characteristics:
                    row_idx+=1
                    row = record.values()
                    # convert s1a.uid to stratum code
                    row[0] = stratum_uid_to_stratum_code(row[0])
                    # convert s2a.uid to stratum code
                    row[2] = stratum_uid_to_stratum_code(row[2])
                    stratigraphy_worksheet.write_row(row_idx, 0, row)

                # all_characteristic_worksheet = workbook.add_worksheet()
                # toInteger(replace(st.uid, $sg_strata_prefix, "")) as strata_num
                # order by strata_num""", uid=stratigraphy_uid,sg_strata_prefix= stratigraphy_uid +"_Strata")

            workbook.close()
            b64content = base64.b64encode(xls_mem_file.getvalue())

        return dict(results=sorted_stratigraphies, results_sheet_file=b64content if export else None)

    # Ajout d'un artefact
    # @params nom de l'artefact
    # @returns 0 si pas ok, 1 si ok
    def addArtefact(self, artefact):
        nb = self.tx.evaluate("MATCH (n:Artefact) where n.uid=$artefact RETURN count(n.uid) as nb",
                                 artefact=artefact)
        if nb >= 1:
            return {'res': 0}
        else:
            self.tx.run("CREATE(artefact:Artefact{uid:$artefact, date:$today, label:'artefact'})",
                           artefact=artefact, today=time.strftime("%Y-%m-%d"))
            return {'res': 1}
        """
        todo: replace by a merge..
        MERGE (n:Artefact {uid:$artefact})
        ON CREATE SET n.date = $today
        RETURN n.uid
        """

    # suppression d'un artefact
    # @params nom de l'artefact
    # @returns
    def delArtefact(self, artefact):
        listStrat = self.tx.run(
            "MATCH (a:Artefact)-[r:IS_REPRESENTED_BY]->(s:Stratigraphy) where a.uid=$artefact RETURN  s.uid as uid",
            artefact=artefact)
        for strat in listStrat:
            self.deleteStratigraphy(strat['uid'])
        self.tx.run("MATCH (a:Artefact) where a.uid=$artefact OPTIONAL MATCH (a)-[x]-() DELETE x, a",
                       artefact=artefact)
        return {'res': 1}
