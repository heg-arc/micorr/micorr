from django.urls import  re_path
from stratigraphies import views as stratigraphies_views
from stratigraphies.views import ShareCreateView, ShareListView, ShareDeleteView

app_name = "stratigraphies"

urlpatterns = [
    re_path(r'^$', stratigraphies_views.home, name='list'),
    re_path(r'^json/isauthenticated', stratigraphies_views.isauthenticated),
    re_path(r'^json/getallartefacts$', stratigraphies_views.getallartefacts),
    re_path(r'^json/getallcharacteristic$', stratigraphies_views.getallcharacteristic),
    re_path(r'^json/getstratsbyartefact/(?P<artefact>\w+)$', stratigraphies_views.getStratigraphyByArtefact),
    re_path(r'^json/getstratigraphydetails/(?P<stratigraphy>[\w-]+)$', stratigraphies_views.getStratigraphyDetails),
    re_path(r'^json/stratigraphyexists/(?P<stratigraphy>[\w-]+)$', stratigraphies_views.stratigraphyExists),
    re_path(r'^json/addstratigraphy/(?P<artefact>\w+)/(?P<stratigraphy>[\w-]+)$', stratigraphies_views.addStratigraphy),
    re_path(r'^json/addartefact/(?P<artefact>\w+)$', stratigraphies_views.addArtefact),
    #re_path(r'^json/save/(?P<data>[[a-zA-Z0-9%-\{\}\[\]\'\"\:\,\ \_]{0,}]{0,})$', stratigraphies_views.views.save),
    re_path(r'^json/save$', stratigraphies_views.save),
    #re_path(r'^json/match/(?P<data>[[a-zA-Z0-9%-\{\}\[\]\'\"\:\,\ \_]{0,}]{0,})$', stratigraphies_views.views.match),
    re_path(r'^json/find_similar$', stratigraphies_views.find_similar),
    re_path(r'^json/deleteStratigraphy/(?P<stratigraphy>[\w-]+)$', stratigraphies_views.deleteStratigraphy),
    re_path(r'^json/deleteartefact/(?P<artefact>\w+)$', stratigraphies_views.deleteArtefact),
    re_path(r'^json/getnaturefamily/(?P<nature>\w+)$', stratigraphies_views.getnaturefamily),
    re_path(r'^update-description/(?P<stratigraphy>[\w-]+)$', stratigraphies_views.update_stratigraphy_description, name='update-description'),
    re_path(r'^delete-user/(?P<stratigraphy>[\w-]+)$', stratigraphies_views.delete_stratigraphy_user, name='delete-user'),
    re_path(r'^email$', stratigraphies_views.sendEmail, name='send_email'),
    re_path(r'^node_descriptions$', stratigraphies_views.node_descriptions, name='node_descriptions'),
    re_path(r'^(?P<stratigraphy>[\w-]+)/create/share$', ShareCreateView.as_view(), name='create-share'),
    re_path(r'^(?P<stratigraphy>[\w-]+)/delete/share/(?P<user_id>\d+)$', ShareDeleteView.as_view(), name='delete-share'),
    re_path(r'^(?P<stratigraphy>[\w-]+)/list/shares$', ShareListView.as_view(), name='list-share')
]

