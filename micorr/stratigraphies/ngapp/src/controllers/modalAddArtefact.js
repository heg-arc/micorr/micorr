'use strict';
import angular from 'angular';

import {testUserInput} from "../init.js";

/**
 * @ngdoc function
 * @name micorrApp.controller:ModalAddArtefactCtrl
 * @description
 * # ModalAddArtefactCtrl
 */
angular.module('micorrApp')
    .controller('ModalAddArtefactCtrl', function ($scope, $route, $uibModal, $log, MiCorrService) {
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'AddArtefactContent.html',
                size: size,
                controller: ['$scope', '$uibModalInstance','scopeParent', 'id',
                    function($scope, $uibModalInstance,scopeParent,id) {
                        $scope.artefact;
                        $scope.ok = function() {
                            if (testUserInput($scope.artefact)) {
                                MiCorrService.createArtefact($scope.artefact).then(function(response){
                                    if (response.data['res'] == 0)
                                        alert("This artefact already exists in database !");
                                    else {
                                        $uibModalInstance.close();
                                        $route.reload();
                                    }
                                });
                            }
                            else
                                alert("Only alphanumeric characters are allowed");

                        };
                        $scope.cancel = function() {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                ],
                resolve: {
                    scopeParent: function() {
                        return $scope; //On passe à la fenêtre modal une référence vers le scope parent.
                    },
                    id: function(){
                        return $scope.id; // On passe en paramètre l'id de l'élément à supprimer.
                    }
                }
            });

        };
    });
