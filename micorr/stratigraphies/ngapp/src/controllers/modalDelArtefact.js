'use strict';
import angular from 'angular';

/**
 * @ngdoc function
 * @name micorrApp.controller:ModalDelArtefactCtrl
 * @description
 * # ModalDelArtefactCtrl
 */
angular.module('micorrApp')
    .controller('ModalDelArtefactCtrl', function ($scope, $route, $uibModal, $log, MiCorrService) {
        $scope.open = function (size, artefact) {
            $scope.artefact = artefact;
            var modalInstance = $uibModal.open({
                templateUrl: 'delArtefactContent.html',
                size: size,
                controller: ['$scope', '$uibModalInstance','scopeParent', 'artefact',
                    function($scope, $uibModalInstance,scopeParent,artefact) {
                        $scope.ok = function() {
                            MiCorrService.deleteArtefact(artefact).then(function (response) {
                                $uibModalInstance.close();
                                $route.reload();
                            });
                        };
                        $scope.cancel = function() {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                ],
                resolve: {
                    scopeParent: function() {
                        return $scope; //On passe à la fenêtre modal une référence vers le scope parent.
                    },
                    artefact: function(){
                        return $scope.artefact; // On passe en paramètre l'id de l'élément à supprimer.
                    }
                }
            });

        };
    });
