'use strict';
import angular from 'angular';

import {Strata} from "../business/strata.js";
import {Characteristic} from "../business/characteristic.js";


/**
 * @ngdoc function
 * @name micorrApp.controller:ModalAddStrataCtrl
 * @description
 * # AddStratigraphyCtrl
 * Contrôlleur qui s'occupe d'ajouter une strate
 */
angular.module('micorrApp')
    .controller('ModalAddStrataCtrl', function ($scope, $rootScope, $uibModal, MiCorrService, StratigraphyData) {

        $scope.$on('initShowStrat', function (event) {
            // get characteristics of nature Family dynamically from StratigraphyData once they have been loaded
            $rootScope.natures = StratigraphyData.natureFamily.characteristics
        })

        $scope.open = function () {
            var modalInstance = $uibModal.open({
                templateUrl: 'modalAddStrata.html',
                controller: ['$scope', 'scopeParent', '$uibModalInstance',
                    function ($scope, scopeParent, $uibModalInstance) {
                        $scope.ok = function () {
                            let nature = new Characteristic('natureFamily', $scope.nature);
                            if (nature != undefined) {
                                var newStrata = new Strata(nature.getRealName(), false, StratigraphyData.getStratigraphy().stratas.length);
                                newStrata.replaceCharacteristic(nature);
                                newStrata.setUid(StratigraphyData.getStratigraphy().getUid() + '_strata_' + (newStrata.getIndex() + 1));

                                //Si c'est une strate CM on lui ajoute deux strates enfant
                                if (nature.getRealName() == 'Corroded metal') {
                                    //Ajout d'un ratio de corrosion défini à 1 par défaut
                                    var ratioChar = new Characteristic();
                                    ratioChar.setName('r1Characteristic');
                                    ratioChar.setRealName('r1');
                                    ratioChar.setFamily('cmCorrosionRatioFamily')
                                    newStrata.replaceCharacteristic(ratioChar);

                                    //Ajout de la sous strate CP

                                    var cpNature = new Characteristic('natureFamily', StratigraphyData.natureFamily.characteristics.find(c => c.code == 'CP'))
                                    var childCPStrata = new Strata(cpNature.getRealName(), true);
                                    childCPStrata.replaceCharacteristic(cpNature);
                                    childCPStrata.setUid(newStrata.getUid() + '_childCP');
                                    newStrata.addChildStrata(childCPStrata);

                                    //Ajout de la sous strate M
                                    var mNature = new Characteristic('natureFamily', StratigraphyData.natureFamily.characteristics.find(c => c.code == 'M'))
                                    var childMStrata = new Strata(mNature.getRealName(), true);
                                    childMStrata.replaceCharacteristic(mNature);
                                    childMStrata.setUid(newStrata.getUid() + '_childM');
                                    newStrata.addChildStrata(childMStrata);
                                }


                                StratigraphyData.pushOneStrata(newStrata);

                                scopeParent.$emit('doUpdate', StratigraphyData.getStratigraphy().getStratas().length - 1);
                                $uibModalInstance.close();
                            }

                        };

                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                ],
                resolve: {
                    scopeParent: function () {
                        return $scope; //On passe à la fenêtre modal une référence vers le scope parent.
                    }
                }
            });
        }

    });
