'use strict';
import angular from 'angular';

/**
 * @ngdoc function
 * @name micorrApp.controller:TabsStrataCtrl
 * @description
 * # TabsStrataCtrl
 * contrôlleur qui est le formulare qui contient les champs mais sans les onglets
 */
angular.module('micorrApp')
    .controller('TabsStrataCtrl', function ($scope, $route, $window, MiCorrService) {

    });
