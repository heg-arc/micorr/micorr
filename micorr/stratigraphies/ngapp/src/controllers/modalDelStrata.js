'use strict';
import angular from 'angular';

/**
 * @ngdoc function
 * @name micorrApp.controller:ModalDelStrataCtrl
 * @description
 * # ModalDelStrataCtrl
 * Contrôlleur qui s'occupe de supprimer une strate dans le service
 *
 * !!!CE FICHIER N'EST PLUS UTILISE!!!
 *
 */
angular.module('micorrApp')
    .controller('ModalDelStrataCtrl', function ($scope, $uibModal, $log, StrataData) {
        $scope.artefactName = $scope.$parent.artefactName;

        //quand on supprime une strate, on se positionne sur la strate 0 et on met à jour le dessin
        $scope.doUpdate = function () {
            StrataData.delStratum();
            $scope.$emit('doUpdate', 0);
        };

        //ouverture de la fenêtre de suppression de la strate.
        // SI on appuie sur ok alors la strate est supprimée dans le service et le dessin est mis à jour
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'delStrataContent.html',
                size: size,
                controller: ['$scope', '$uibModalInstance','scopeParent', 'id',
                    function($scope, $uibModalInstance,scopeParent,id) {
                        $scope.ok = function() {

                            scopeParent.doUpdate();

                            $uibModalInstance.close();
                        };
                        $scope.cancel = function() {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                ],
                resolve: {
                    scopeParent: function() {
                        return $scope; //On passe à la fenêtre modal une référence vers le scope parent.
                    },
                    id: function(){
                        return $scope.id; // On passe en paramètre l'id de l'élément à supprimer.
                    }
                }
            });

        };
    });
