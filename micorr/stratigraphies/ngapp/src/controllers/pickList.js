'use strict';
import angular from 'angular';


/**
 * @ngdoc function
 * @name micorrApp.controller:PickListCtrl
 * @description
 * # PickListCtrl
 * Contrôlleur qui s'occupe des picklist
 */
angular.module('micorrApp')
    .controller('PickListCtrl', function ($scope, $location, $routeParams, MiCorrService) {
        $scope.selectedSubcprimicrostructureFamily = [];
    });
