'use strict';
import angular from 'angular';


/**
 * @ngdoc function
 * @name micorrApp.controller:ModalDelStratigraphyCtrl
 * @description
 * # ModalDelStratigraphyCtrl
 */
angular.module('micorrApp')
    .controller('ModalDelStratigraphyCtrl', function ($scope, $route, $uibModal, $log, MiCorrService) {
        $scope.open = function (size, strat) {
            var modalInstance = $uibModal.open({
                templateUrl: 'delStratigraphyContent.html',
                size: size,
                controller: ['$scope', '$uibModalInstance','scopeParent', 'id',
                    function($scope, $uibModalInstance,scopeParent,id) {
                        $scope.ok = function() {
                            MiCorrService.deleteStratigraphy(strat).then(function(response){
                                $route.reload();
                            });
                            $uibModalInstance.close();
                        };
                        $scope.cancel = function() {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                ],
                resolve: {
                    scopeParent: function() {
                        return $scope; //On passe à la fenêtre modal une référence vers le scope parent.
                    },
                    id: function(){
                        return $scope.id; // On passe en paramètre l'id de l'élément à supprimer.
                    }
                }
            });

        };
    });
