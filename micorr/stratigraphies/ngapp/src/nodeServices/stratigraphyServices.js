/**
 * Created by Thierry Hubmann
 * Ce fichier contient tous les services liés aux stratigraphies pour Node.js
 */
import axios from 'axios';
import { createSVGWindow, createWindow } from 'svgdom'

import {Stratigraphy} from '../business/stratigraphy.js';
import {GraphGenerationUtil} from '../utils/graphGenerationUtil.js';


var url = process.env.DJANGO_SERVICE_URL || "http://django:5000/micorr";
console.log('stratigrapyService.js: DJANGO_SERVICE_URL='+url);

export default  {

    /**
     * Retourne une instance de Stratigraphy en spécifiant son nom
     * @param name le nom de la stratigraphie
     * return une instance de la stratigraphie
     */
    getStratigraphyByName: function (name, callback) {
        var stratigraphy = new Stratigraphy();
        axios.get(url + "/json/getstratigraphydetails/" + name).then(function (response) {
             console.log("getStratigraphyByName" + name);
            stratigraphy.load(response.data, name);
            return callback(stratigraphy);
        }).catch(function(error) {
            // handle error
            console.log(error);
            }
        ).then(function () {
    // always executed
  });
    },

    /**
     * Dessine la stratigraphie et retourne le SVG généré
     * @param stratigraphy
     * @param width
     * @param callback
     * @returns le SVG généré
     */
    drawStratigraphy: function (stratigraphy, width, callback) {
        // see nodeUtils.js getDrawer
        const window  = createSVGWindow()
        var drawer = new GraphGenerationUtil(window, stratigraphy);
        if (width == undefined) {
            width = 200;
        }
        else
            width = parseInt(width);
        var result = drawer.drawStratigraphy(width);
        console.log('stratigraphy drawn')
        return callback(result);
    }

    };
