import {initStratTab} from "../controllers/stratLib.js";
import {saveAs} from "file-saver";

angular.module('micorrApp').component('findSimilar', {
    //template: 'TBD: Detail view for <span>{{$ctrl.strat}}</span>',
    template: require('../../views/find-similar.html'),
    bindings: {
        sdata: '='
    },
    controller: ['$routeParams', '$scope', 'MiCorrService','StratigraphyData',
        function FindSimilarController($routeParams, $scope, MiCorrService, StratigraphyData) {

            let stratigraphy = StratigraphyData.getStratigraphy();
            // weights list generated from spec using notebook/Similarity-spreadsheet-extract-tool
            let _defaultWeights = [{'mode': 'Bi', 'name': 'Shape', 'uid': 'shapeFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Direction', 'uid': 'directionFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Brightness', 'uid': 'brightnessFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Thick. relative', 'uid': 'thicknessFamily', 'value': 40}, {'mode': 'Bi', 'name': 'Thick. Average', 'uid': 'avgThicknessVarFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Continuity', 'uid': 'continuityFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Colour', 'uid': 'colourFamily', 'value': 40}, {'mode': 'Bi', 'name': 'Opacity', 'uid': 'opacityFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Compactness', 'uid': 'porosityFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Hardness', 'uid': 'hardnessFamily', 'value': 40}, {'mode': 'Bi', 'name': 'Cohesion', 'uid': 'cohesionFamily', 'value': 40}, {'mode': 'Bi', 'name': 'Cracking struct.', 'uid': 'crackingStructureFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Crack. Surf. Dir.', 'uid': 'crackingSurfaceDirectionFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Crack. In. Dir.', 'uid': 'crackingInwardDirectionFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Crack. length', 'uid': 'crackingLengthVarFamily', 'value': 5}, {'mode': 'Bi', 'name': 'Crack. width', 'uid': 'crackingWidthFamily', 'value': 5}, {'mode': 'Bi', 'name': 'Crack. Filling', 'uid': 'crackingFillingFamily', 'value': 5}, {'mode': 'Bi', 'name': 'Crack. Edge', 'uid': 'crackingEdgesFamily', 'value': 5}, {'mode': 'Bi', 'name': 'Types of cracks', 'uid': 'crackingTypeOfCracksFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Microstructure', 'uid': 'microstructureFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Main element(s)', 'uid': 'compositionMainElementsFamily', 'value': 40}, {'mode': 'Bi', 'name': 'Sec. element(s)', 'uid': 'compositionSecondaryElementsFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Add. element(s)', 'uid': 'compositionAdditionalElementsFamily', 'value': 5}, {'mode': 'Bi', 'name': 'Profile', 'uid': 'interfaceProfileFamily', 'value': 40}, {'mode': 'Bi', 'name': 'Transition', 'uid': 'interfaceTransitionFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Roughness', 'uid': 'interfaceRoughnessFamily', 'value': 10}, {'mode': 'Bi', 'name': 'Adherence', 'uid': 'interfaceAdherenceFamily', 'value': 10}, {'mode': 'CS', 'name': 'Continuity', 'uid': 'continuityCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Opacity', 'uid': 'morphologyOpacityWithOpticalMicroscopeDarkFieldCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Colour ODF', 'uid': 'morphologyColourWithOpticalMicroscopeDarkFieldCSFamily', 'value': 10}, {'mode': 'CS', 'name': 'Colour OBF', 'uid': 'morphologyColourWithOpticalMicroscopeBrightFieldCSFamily', 'value': 10}, {'mode': 'CS', 'name': 'Colour SEM SE', 'uid': 'morphologyColourWithScanningElectronMicroscopeSecondaryElectronsCSFamily', 'value': 10}, {'mode': 'CS', 'name': 'Colour SEM BE', 'uid': 'morphologyColourWithScanningElectronMicroscopeBackscatteredElectronsCSFamily', 'value': 10}, {'mode': 'CS', 'name': 'Thick. relative', 'uid': 'thicknessCSFamily', 'value': 10}, {'mode': 'CS', 'name': 'Thick. Average', 'uid': 'avgThicknessCSVarFamily', 'value': 5}, {'mode': 'CS', 'name': 'Compactness', 'uid': 'porosityFamily', 'value': 5}, {'mode': 'CS', 'name': 'Porosities amount', 'uid': 'texturePorositiesAmountCSVarFamily', 'value': 20}, {'mode': 'CS', 'name': 'Microhardness (M)', 'uid': 'textureMicrohardnessCSVarFamily', 'value': 20}, {'mode': 'CS', 'name': 'Cracking struct.', 'uid': 'crackingStructureCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Crack. In. Dir.', 'uid': 'crackingInwardDirectionCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Crack. length', 'uid': 'crackingLengthCSVarFamily', 'value': 5}, {'mode': 'CS', 'name': 'Crack. width', 'uid': 'crackingWidthCSVarFamily', 'value': 5}, {'mode': 'CS', 'name': 'Crack. Filling', 'uid': 'crackingFillingCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Crack. Depth', 'uid': 'crackingDepthCSVarFamily', 'value': 5}, {'mode': 'CS', 'name': 'Microstructure', 'uid': 'microstructureCSFamily', 'value': 25}, {'mode': 'CS', 'name': 'Microstru. Subchar.', 'uid': 'microstructureCSFamily', 'value': 20}, {'mode': 'CS', 'name': 'Main element(s) list', 'uid': 'compositionMainElementsListCSFamily', 'value': 40}, {'mode': 'CS', 'name': 'Main element(s) amount', 'uid': 'compositionMainElementsListCSFamily', 'value': 5, 'amount': 1}, {'mode': 'CS', 'name': 'Sec. element(s) list', 'uid': 'compositionSecondaryElementsListCSFamily', 'value': 40}, {'mode': 'CS', 'name': 'Sec. element(s) amount', 'uid': 'compositionSecondaryElementsListCSFamily', 'value': 5, 'amount': 1}, {'mode': 'CS', 'name': 'Add. element(s) list', 'uid': 'compositionAdditionalElementsListCSFamily', 'value': 10}, {'mode': 'CS', 'name': 'Add. element(s) amount', 'uid': 'compositionAdditionalElementsListCSFamily', 'value': 5, 'amount': 1}, {'mode': 'CS', 'name': 'Compounds-XRD', 'uid': 'compositionCompoundsXrdCSFamily', 'value': 40}, {'mode': 'CS', 'name': 'Compounds-FTIR', 'uid': 'compositionCompoundsFtirCSFamily', 'value': 40}, {'mode': 'CS', 'name': 'Compounds0Raman', 'uid': 'compositionCompoundsRamanSpectroscopyCSFamily', 'value': 40}, {'mode': 'CS', 'name': 'Coupounds-Others', 'uid': 'compositionCompoundsOthersCSFamily', 'value': 40}, {'mode': 'CS', 'name': 'Profile', 'uid': 'interfaceProfileCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Transition', 'uid': 'interfaceTransitionCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Adherence', 'uid': 'interfaceAdherenceCSFamily', 'value': 5}, {'mode': 'CS', 'name': 'Rp', 'uid': 'interfaceRpCSVarFamily', 'value': 5}, {'mode': 'CS', 'name': 'Rt', 'uid': 'interfaceRtCSVarFamily', 'value': 5}, {'mode': 'CS', 'name': 'Rc', 'uid': 'interfaceRcCSVarFamily', 'value': 5}, {'mode': 'CS', 'name': 'Ra', 'uid': 'interfaceRaCSVarFamily', 'value': 5}];
            function selectWeights() {
                for (let weight of _defaultWeights) {
                    if (stratigraphy.uid && stratigraphy.stratas.length) {
                        let allStrataCharacteristics = [];
                        for (let stratum of stratigraphy.stratas) {
                            allStrataCharacteristics = allStrataCharacteristics.concat(stratum.getCharacteristicsByFamily(weight.uid).concat(stratum.getContainerElements(weight.uid)));
                            if (weight.uid in stratum.variables) {
                                allStrataCharacteristics.push(stratum.variables[weight.uid]);
                            }
                        }
                        weight.disabled = allStrataCharacteristics.length == 0;
                    } else {
                        weight.disabled = false;
                    }
                }
            }
            selectWeights();
            const FAMILY_GROUPS = [
                {order: 0, uid: 'fgMorphology', name: 'Morphology'},
                {order: 1, uid: 'fgTexture', name: 'Texture'},
                {order: 2, uid: 'fgMicrostructure', name: 'Microstructure'},
                {order: 3, uid: 'fgComposition', name: 'Composition'},
                {order: 4, uid: 'fgInterface', name: 'Interface'}
            ]
            $scope.fgTabs={};
            for (let familyGroup of FAMILY_GROUPS) {
                $scope.fgTabs[familyGroup.uid] = {};
                initStratTab($scope.fgTabs[familyGroup.uid], StratigraphyData, familyGroup.uid)
            }

            console.log('findSimilarController - stratigraphy.uid='+stratigraphy.uid);
            $scope.weights = angular.copy(_defaultWeights);
            $scope.checkWeights = {
                Bi: stratigraphy.observationMode.binocular, CS: !stratigraphy.observationMode.binocular
            };

            $scope.filterWeights = function(value, index, array) {
              return (value.mode == ($scope.checkWeights.Bi ? 'Bi' : '')) || (value.mode == ($scope.checkWeights.CS ? 'CS' : ''))  ;
            };
            $scope.target ={
                stratigraphy:$routeParams.strat,
                search: {public:true, private:false},
                export : true,
                match_strata_by_class: false
            };
            $scope.defaultWeights = function () {
                $scope.weights = angular.copy(_defaultWeights);
            }
            $scope.clearWeights = function () {

                $scope.weights.forEach(function (el) {
                    el.value = 0;
                });
            }

            $scope.retry = function () {
                $scope.target.weights = $scope.weights.filter($scope.filterWeights).filter(w => !w.disabled);
                $scope.results = [];
                $scope.results_sheet_file = null;
                MiCorrService.matchStratigraphy(JSON.stringify($scope.target)).then(function (response) {
                    $scope.results = response.data.results;
                    $scope.results_sheet_file = response.data.results_sheet_file;
                });
            };
            $scope.download_xls = function() {
                if ($scope.results_sheet_file) {
                        // xlsx spreadsheet sent as base64 as part of the api response
                        // convert bqck to binary and auto download
                        const type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        const dataurl = `data:${type};base64,${$scope.results_sheet_file}`
                        fetch(dataurl).then(res => res.blob()).then(blob => saveAs(blob, 'similarities.xlsx'))
                    }
            }
            // First search on modal opening with default weights
            $scope.retry();

        }
    ]
}).filter('fg_weights', function () {
    return function (allWeights, checkWeights, fg) {
        // custom ng filter with checkWeights and fg arguments
        // to filter weights by observation mode and family Group
        function filterWeights(value, index, array) {
            return ((value.mode == (checkWeights.Bi ? 'Bi' : '')) || (value.mode == (checkWeights.CS ? 'CS' : ''))) &&
                (fg.familyGroup && fg.familyGroup.find(f => f.uid  == value.uid));
        };
        return allWeights.filter(filterWeights);
    }
});
