const path = require('path');
const webpack = require('webpack');


module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../static/micorr/dist')
    },
    mode: 'development',
    target: 'web',
    devtool: 'source-map',
    module:
            {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|nodeServices)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ["@babel/preset-env"]
                        }
                    }
                },
                {test: /\.html$/i, loader: "html-loader"},
                {test: /\.css$/, use: ["style-loader", "css-loader"]},
                { test: /\.tsx?$/, loader: "ts-loader" }
            ]
        },
    plugins: [
        // build optimization plugins
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(true),
            WEBPACK_TARGET: "web",
        })
    ],
    resolve: {
        fallback: {
            fs: false,
            path: false
        },
        alias: {
            'datauri/sync.js$': '/src/utils/noop_datauri.js'
        }
    }
};

