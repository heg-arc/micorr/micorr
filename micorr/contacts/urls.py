from django.urls import  re_path
from django.contrib.auth.decorators import login_required

from .views import ContactCreateView

app_name="contacts"

urlpatterns = [
    re_path(r'^create/$', login_required(ContactCreateView.as_view()), name='contact-create'),
]

