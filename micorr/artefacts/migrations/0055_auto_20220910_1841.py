# Generated by Django 3.0.14 on 2022-09-10 16:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('artefacts', '0054_sectiontemplate_title_title'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='element',
            options={'get_latest_by': 'modified'},
        ),
    ]
