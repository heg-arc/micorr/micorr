# Generated by Django 3.0.14 on 2022-11-05 22:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artefacts', '0055_auto_20220910_1841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='token',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
    ]
