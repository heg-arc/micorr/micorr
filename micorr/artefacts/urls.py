from django.urls import  re_path
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from artefacts import views as artefacts_views

app_name="artefacts"

urlpatterns = [
    re_path(r'^$', artefacts_views.ArtefactListView.as_view(), name='artefact-list'),
    re_path(r'^keywords/$', artefacts_views.ArtefactListView.as_view(), name='artefact-search-by-keywords'),
    re_path(r'^(?P<pk>\d+)/$', artefacts_views.ArtefactDetailView.as_view(), name='artefact-detail'),
    re_path(r'^(?P<pk>\d+)/print/$', artefacts_views.ArtefactDetailPrintView.as_view(), name='artefact-detail-print'),

    re_path(r'^(?P<pk>\d+)/update/$', login_required(artefacts_views.ArtefactUpdateView.as_view()),
        name='artefact-update'),
    re_path(r'^(?P<pk>\d+)/delete/$', login_required(artefacts_views.ArtefactDeleteView.as_view()),
        name='artefact-delete'),
    re_path(r'^(?P<pk>\d+)/delete/(?P<object_id>\d+)/$', login_required(artefacts_views.ArtefactDeleteView.as_view()),
        name='artefact-object-delete'),
    re_path(r'^create/$', login_required(artefacts_views.ArtefactUpdateView.as_view()), name='artefact-create'),

    re_path(r'^add/author/?$', artefacts_views.newAuthor),
    re_path(r'^add/type/?$', artefacts_views.newType),
    re_path(r'^add/origin/?$', artefacts_views.newOrigin),
    re_path(r'^add/recovering_date/?$', artefacts_views.newRecoveringDate),
    re_path(r'^add/environment/?$', artefacts_views.newEnvironment),
    re_path(r'^add/location/?$', artefacts_views.newLocation),
    re_path(r'^add/owner/?$', artefacts_views.newOwner),
    re_path(r'^add/alloy/?$', artefacts_views.newAlloy),
    re_path(r'^add/technology/?$', artefacts_views.newTechnology),
    re_path(r'^add/sample_location/?$', artefacts_views.newSampleLocation),
    re_path(r'^add/responsible_institution/?$', artefacts_views.newResponsibleInstitution),
    re_path(r'^add/microstructure/?$', artefacts_views.newMicrostructure),
    re_path(r'^add/corrosion_form/?$', artefacts_views.newCorrosionForm),
    re_path(r'^add/corrosion_type/?$', artefacts_views.newCorrosionType),

    re_path(r'^(?P<section_id>\d+)/create/image/$', login_required(artefacts_views.ImageCreateView.as_view()), name='image-create'),
    re_path(r'^(?P<section_id>\d+)/update/image/(?P<pk>\d+)/$', login_required(artefacts_views.ImageUpdateView.as_view()), name='image-update'),
    re_path(r'^(?P<section_id>\d+)/refresh/image/$', TemplateView.as_view(template_name='artefacts/refresh.html'), name='image-refresh'),
    re_path(r'^(?P<section_id>\d+)/refresh/div/$', artefacts_views.RefreshDivView, name='div-refresh'),
    re_path(r'^image/(?P<pk>\d+)/delete/$', login_required(artefacts_views.ImageDeleteView.as_view()), name='image-delete'),
    re_path(r'^(?P<section_id>\d+)/list/stratigraphy/$', artefacts_views.StratigraphyListView, name='stratigraphy-list'),
    re_path(r'^(?P<section_id>\d+)/add/stratigraphy/(?P<stratigraphy_uid>[\w\-]+)/$',artefacts_views.StratigraphyAddView, name='add-stratigraphy'),
    re_path(r'^(?P<section_id>\d+)/update/stratigraphy/(?P<pk>[\w\-]+)/$',login_required(artefacts_views.StratigraphyUpdateView.as_view()),name='stratigraphy-update'),
    re_path(r'^(?P<section_id>\d+)/refresh/stratigraphy/$', TemplateView.as_view(template_name='artefacts/strat-refresh.html'), name='strat-refresh'),
    re_path(r'^(?P<section_id>\d+)/refresh/strat-div/$', artefacts_views.RefreshStratDivView, name='strat-div-refresh'),
    re_path(r'^stratigraphy/(?P<pk>\d+)/delete/$', login_required(artefacts_views.StratigraphyDeleteView.as_view()), name='stratigraphy-delete'),

    re_path(r'^(?P<artefact_id>\d+)/document/(?P<pk>\d+)/update/$', login_required(artefacts_views.DocumentUpdateView.as_view()),
       name='document-update'),
    re_path(r'^(?P<artefact_id>\d+)/document/(?P<pk>\d+)/delete/$',
       login_required(artefacts_views.DocumentDeleteView.as_view()), name='document-delete'),
    re_path(r'^(?P<artefact_id>\d+)/document/create/$', login_required(artefacts_views.DocumentCreateView.as_view()),
       name='document-create'),
    re_path(r'^stratigraphy/$', artefacts_views.search_stratigraphy, name='searchStratigraphy'),

    re_path(r'^(?P<artefact_id>\d+)/contact_author/', artefacts_views.contactAuthor, name='contact_author'),
    re_path(r'^(?P<artefact_id>\d+)/share/', artefacts_views.shareArtefact, name='share_artefact'),
    re_path(r'^(?P<artefact_id>\d+)/share_with_a_friend/', artefacts_views.shareArtefactWithFriend, name='share_artefact_with_friend'),

    re_path(r'^(?P<artefact_id>\d+)/tokens/$', login_required(artefacts_views.listToken), name='list_tokens'),
    re_path(r'^(?P<artefact_id>\d+)/token/(?P<pk>\d+)/delete/$',
        login_required(artefacts_views.TokenDeleteView.as_view()), name='delete_token'),

    re_path(r'^collaboration/$', login_required(artefacts_views.CollaborationListView.as_view()), name='collaboration-menu'),
    re_path(r'^collaboration/deleted/$', login_required(artefacts_views.CollaborationDeletedListView.as_view()), name='collaboration-deleted-menu'),
    re_path(r'^collaboration/(?P<token_id>\d+)/update/$', login_required(artefacts_views.CollaborationUpdateView.as_view()), name='collaboration-update'),
    re_path(r'^collaboration/(?P<token_id>\d+)/comment/$', login_required(artefacts_views.CollaborationCommentView.as_view()), name='collaboration-comment'),
    re_path(r'^collaboration/(?P<token_id>\d+)/comment/(?P<section_id>\d+)/(?P<field>\w*)/$', login_required(artefacts_views.CollaborationCommentView.as_view()), name='collaboration-comment'),
    re_path(r'^(?P<pk>\d+)/collaboration/hide/$', login_required(artefacts_views.CollaborationHideView.as_view()), name='collaboration-hide'),
    re_path(r'^(?P<token_id>\d+)/collaboration/retrieve/$', login_required(artefacts_views.retrieveDeletedCollaboration), name='collaboration-retrieve'),

    re_path(r'^collaboration/(?P<token_id>\d+)/send/$', login_required(artefacts_views.sendComments), name='send-comments'),
    re_path(r'collaboration/(?P<pk>\d+)/read/$', login_required(artefacts_views.CommentReadView.as_view()), name='read-comments'),
    re_path(r'^collaboration/(?P<pk>\d+)/delete/$', login_required(artefacts_views.CommentDeleteView.as_view()), name='delete-comment'),

    re_path(r'^publication/$', login_required(artefacts_views.PublicationListView.as_view()), name='publication-menu'),
    re_path(r'^publication/create/(?P<pk>\d+)/$', login_required(artefacts_views.PublicationCreateView.as_view()), name='publication-create'),
    re_path(r'^publication/(?P<pk>\d+)/$', login_required(artefacts_views.PublicationArtefactDetailView.as_view()), name='publication-artefact-detail'),
    re_path(r'^administration/$', login_required(artefacts_views.AdministrationListView.as_view()), name='publication-administration-menu'),
    re_path(r'^administration/(?P<pk>\d+)/(?P<accessType>\w+)/$', login_required(artefacts_views.AdministrationArtefactDetailView.as_view()), name='publication-administration-detail'),
    re_path(r'^administration/decision/(?P<pk>\d+)/$', login_required(artefacts_views.PublicationUpdateDecision.as_view()), name='publication-administration-decision'),
    re_path(r'^administration/confirm/(?P<publication_id>\d+)/$', login_required(artefacts_views.confirmDecisionDelegatedAdmin), name='publication-administration-confirm'),
    re_path(r'^administration/reject/(?P<pk>\d+)/$', login_required(artefacts_views.PublicationDecisionReject.as_view()), name='publication-administration-reject'),
    re_path(r'^administration/delegate/(?P<pk>\d+)/$', login_required(artefacts_views.PublicationDelegateView.as_view()), name='publication-administration-delegate'),
    re_path(r'^city-autocomplete/$', artefacts_views.CityAutocomplete.as_view(), name='city-autocomplete'),
    re_path(r'^region-autocomplete/$', artefacts_views.RegionAutocomplete.as_view(), name='region-autocomplete'),
    re_path(r'^country-autocomplete/$', artefacts_views.CountryAutocomplete.as_view(), name='country-autocomplete'),
    re_path(r'^generic-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.GenericAutoComplete.as_view(create_field='name'), name='generic-autocomplete'),
    re_path(r'^date-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.GenericAutoComplete.as_view(create_field='date'), name='date-autocomplete'),
    re_path(r'^type-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.GenericAutoComplete.as_view(create_field='type'), name='type-autocomplete'),
    re_path(r'^form-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.GenericAutoComplete.as_view(create_field='form'), name='form-autocomplete'),
    re_path(r'^metal-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.GenericAutoComplete.as_view(create_field='element'), name='metal-autocomplete'),
    re_path(r'^element-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.ElementAutoCompleteFromList.as_view(), name='element-autocomplete'),
    re_path(r'^contact-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.ContactAutoComplete.as_view(), name='contact-autocomplete'),
    re_path(r'^origin-autocomplete/(?P<model>[\w-]+)/$', artefacts_views.OriginAutoComplete.as_view(), name='origin-autocomplete'),
    re_path(r'^test-ontology/$', artefacts_views.display_ontology, name='test-ontology'),
    re_path(r'^ontology/$', artefacts_views.ontology_view, name='ontology')
]
