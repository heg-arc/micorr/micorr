import rdflib
# from SPARQLWrapper import SPARQLWrapper, JSON


class Ontology:
    def __init__(self):
        self.graph = rdflib.Graph(store='SPARQLStore')
        self.graph.open("http://fuseki:3030/ds/query")

    def sparql_query(self, query: str):
        return self.graph.query(query)

    def get_labels(self):
        return self.sparql_query("""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?indiv ?classlabel ?indivlabel
    WHERE { ?indiv a ?class .
    ?indiv rdfs:label ?indivlabel .
    ?class rdfs:label ?classlabel }
    ORDER BY ?classlabel ?indiv
    """)

    def get_properties(self, text: str):
        return self.sparql_query(f"""
        PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX owl:<http://www.w3.org/2002/07/owl#>
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
        PREFIX vocab:<http://micorr.ig.he-arc.ch/vocab#>
        PREFIX ont: <http://www.co-ode.org/ontologies/ont.owl#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT ?artefact ?artefactId ?artefactType ?artefactName ?text
        WHERE {{
            {{
                ?artefact a ?artefactType .
                ?artefact rdfs:label ?artefactName .
                BIND(IRI(CONCAT(STR(?artefactType), "_id")) AS ?iriConcat)
                BIND("{text}" AS ?text)
                ?artefact ?iriConcat ?artefactId .
                FILTER (lcase(str(?artefactName)) = "{text.lower()}")
                FILTER( STRSTARTS(str(?artefactType), "http://micorr.ig.he-arc.ch/vocab") )
            }} UNION {{
                ?artefact a ?artefactType .
                ?artefact rdfs:label ?artefactName .
                BIND (COALESCE(?artefactId, "0") As ?artefactId) .
                FILTER (LCASE(STR(?artefactName)) = "{text.lower()}")
                FILTER (STRSTARTS(str(?artefactType), "http://www.w3.org/2002/07/owl#Class") )
            }}
        }}
        """)

    def get_parents(self, text: str):
        return self.sparql_query(f"""
        PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX owl:<http://www.w3.org/2002/07/owl#>
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
        PREFIX vocab:<http://micorr.ig.he-arc.ch/vocab#>
        PREFIX ont: <http://www.co-ode.org/ontologies/ont.owl#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

        SELECT ?labelSister ?labelParent
        WHERE {{
            {{
                ?artefact a vocab:{text} .
                ?artefact rdfs:label ?labelSister .
                vocab:{text} rdfs:label ?labelParent .
            }} UNION {{
                ?artefact a owl:{text} .
                ?artefact rdfs:label ?labelSister .
                BIND(COALESCE(?labelParent, "Thing") AS ?labelParent) .
            }}
        }}
    """)

    def get_property_assertions(self, text: str):
        return self.sparql_query(f"""
            PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX owl:<http://www.w3.org/2002/07/owl#>
            PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
            PREFIX vocab:<http://micorr.ig.he-arc.ch/vocab#>
            PREFIX ont: <http://www.co-ode.org/ontologies/ont.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

            SELECT ?labelAssertion
            WHERE {{
                {{
                    ?artefact rdfs:label "{text}" .
                    ?artefact ont:isLinkedTo ?linkedTo .
                    ?linkedTo rdfs:label ?labelAssertion .
                }} UNION {{
                    ?artefact rdfs:label "{text}" .
                    ?artefact ont:resultsFromTechnology ?resFromTechno .
                    ?resFromTechno rdfs:label ?labelAssertion
                }} UNION {{
                    ?artefact rdfs:label "{text}" .
                    ?artefact ont:correspondsToCorrosionForm ?resFromTechno .
                    ?resFromTechno rdfs:label ?labelAssertion
                }} UNION {{
                    ?artefact rdfs:label "{text}" .
                    ?artefact ont:isFoundInCorrosionType ?resFromTechno .
                    ?resFromTechno rdfs:label ?labelAssertion
                }} UNION {{
                    ?artefact rdfs:label "{text}" .
                    ?artefact ont:takesPlaceInChronologyCategory ?resFromTechno .
                    ?resFromTechno rdfs:label ?labelAssertion
                }}
            }}
            """)

    def get_children(self, text: str):
        return self.sparql_query(f"""
            PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
            PREFIX vocab:<http://micorr.ig.he-arc.ch/vocab#>

            SELECT ?labelChild
            WHERE {{
                ?artefact a vocab:{text} .
                ?artefact rdfs:label ?labelChild .
            }}
        """)
