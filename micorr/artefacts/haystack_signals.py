from artefacts.models import Artefact
from django.db import models
from haystack import signals


class PublishedOnlySignalProcessor(signals.RealtimeSignalProcessor):

    def handle_save(self, sender, instance, **kwargs):
        # Overrides RealtimeSignalProcessor handle_save method to address
        # removal of Artefact instance when they are unpublished

        if instance.published:
            super().handle_save(sender, instance, **kwargs)
        else:
            self.handle_delete(sender, instance, **kwargs)

    def setup(self):
        # Listen only to the ``Artefact`` model.
        models.signals.post_save.connect(self.handle_save, sender=Artefact)
        models.signals.post_delete.connect(self.handle_delete, sender=Artefact)

    def teardown(self):
        # Disconnect only for the ``Artefact`` model.
        models.signals.post_save.disconnect(self.handle_save, sender=Artefact)
        models.signals.post_delete.disconnect(self.handle_delete, sender=Artefact)
