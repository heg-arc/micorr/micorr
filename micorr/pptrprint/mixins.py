import requests
import datetime

from django.http import HttpResponse
from django.conf import settings
from django.views.generic.base import TemplateResponseMixin

from django.template.loader import get_template


def pptr_print_page(as_user, original_page_path, extra_url_params=None):
    puppeteer_url_params = {'format': 'a4',
                            'displayHeaderFooter': 'true',
                            'printBackground': True}
    if extra_url_params:
        puppeteer_url_params.update(extra_url_params)

    if as_user and as_user.is_authenticated:
        # we pass drf auth token for the user to puppeteer
        # for puppeteer to  correctly call back the view to print on behalf of the user
        from rest_framework.authtoken.models import Token
        token, created = Token.objects.get_or_create(user=as_user)
        puppeteer_url_params['Token'] = token.key
    puppeteer_url_query_string = '&'.join([f'{k}={v}' for k, v in puppeteer_url_params.items()])
    requests_response = requests.post(
        settings.PUPPETEER_BASE_URL + f'pdf/{settings.PPTRPRINT_DJANGO_URL + original_page_path}?{puppeteer_url_query_string}')
    return requests_response


class TemplatePdfResponseMixin(TemplateResponseMixin):

    def render_to_response(self, context, **response_kwargs):
        """
        overrides TemplateResponseMixin.render_to_response method
        to return a pdf rendered by puppeteer as a file attachment

        Return a response, using the `response_class` for this view, with a
        template rendered with the given context.

        Pass response_kwargs to the constructor of the response class.
        """

        response_kwargs.setdefault('content_type', self.content_type)
        send_html_data_to_puppeteer = self.request.GET.get('dataUrl', False)

        puppeteer_url_params = {'format': 'a4',
                                'displayHeaderFooter': 'true',
                                'printBackground': self.request.GET.get('printBackground', 'true')
                                }
        puppeteer_url_query_string = '&'.join([f'{k}={v}' for k, v in puppeteer_url_params.items()])

        if send_html_data_to_puppeteer:
            # render our view template with current context
            from .templatetags import static
            static.enable_absolute_url = True

            # save_static_url = settings.STATIC_URL
            save_media_url = settings.MEDIA_URL
            # settings.STATIC_URL = settings.DJANGO_CONTAINER_URL + settings.STATIC_URL
            settings.MEDIA_URL = settings.PPTRPRINT_DJANGO_URL + settings.MEDIA_URL

            template = get_template(self.template_name)  # , using='django4puppeteer')
            # but replacing static_url by absolute ones suitable for puppetter in our docker network
            # not recommended hack but no way to pass url argument to the template engine
            # so we'd need to create a custom template engine statically configured to use different URL

            html_str = template.render(context, self.request)
            if settings.DEBUG:
                with open('./micorr-output.html', 'w') as f:
                    f.write(html_str)

            static.enable_absolute_url = False

            # settings.STATIC_URL = save_static_url
            settings.MEDIA_URL = save_media_url

            requests_response = requests.post(
                settings.PUPPETEER_BASE_URL + f'pdf/data:text/html?{puppeteer_url_query_string}',
                data=html_str.encode('utf-8'))
        else:
            original_page_path = self.request.path.removesuffix('print/')

            requests_response = pptr_print_page(self.request.user, original_page_path, {'printBackground': self.request.GET.get('printBackground', 'true')})

        response = HttpResponse(requests_response.content, content_type='application/pdf')
        response[
            'Content-Disposition'] = f'attachment; filename=micorr-artefact-{self.object.id}-{datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f%z")}.pdf'
        return response
