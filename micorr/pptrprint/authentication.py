from django.contrib.auth.middleware import RemoteUserMiddleware

from django.contrib.auth.backends import ModelBackend  # RemoteUserBackend
from rest_framework.authentication import TokenAuthentication


# Token Authentication for standard django views to allow puppeteer access to private views
# on behalf of the user requesting printing

class TokenAuthenticationMiddleware(RemoteUserMiddleware):
    """
    We use standard django RemoteUserMiddleware to use DRF TokenAuthentication as a  django authentication backend
    It's necessary as the default AuthenticationMiddleware will not call authentication backends based on request headers content only.

    Here we only change the default 'REMOTE_USER' header key
    to get the  header for django rest framework TokenAuthentication
    """
    header = 'HTTP_AUTHORIZATION'


class TokenAuthenticationBackend(TokenAuthentication, ModelBackend):

    def authenticate(self, request, **credentials):
        """
        """
        # we call default TokenAuthentication which re-extract token from request
        user_and_credential = super(TokenAuthenticationBackend, self).authenticate(request)
        # and return the authenticated user
        if user_and_credential:
            return user_and_credential[0]
