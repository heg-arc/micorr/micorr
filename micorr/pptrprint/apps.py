from django.apps import AppConfig


class PPTRPrintConfig(AppConfig):
    name = 'pptrprint'
