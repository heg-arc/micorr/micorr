from urllib.parse import urljoin
from django import template
from django.templatetags.static import StaticNode, PrefixNode

register = template.Library()

enable_absolute_url = False


class AbsoluteStaticNode(StaticNode):
    @classmethod
    def handle_simple(cls, path):
        path_url = super().handle_simple(path)
        if not enable_absolute_url:
            return path_url
        else:
            return urljoin(PrefixNode.handle_simple("DJANGO_CONTAINER_URL"), path_url)


@register.tag('static')
def do_static(parser, token):
    """
    Join the given path with the STATIC_URL setting.

    Usage::

        {% static path [as varname] %}

    Examples::

        {% static "myapp/css/base.css" %}
        {% static variable_with_path %}
        {% static "myapp/css/base.css" as admin_base_css %}
        {% static variable_with_path as varname %}
    """
    return AbsoluteStaticNode.handle_token(parser, token)
