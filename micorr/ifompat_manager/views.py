import logging

import requests

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_http_methods, etag
from django.views.generic import TemplateView

from rest_framework import viewsets, status
from rest_framework import permissions

from rest_framework.response import Response
from rest_framework.decorators import action

from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

import json

from wagtail.images import get_image_model
from wagtail.images.views.serve import generate_image_url, generate_signature, ServeView, SendFileView

from micorr.ifompat_manager.forms import FeedbackForm

WImage = get_image_model()
from ifompat_manager import json_conversions
from ifompat_manager import ontology_management
import ifompat_manager.models
from ifompat_manager.serializers import WImageSerializer, MetalSerializer, ResultSerializer, PropertySerializer, \
    QuestionSerializer, AnswerSerializer, ValueSerializer, DependencyTypeSerializer, PropertyTypeSerializer, \
    ImageSerializer, PropertyValuesSerializer, DependencySerializer, OntoSerializer

class MetalViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Metal.objects.all()
    serializer_class = MetalSerializer
    permission_classes = [permissions.IsAuthenticated]

class ResultViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Result.objects.all()
    serializer_class = ResultSerializer
    permission_classes = [permissions.IsAuthenticated]

class PropertyViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Property.objects.all()
    serializer_class = PropertySerializer
    permission_classes = [permissions.IsAuthenticated]

class QuestionViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [permissions.IsAuthenticated]

class AnswerViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = [permissions.IsAuthenticated]

class ValueViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Value.objects.all()
    serializer_class = ValueSerializer
    permission_classes = [permissions.IsAuthenticated]
    filterset_fields = ['version']

class DependencyTypeViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.DependencyType.objects.all()
    serializer_class = DependencyTypeSerializer
    permission_classes = [permissions.IsAuthenticated]

class PropertyTypeViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.PropertyType.objects.all()
    serializer_class = PropertyTypeSerializer
    permission_classes = [permissions.IsAuthenticated]

class ImageViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = [permissions.IsAuthenticated]

class WImageViewSet(viewsets.ModelViewSet):
    queryset = WImage.objects.all()
    serializer_class = WImageSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        # super(WImageViewSet, self).create(request,*args, **kwargs)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


@ensure_csrf_cookie
def get_csrf(request):
    return HttpResponse( 'csrftoken')



class PropertyValuesViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.PropertyValues.objects.all()
    serializer_class = PropertyValuesSerializer
    permission_classes = [permissions.IsAuthenticated]

class DependencyViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Dependency.objects.all()
    serializer_class = DependencySerializer
    permission_classes = [permissions.IsAuthenticated]

class OntoViewSet(viewsets.ModelViewSet):
    queryset = ifompat_manager.models.Version.objects.all()
    serializer_class = OntoSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=False, methods=['get'])
    def get_latest(self, request):
        return Response(OntoSerializer(ifompat_manager.models.Version.objects.filter(done=True).order_by('-version_number')[0]).data)

    @action(detail=False, methods=['get'])
    def get_latest_wip(self, request):
        return Response(OntoSerializer(ifompat_manager.models.Version.objects.filter(done=False).order_by('-version_number')[0]).data)


def json_download(request, version):
    if request.method != 'GET':
        return HttpResponse(status=400)

    if version == 'latest':
        version = ifompat_manager.models.Version.get_latest_done().version_number
    elif version == 'wip':
        version = ifompat_manager.models.Version.get_latest().version_number

    jay = json_conversions.convert_to_json(version)
    return JsonResponse(jay, status=200)


def json_upload(request):

    if permissions.IsAuthenticated() or True:
        if request.method == 'POST':
            jay = json.loads(json.loads(request.body))

            json_conversions.convert_from_json(jay)

    return HttpResponse(status=200)

def prepare_working_onto(request):


    latest_version = ifompat_manager.models.Version.get_latest()

    # return latest version
    #data = json_conversions.convert_to_json(latest_version.version_number)
    #return JsonResponse(data, status=200 if data else 400)

    result = False
    #
    if latest_version.done and permissions.IsAuthenticated():
        result = ontology_management.create_version(latest_version.version_number, latest_version.version_number+1)
    else:
        result = True

    return HttpResponse(status=200 if result else 400)

@login_required
@require_http_methods(["PUT"])
def set_working_onto_done(request):
    latest_version = ifompat_manager.models.Version.get_latest()

    if latest_version:
        latest_version.done = True
        latest_version.save()
    return JsonResponse({'id':latest_version.id,'version_number':latest_version.version_number, 'done':latest_version.done}, status = 200)

# template views to load Ifompat angular apps
class IFOMPatNgView(TemplateView):
    template_name = 'ifompat_manager/index.html'

class IFOMPatNgAdminView(TemplateView):
    template_name = 'ifompat_manager/admin.html'

def image_etag(request, title, rendition=None):
    # we generate a fast etag not based on file content but on image title and filter only
    # so browsers will only invalidate their cache if we change our IMAGE_FILTER settings (or change our etag generation)
    rendition = rendition or settings.VISUAL_INSPECTION_IMAGE_FILTER
    return title + '.' + rendition


@etag(etag_func=image_etag)
@cache_control(must_revalidate=True, max_age=60, stale_while_revalidate=10)
@require_http_methods(["GET","HEAD"])
def render_image(request, title, rendition=None):
    """
    view to return or generate the Wagtail image rendition with our default size and quality settings
    VISUAL_INSPECTION_IMAGE_FILTER
    see https://docs.wagtail.org/en/stable/advanced_topics/images/renditions.html
    :param request:
    :param title: Image.title used to lookup the Wagtail Image object
    :param rendition:
    :return:
    """
    image = get_object_or_404(WImage, title=title)
    rendition = rendition or settings.VISUAL_INSPECTION_IMAGE_FILTER
    signature = generate_signature(image.id, rendition)
    return SendFileView.as_view()(request, signature, image.id, rendition)
    # direct serve file with django (without nginx)
    # return ServeView.as_view()(request, signature, image.id, rendition)
    #
    # redirect alternative
    # call wagtail generate_image_url (generate the rendition if necessary and get url) and redirect
    # but extra hop and not etag compatible as etags are ignored in redirects response
    # image_url = generate_image_url(image, rendition)
    # def get(self, request, signature, image_id, filter_spec):
    # return HttpResponseRedirect(image_url)

def submit_feedback_to_gitlab(request):
    """
    Forward the feedback form that the user has completed in the result page and forward it to gitlab as an issue.
    If the user attached an image, the image is first uploaded to the gitlab repository and linked in the issue
    (that's how it works, you can't add image to an issue directly).
    """
    if request.method != 'POST':
        return HttpResponse(status=405)

    form = FeedbackForm(request.POST, request.FILES)
    if not form.is_valid():
        return JsonResponse({'errors': form.errors}, status=400)

    headers = {
        'PRIVATE-TOKEN': settings.GITLAB_ACCESS_TOKEN
    }

    markdown_image_link = 'Aucune image fournie.'
    if form.cleaned_data['image']:
        files = {
            'file': form.cleaned_data['image']
        }
        response = requests.post(url=f'{settings.GITLAB_FEEDBACK_API_ENDPOINT}/uploads', headers=headers, files=files, timeout=3 * 60)
        if response.ok:
            markdown_image_link = response.json().get('markdown')
        else:
            markdown_image_link = "Une erreur est survenue lors de l'envoie de l'image à Gitlab."

    response = requests.post(url=f'{settings.GITLAB_FEEDBACK_API_ENDPOINT}/issues', headers=headers, json=form.to_gitlab_issue(markdown_image_link), timeout=2 * 60)
    return HttpResponse(status=response.status_code, content=response.text)
