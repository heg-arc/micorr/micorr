import ifompat_manager.models

import ifompat_manager.json_conversions

def create_version(copy_version, dest_version):
    base_version = ifompat_manager.models.Version.objects.get(done=True, version_number=copy_version)

    result = False
    if base_version:
        base_onto = ifompat_manager.json_conversions.convert_to_json(copy_version)
        base_onto['version'] = dest_version

        dest_version = ifompat_manager.models.Version.objects.filter(version_number=dest_version)

        if not dest_version:
            result, new_onto = ifompat_manager.json_conversions.convert_from_json(base_onto)
            new_onto.done = False
            new_onto.save()

    return result