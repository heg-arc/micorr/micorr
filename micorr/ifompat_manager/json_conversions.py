"""
Module providing functions that handles the upload and download of
a json file that follows the format of ifom-patv2

Those functions are for developing purposes, the ontology should be modified via ifompat_manager

the two functions are :
- convert_to_json that returns a json file which has the entire ontology for a given version
- convert_from_json that accepts a json file with an ifom-patv2 ontology and integrates it in the database
"""
import ifompat_manager
from django.db import transaction

import logging

logger = logging.getLogger(__name__)
def convert_to_json(version: int = -1) -> dict:
    '''
    Convert the ontology stored in postgresql attached to version in a json file
    that can be used by ifompat

    version -- the version number of the requested ontology default to the latest one
    '''

    # get version specified or latest one if version < 0
    if int(version) < 0:
        version_db = ifompat_manager.models.Version.get_latest_done()
    else:
        version_db = ifompat_manager.models.Version.objects.get(version_number=version)

    # selects of metals, questions and properties, the three pillars of the ontology
    # ordered by id for now to "respect" original json file creation order for display todo add order property and management
    metals = ifompat_manager.models.Metal.objects.filter(version=version_db).order_by('id')
    questions = ifompat_manager.models.Question.objects.filter(version=version_db).order_by('order_key', 'id')
    properties = ifompat_manager.models.Property.objects.filter(version=version_db).order_by('id')

    # Ontology scaffold
    onto = {'version': version_db.version_number, 'properties': {}, 'metals': {}, 'questions': {}, 'results': {}}

    ## PROPERTIES
    for prop in properties:
        jprop = {'values': [], 'dependencies': {'or': {}, 'and': {}}}
        jkey = prop.key

        values = prop.values.order_by('id').all()
        dependencies = prop.dependencies.order_by('id').all()

        for value in values:
            jprop['values'].append(value.value)

        for dependency in dependencies:
            jprop['dependencies'][dependency.dep_type.value][dependency.property.key] = [val.value for val in
                                                                                         dependency.values.order_by('id').all()]

        jprop['type'] = prop.property_type.value

        onto['properties'][jkey] = jprop

    ## METALS
    for metal in metals:
        jmetal = {'name': metal.name, 'link': metal.link}

        for result in metal.results.order_by('id').all():
            jresult = {'properties': {}, 'text': result.text, 'metal': metal.key, 'images': []}

            jkey = result.key

            for prop in result.properties.order_by('id').all():
                jresult['properties'][prop.property.key] = [value.value for value in prop.values.order_by('id').all()]

            for image in result.images.order_by('id').all():
                jresult['images'].append({'image': image.image, 'text': image.text, 'credits': image.credits})

            onto['results'][jkey] = jresult
        jkey = metal.key

        onto['metals'][jkey] = jmetal

    ## QUESTIONS
    for question in questions:
        jquestion = {'answers': [], 'question': {'text': question.text, 'info': question.info},
                     'type': question.type.value, 'images': [], 'dependencies': {'or': {}, 'and': {}}}
        jkey = question.key

        if question.property:
            jquestion['property'] = question.property.key

        for answer in question.answers.order_by('order_key', 'id').all():
            janswer = {'text': answer.text, 'info': answer.info, 'values': [val.value for val in answer.values.order_by('id').all()]}

            janswer['images'] = []
            for image in answer.images.order_by('id').all():
                janswer['images'].append({'image': image.image, 'credits': image.credits})

            jquestion['answers'].append(janswer)

        for image in question.images.order_by('id').all():
            jquestion['images'].append({'image': image.image, 'text': image.text})

        for dependency in question.dependencies.order_by('id').all():
            jquestion['dependencies'][dependency.dep_type.value][dependency.property.key] = [val.value for val in
                                                                                             dependency.values.order_by('id').all()]

        onto['questions'][jkey] = jquestion

    return onto


@transaction.atomic
def convert_from_json(onto: dict):
    '''
    Store the given json object that represents an ontology into the database

    onto -- a json file that follows ifom-patv2 format
    '''

    # We get the latest version and compare it to the one found in the ontology
    # there is no update if we have a more recent one

    version = onto['version']
    last_version = ifompat_manager.models.Version.get_latest_done()

    if last_version:
        last_version = last_version.version_number
    else:
        last_version = 0

    if version <= last_version:
        return False

    version_db = ifompat_manager.models.Version(version_number=version, done=True)
    version_db.save()

    db_values = None

    ### PROPERTIES

    for k, prop in onto['properties'].items():

        # We create missing types and allow a default one if missing, the metal type is the default one
        prop_type, created = ifompat_manager.models.PropertyType.objects.get_or_create(version=version_db,
            value=prop['type'] if 'type' in prop else 'metal')

        db_prop = ifompat_manager.models.Property.objects.create(version=version_db, key=k, property_type=prop_type)

        # Before any properties we need to create values, however many properties share values so we check before
        for value in prop['values']:
            val_db, created = ifompat_manager.models.Value.objects.get_or_create(version=version_db, value=value)
            db_prop.values.add(val_db)


    # Those values will be useful for the rest of the upload so we keep their references handy
    db_values = ifompat_manager.models.Value.objects.filter(version=version_db)

    db_props = ifompat_manager.models.Property.objects.filter(version=version_db)

    # The following steps may look daunting but they are actually
    # just creation of database entries with some default values
    # A serialiser would have been handy but the json format required by ifom-patv2 is not a one-to-one conversion

    ### Dependencies
    for db_prop in db_props:
        # Dependencies requires a second pass in the properties otherwise they wouldn't be created yet

        if 'dependencies' in onto['properties'][db_prop.key]:
            dependencies = onto['properties'][db_prop.key]['dependencies']

            for dep_type in dependencies:
                deps = dependencies[dep_type]

                for k, dep in deps.items():
                    db_dep_type, created = ifompat_manager.models.DependencyType.objects.get_or_create(
                        value=dep_type, version=version_db)
                    # single step Dependency create and db_prop.dependencies.add
                    db_dep = db_prop.dependencies.create(
                        dep_type=db_dep_type,
                        property=db_props.get(key=k),
                        version=version_db
                    )

                    db_dep.values.add(*db_values.filter(value__in=dep))

    ### QUESTIONS
    for index, (k, question) in enumerate(onto['questions'].items()):
        info = ''

        #### Question Type (question or transition)
        db_type, created = ifompat_manager.models.QuestionType.objects.get_or_create(
            value=question['type'] if 'type' in question else 'question', version=version_db)

        if 'info' in question['question']:
            info = question['question']['info']

        db_question = ifompat_manager.models.Question(
            text=question['question']['text'],
            info=info,
            version=version_db,
            type=db_type,
            key=k,
            order_key = index
        )

        # Link referenced property, if there's any
        if 'property' in question:
            property_key = question['property']
            try:
                db_prop = ifompat_manager.models.Property.objects.get(key=property_key,version=version_db)
            except ifompat_manager.models.Property.DoesNotExist:
                raise Exception(f"The referenced property '{property_key}' is referenced by a question but is not in "
                                f"the list of properties.")
            else:
                db_question.property = db_prop

        db_question.save()

        if 'answers' in question:
            for index, answer in enumerate(question['answers']):
                db_answer = db_question.answers.create(text=answer['text'],
                                                       info=answer['info'] if 'info' in answer else '',
                                                       version=version_db,
                                                       order_key=index)
                for val in answer['values']:
                    val_db, created = ifompat_manager.models.Value.objects.get_or_create(version=version_db, value=val)
                    db_answer.values.add(val_db)
                for image in answer['images']:
                    #todo shall we get_or_create image wich already exist with same image key but text set ? instead of recreating a new Image without text (text isn't display in answer component anyway...)
                    if not isinstance(image, str):
                        image = image['image']
                    db_answer.images.create(text='', image=image, version=version_db)

        if 'dependencies' in question:
            dependencies = question['dependencies']

            for dep_type in dependencies:
                deps = dependencies[dep_type]

                for k, dep in deps.items():
                    db_dep_type, created = ifompat_manager.models.DependencyType.objects.get_or_create(version=version_db, value=dep_type)
                    db_dep = db_question.dependencies.create(
                        dep_type=db_dep_type,
                        property=db_props.get(key=k),
                        version=version_db
                    )
                    db_dep.values.add(*db_values.filter(value__in=dep))

        if 'images' in question:
            for image in question['images']:
                text = ''
                credits = ''
                if 'text' in image:
                    text = image['text']
                if 'credits' in image:
                    credits = image['credits']
                db_question.images.create(text=text, image=image['image'], credits=credits, version=version_db)

        db_question.save()

    ### METALS

    ifompat_manager.models.Metal.objects.bulk_create(
        ifompat_manager.models.Metal(key=k, name=metal['name'], link=metal['link'], version=version_db) for k, metal in
         onto['metals'].items())


    ### RESULTS
    for k, result in onto['results'].items():
        db_metal = ifompat_manager.models.Metal.objects.get(key=result['metal'], version=version_db)

        db_result = ifompat_manager.models.Result.objects.create(key=k, text=result['text'], version=version_db, metal=db_metal)

        for prop_key, prop in result['properties'].items():
            db_prop = db_result.properties.create(property=db_props.get(key=prop_key), version=version_db)

            for val in prop:
                try:
                    db_prop.values.add(db_values.get(value=val))
                except ifompat_manager.models.Value.DoesNotExist:
                    logger.warning(f'missing value = {val} in db')


        for image in result['images']:
            text = ''
            credits = ''
            if 'text' in image:
                text = image['text']
            if 'credits' in image:
                credits = image['credits']
            db_result.images.create(text=text, image=image['image'], credits=credits, version=version_db)

    # Returns true if we uploaded
    return True, version_db
