from django import forms


class FeedbackForm(forms.Form):
    email = forms.EmailField(required=True)
    share_link = forms.URLField(required=True)
    subject = forms.CharField(required=True)
    feedback = forms.CharField(required=True)
    image = forms.ImageField(required=False)
    credits = forms.CharField(required=False)

    def to_gitlab_issue(self, markdown_image_link=""):
        if not self.is_valid():
            raise Exception("Can't convert an invalid form to an issue")

        # In markdown, one space is not enough to do a line break
        formatted_feedback = self.cleaned_data['feedback'].replace('\n', '\n\n')
        return {
            'title': self.cleaned_data['subject'],
            'description': (
                "# Informations\n"
                "### Email\n"
                f"{self.cleaned_data['email']}\n"

                "### Result\n"
                f"{self.cleaned_data['share_link']}\n"

                "# Image\n"
                f"{markdown_image_link}\n\n"
                f"credits: {self.cleaned_data['credits']}\n"

                "# Feedback / comment\n"
                f"{formatted_feedback}"
            )
        }
