from django.contrib import admin

# Register your models here.
from .models import Value, Dependency, DependencyType, PropertyType, Property, PropertyValues, Metal, Result, Answer, \
    Image, QuestionType, Question, Version

@admin.register(Value)
class ValueAdmin(admin.ModelAdmin):
    pass

admin.site.register(Dependency)
admin.site.register(DependencyType)
admin.site.register(PropertyType)
admin.site.register(Property)
admin.site.register(PropertyValues)
admin.site.register(Metal)
admin.site.register(Result)
admin.site.register(Answer)
admin.site.register(Image)
admin.site.register(QuestionType)
admin.site.register(Question)
admin.site.register(Version)


