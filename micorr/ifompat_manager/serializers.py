from django.urls import path, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from rest_framework.serializers import ModelSerializer, ListSerializer

from .models import *

from wagtail.images import get_image_model
WImage = get_image_model()

class WImageSerializer(ModelSerializer):
    class Meta:
        model = WImage
        fields = '__all__'

def manyToManyUpdateFactory(m2m_fields, remove_and_delete=False):
    """
    ManyToManyUpdate class factory
    : param m2m_fielfs: list of tuples (field_name: name of the manytomany field to handle, field_model: class of the manytomany field to handle)

    :param remove_and_delete: if False update() will only remove related instance, if True removed instance will be deleted
    :return: specialized ManyToManyUpdate class
    """

    class ManyToManyUpdate():

        def update(self, instance, validated_data):
            def add_remove_related(field_data, field_name, field_model, remove_and_delete):
                existing_records_mapping = {record.id: record for record in getattr(instance, field_name).all()}
                # data_mapping = {item['id']: item for item in field_data}
                data_list = [(item['id'], item) for item in field_data]
                for item_id, data in data_list:
                    existing_record = existing_records_mapping.get(item_id)
                    if existing_record is None:
                        # was not there
                        if item_id:  # and field_model.objects.filter(pk=item_id).exists():
                            getattr(updated_instance, field_name).add(field_model.objects.get(pk=item_id))
                        else:
                            # item_id  / data['id'] = 0 used for adding new field_model instance to field_name manyTomany
                            data.pop('id')

                            def collapse_nested(data):
                                collapsed_data = {}
                                for k, v in data.items():
                                    # if hasattr(v, 'id'):
                                    #     collapsed_data[k] = getattr(v, 'id')
                                    if isinstance(v, dict) and 'id' in v:
                                        if k == 'property':
                                            collapsed_data[k] = Property.objects.get(pk=v['id'])
                                        else:
                                            collapsed_data[k] = v['id']
                                    elif k != 'values':  # filter out values many2many empty to avoid TypeError: Direct assignment to the forward side of a many-to-many set is prohibited. Use .set() instead
                                        collapsed_data[k] = v

                                return collapsed_data

                            getattr(updated_instance, field_name).create(**collapse_nested(data))

                    else:
                        # value was there already do nothing
                        pass
                for record_id, record in existing_records_mapping.items():
                    data = next((item for item in data_list if item[0]==record_id), None)
                    if data is None:
                        # record is there in current instance but removed from data we have to remove it from ManyToMany field
                        getattr(updated_instance, field_name).remove(record)
                        if remove_and_delete:
                            record.delete()
                # updated_instance.save()

            field_data_list = []
            present_m2m_fields = []
            for i, m2m_field in enumerate(m2m_fields):
                field_name = m2m_field[0]
                if field_name in validated_data:
                    field_data_list.append(validated_data.pop(field_name))
                    present_m2m_fields.append(m2m_field)

            updated_instance = super().update(instance, validated_data)

            for i, m2m_field in enumerate(present_m2m_fields):
                field_name=m2m_field[0]
                field_model=m2m_field[1]
                field_data = field_data_list[i]
                add_remove_related(field_data, field_name, field_model, remove_and_delete)
            return updated_instance

        def create(self, validated_data):
            field_data_list = []
            for i, m2m_field in enumerate(m2m_fields):
                field_name = m2m_field[0]
                field_data_list.append(validated_data.pop(field_name))

            if 'id' in validated_data and not validated_data['id']:
                validated_data.pop('id')

            created_instance = super().create(validated_data)
            for i, m2m_field in enumerate(m2m_fields):
                field_name = m2m_field[0]
                field_model = m2m_field[1]
                field_data = field_data_list[i]
                if len(field_data):
                    getattr(created_instance, field_name).add(*field_data)
            return created_instance

    return ManyToManyUpdate

class ValueListSerializer(ListSerializer):

    def update(self, instance, validated_data):
        # Maps for id->instance and id->data item.
        value_mapping = {value.id: value for value in instance}
        data_mapping = {item['id']: item for item in validated_data}

        # Perform creations and updates.
        ret = []
        for value_id, data in data_mapping.items():
            value = value_mapping.get(value_id, Value.objects.get(pk=value_id))
            if value is None:
                ret.append(self.child.create(data))
            else:
                ret.append(self.child.update(value, data))

        # Perform deletions.
        # for value_id, value in value_mapping.items():
        #     if value_id not in data_mapping:
        #         value.delete()

        return ret

class ValueSerializer(ModelSerializer):
    # override default read-only id definition to have it in validated_data for nested serializer update
    id = serializers.IntegerField()
    class Meta:
        model = Value
        list_serializer_class = ValueListSerializer

        fields = ['id', 'version', 'value']

class DependencyTypeSerializer(ModelSerializer):
    class Meta:
        model = DependencyType
        fields = ['id', 'version', 'value']

class PropertyTypeSerializer(ModelSerializer):
    class Meta:
        model = PropertyType
        fields = ['id', 'version', 'value']

class QuestionTypeSerializer(ModelSerializer):
    class Meta:
        model = QuestionType
        fields = ['id', 'version', 'value']

class ImageSerializer(ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Image
        fields = ['id', 'version', 'image', 'text', 'credits']


class DependencySerializer(manyToManyUpdateFactory([('values', Value)]), ModelSerializer):
    id = serializers.IntegerField()
    values = ValueSerializer(many=True, read_only=False)
    class Meta:
        model = Dependency
        fields = ['id', 'version', 'dep_type', 'property', 'values']


class PropertySerializer(manyToManyUpdateFactory([('values', Value), ('dependencies', Dependency)]), ModelSerializer):
    values = ValueListSerializer(child= ValueSerializer(), read_only=False)
    dependencies = DependencySerializer(many=True, read_only=False)
    # override default read-only id definition to have it in validated_data for nested serializer update
    id = serializers.IntegerField()
    class Meta:
        model = Property
        fields = ['id', 'version', 'key', 'values', 'dependencies', 'property_type']


class AnswerSerializer(manyToManyUpdateFactory([('values', Value), ('images', Image)]), ModelSerializer):
    values = ValueSerializer(many=True, read_only=False)
    images = ImageSerializer(many=True, read_only=False)
    # override default read-only id definition to have it in validated_data for nested serializer update
    id = serializers.IntegerField()
    class Meta:
        model = Answer
        fields = ['id', 'order_key', 'version', 'values', 'text', 'info', 'images']


class QuestionSerializer(
    manyToManyUpdateFactory([('answers', Answer), ('images', Image), ('dependencies', Dependency)]), ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=False)
    images = ImageSerializer(many=True, read_only=False)
    dependencies = DependencySerializer(many=True, read_only=False)

    class Meta:
        model = Question
        fields = ['id', 'order_key', 'key', 'version', 'property', 'text', 'info', 'answers', 'type', 'images', 'dependencies']

class PropertyValuesSerializer(ModelSerializer):
    property = PropertySerializer(read_only=False)
    values = ValueListSerializer(child=ValueSerializer(), read_only=False)
    # override default read-only id definition to have it in validated_data for nested serializer update
    id = serializers.IntegerField()
    class Meta:
        model = PropertyValues
        fields = ['id', 'version', 'property', 'values']

    def update(self, instance, validated_data):

        values_data = validated_data.pop('values')
        # drop property field that is in fact read only
        validated_data.pop('property')
        property_values = super().update(instance, validated_data)
        # value_serializer = self.fields['values']
        # values = instance.values
        # value_serializer.update(values.all(),values_data)

        value_mapping = {value.id: value for value in instance.values.all()}
        data_mapping = {item['id']: item for item in values_data}

        for value_id, data in data_mapping.items():
            value = value_mapping.get(value_id)
            if value is None:
                # was not there
                if value_id: #  and Value.objects.filter(pk=value_id).exists():
                    property_values.values.add(Value.objects.get(pk=value_id))
                else:
                    property_values.values.create(data)
            else:
                # value was there already do nothing
                pass
        for value_id, value in value_mapping.items():
            data = data_mapping.get(value_id)
            if data is None:
                # value is there in current instance but removed from data we have to remove it from ManyToMany field
                property_values.values.remove(value)
        property_values.save()
        return property_values

    def create(self, validated_data):
        values_data = validated_data.pop('values')
        property_values = super().create(validated_data)
        if len(values_data):
            property_values.values.add(*values_data)
        return property_values

class ResultSerializer(manyToManyUpdateFactory([('properties', PropertyValues),('images', Image)], remove_and_delete=True), ModelSerializer):
    properties = PropertyValuesSerializer(many=True, read_only=False)
    images = ImageSerializer(many=True, read_only=False)

    class Meta:
        model = Result
        fields = ['id', 'version', 'metal', 'key', 'text', 'properties', 'images']


class MetalSerializer(ModelSerializer):

    results = ResultSerializer(many=True, read_only=True)
    class Meta:
        model = Metal
        fields = ['id', 'name', 'key', 'link', 'version', 'results']


class OntoSerializer(ModelSerializer):
    properties = PropertySerializer(many=True, read_only=False)
    questions = QuestionSerializer(many=True)
    metals = MetalSerializer(many=True)
    property_types = PropertyTypeSerializer(many=True, read_only=True)
    question_types = QuestionTypeSerializer(many=True, read_only=True)
    dependency_types = DependencyTypeSerializer(many=True, read_only=True)

    class Meta:
        model = Version
        fields = ['id', 'done', 'version_number', 'properties', 'questions', 'metals', 'property_types', 'question_types', 'dependency_types']
