from django.db import models

class Version(models.Model):
    version_number = models.IntegerField(default=0)
    done = models.BooleanField(default=False)

    @classmethod
    def get_latest_done(cls):
        return cls.objects.filter(done=True).order_by('-version_number').first()

    @classmethod
    def get_latest(cls):
        return cls.objects.all().order_by('-version_number').first()


class Value(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='values')
    value = models.TextField(default='')

class Dependency(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='dependencies')
    dep_type = models.ForeignKey('DependencyType', models.CASCADE)
    property = models.ForeignKey('Property', models.CASCADE, related_name='prop_deps')
    values = models.ManyToManyField('Value')

class DependencyType(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='dependency_types')
    value = models.TextField(default='')

class PropertyType(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='property_types')
    value = models.TextField(default='')

class Property(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='properties')
    key = models.TextField(default='')
    values = models.ManyToManyField('Value')
    dependencies = models.ManyToManyField('Dependency', related_name='dep_props')
    property_type = models.ForeignKey('PropertyType', models.CASCADE)

    class Meta:
        ordering = ['key', 'id']

class PropertyValues(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='property_values')
    property = models.ForeignKey('Property', models.CASCADE)
    values = models.ManyToManyField('Value')

    class Meta:
        ordering = ['property__key', 'id']

class Metal(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='metals')
    key = models.TextField(default='')
    name = models.TextField(default='')
    link = models.TextField(default='')
    class Meta:
        ordering = ['name', 'key']


class Result(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='results')
    metal = models.ForeignKey('Metal', models.CASCADE, related_name='results')
    key = models.TextField(default='')
    text = models.TextField(default='')
    properties = models.ManyToManyField('PropertyValues')
    images = models.ManyToManyField('Image')
    class Meta:
        ordering = ['text', 'key']


class Answer(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='answers')
    values = models.ManyToManyField('Value')
    text = models.TextField(default='')
    info = models.TextField(default='', null=True, blank=True)
    images = models.ManyToManyField('Image')
    order_key = models.IntegerField(default=-1)

    class Meta:
        ordering = ['order_key', 'text']


class Image(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='images')
    image = models.TextField(default='')
    text = models.TextField(default='', blank=True)
    credits = models.TextField(default='', blank=True)

class QuestionType(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='question_types')

    value = models.TextField(default='')

class Question(models.Model):
    version = models.ForeignKey('Version', models.CASCADE, related_name='questions')
    property = models.ForeignKey('Property', models.CASCADE, blank=True, null=True)
    type = models.ForeignKey('QuestionType', models.CASCADE)
    order_key = models.IntegerField(default=-1)

    key = models.TextField(default='')

    text = models.TextField(default='')
    info = models.TextField(default='', blank=True)
    dependencies = models.ManyToManyField('Dependency', related_name='dep_questions')
    answers = models.ManyToManyField('Answer')
    images = models.ManyToManyField('Image')
    class Meta:
        ordering = ['order_key', 'text', 'key']

