from django.apps import AppConfig


class IfompatManagerConfig(AppConfig):
    name = 'ifompat_manager'
