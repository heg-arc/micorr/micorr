from django.http import JsonResponse
from django.urls import path, include

from ifompat_manager import views

from rest_framework import routers

from micorr.ifompat_manager.views import IFOMPatNgView, IFOMPatNgAdminView, get_csrf, render_image, submit_feedback_to_gitlab

router = routers.DefaultRouter()

router.register(r'ontologies', views.OntoViewSet)

router.register(r'metals', views.MetalViewSet)
router.register(r'property_values', views.PropertyValuesViewSet)
router.register(r'results', views.ResultViewSet)

router.register(r'questions', views.QuestionViewSet)
router.register(r'answers', views.AnswerViewSet)

router.register(r'properties', views.PropertyViewSet)
router.register(r'property_types', views.PropertyTypeViewSet)
router.register(r'dependencies', views.DependencyViewSet)
router.register(r'dependency_types', views.DependencyTypeViewSet)

router.register(r'values', views.ValueViewSet)

router.register(r'images', views.ImageViewSet, basename='ifompat_manager_image')
router.register(r'wimages', views.WImageViewSet)


from django.views.decorators.csrf import csrf_exempt

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

app_name = 'ifompat_manager'

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('json-download/', csrf_exempt(views.json_download)),
    path('json-download/<version>', csrf_exempt(views.json_download)),
    path('json-upload/', csrf_exempt(views.json_upload)),
    path('prepare_working_onto/', csrf_exempt(views.prepare_working_onto)),
    path('set_working_onto_done/', csrf_exempt(views.set_working_onto_done)),
    path('home/', IFOMPatNgView.as_view(), name='ifompat_home'),
    path('admin/', IFOMPatNgAdminView.as_view(), name='ifompat_admin'),
    path('csrf/', get_csrf, name='ifompat_csrf'),
    path('render_image/<title>', render_image),
    path('feedback/', csrf_exempt(submit_feedback_to_gitlab))
]


