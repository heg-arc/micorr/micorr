# -*- coding: utf-8 -*-


from django.views.generic import TemplateView
from django.views import defaults as default_views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path
from .views import HomePageView
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from wagtail.contrib.sitemaps.views import sitemap as sitemap_wagtail
from wagtail import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls
from wagtail.images.views.serve import ServeView


from config.sitemaps import ArtefactSitemap, HomePageSitemap

sitemaps = {
    'artefact': ArtefactSitemap,
    'home': HomePageSitemap,
}

urlpatterns = [
    re_path(r'^$', HomePageView.as_view(), name="home"),

    # Uncomment the next line to enable the admin:
    path(settings.ADMIN_URL, admin.site.urls),

    # User management
    re_path(r'^users/', include("users.urls", namespace="users")),
    re_path(r'^accounts/', include('allauth.urls')),
    path('captcha/', include('captcha.urls')),

    # Uncomment the next line to enable avatars
    re_path(r'^avatar/', include('avatar.urls')),

    # Newsletter
    #re_path(r'^newsletter/', include('newsletter.urls')),
    re_path(r'^tinymce/', include('tinymce.urls')),

    # Envelope (contact form)
    #re_path(r'^contact/', include('envelope.urls')),

    # Your stuff: custom urls go here
    #re_path(r'^dev/', include('deployment.urls')),

    # Artefacts management
    re_path(r'^artefacts/', include('artefacts.urls')),

    # Contacts management
    re_path(r'^contacts/', include('contacts.urls')),

    # Django-terms app
    # re_path(r'^terms/', include('terms.urls')),

    # Sitemaps
    re_path(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
    name='django.contrib.sitemaps.views.sitemap'),

    re_path(r'^sitemap-wagtail\.xml$', sitemap_wagtail),

    # Robots
    re_path(r'^robots\.txt', include('robots.urls')),

    # Stratigraphies
    re_path(r'^micorr/', include('stratigraphies.urls', namespace="stratigraphies")),
    re_path(r'^visual_inspection/', include('ifompat_manager.urls', namespace='ifompat_manager')),
    re_path(r'^reviews/', include('reviews.urls', namespace='reviews'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    import debug_toolbar
    urlpatterns += [
        re_path(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        re_path(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        re_path(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        re_path(r'^500/$', default_views.server_error),
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ]

# Wagtail URLS
urlpatterns += [
    re_path(r'^w/admin/', include(wagtailadmin_urls)),
    re_path(r'^w/documents/', include(wagtaildocs_urls)),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's serving mechanism
    re_path(r'^images/([^/]*)/(\d*)/([^/]*)/[^/]*$', ServeView.as_view(), name='wagtailimages_serve'),
    re_path(r'', include(wagtail_urls)),
]

