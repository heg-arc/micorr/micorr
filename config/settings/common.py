# -*- coding: utf-8 -*-
"""
Django settings for micorr project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""


import os

import environ

ROOT_DIR = environ.Path(__file__) - 3  # (micorr/config/settings/common.py - 3 = micorr/)
APPS_DIR = ROOT_DIR.path('micorr')

env = environ.Env()

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.forms',
    # Useful template tags:
    # 'django.contrib.humanize',
    'dal',
    'dal_select2',
    'dal_legacy_static',
    # Admin
    'django.contrib.admin',
)
THIRD_PARTY_APPS = (
    'crispy_bootstrap3',
    'crispy_forms',  # Form layouts
    'avatar',  # for user avatars
    'allauth',  # registration
    'allauth.account',  # registration
    'allauth.socialaccount',  # registration
    'tinymce',
    'sorl.thumbnail',
    # 'newsletter',
    #'envelope',
    'cities_light',
    'django_filters',
    'haystack',
    'terms',
    # wagtail apps
    'compressor',
    'taggit',
    'modelcluster',
    'wagtail',
    'wagtail.admin',
    'wagtail.documents',
    'wagtail.snippets',
    'wagtail.users',
    'wagtail.images',
    'wagtail.embeds',
    'wagtail.search',
    'wagtail.contrib.forms',
    'wagtail.contrib.redirects',
    'wagtail.contrib.sitemaps',
    'wagtail.contrib.settings',
    'wagtail.contrib.modeladmin',
    'wagtail.sites', #added in wagtail v0.7...required in v2.0
    'wagtailnews',
    'wagtail.contrib.routable_page',
    # end wagtail apps
    'robots',
    'simplejson',
    'django_extensions',
    'captcha',
    'rest_framework',
    'corsheaders',
    'rest_framework.authtoken'
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'micorr.users.apps.UsersConfig',
    'pptrprint', # puppeteer / Django integration
    'artefacts',
    'contacts',
    'documents', # wagtail implementation
    'stratigraphies',
    'ifompat_manager', # visual_inspection backend and admin
    'reviews' # Editorial process reviews
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE = (
    "corsheaders.middleware.CorsMiddleware",
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'allauth.account.middleware.AccountMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'micorr.pptrprint.authentication.TokenAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'wagtail.contrib.redirects.middleware.RedirectMiddleware'
)

# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
    #'sites': 'micorr.contrib.sites.migrations'
}

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('MiCorr', 'micorr@he-arc.ch'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
    'default': env.db('DATABASE_URL', default='postgres://micorr@postgres:5432/micorr'),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# Starting with Django 3.2 we need to define type of auto created primary keys
# HINT: Configure the DEFAULT_AUTO_FIELD setting or the ArtefactsConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
# new default is BigAutoField (64 bits) as a first step without migration we keep it to 32 bits
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'


# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Zurich'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

WAGTAILADMIN_BASE_URL = 'w/admin/'
WAGTAIL_SITE_NAME = 'MiCorr'


# Draftail fixed Hallo.js removed and in wagtail 3.0 see
# wagtail issue https://github.com/wagtail/wagtail/issues/4602
# https://github.com/wagtail/wagtail/commit/2bec229e120c744ca13e79313e3c34c8503af9f1
WAGTAILADMIN_RICH_TEXT_EDITORS = {
    'default': {
        'WIDGET': 'wagtail.admin.rich_text.DraftailRichTextArea'
    }
}

# https://docs.wagtail.org/en/stable/releases/2.15.html#upgrade-considerations
# change to  new (as of wagtail 2.15) postgres aware search backend
WAGTAILSEARCH_BACKENDS = {
    'default': {
        'BACKEND': 'wagtail.search.backends.database',
        'SEARCH_CONFIG': 'english',
    }
}

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = False
DATETIME_FORMAT = "Y-m-d H:i:s"

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                #'allauth.account.context_processors.account',
                #'allauth.socialaccount.context_processors.socialaccount',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                # Your stuff: custom template context processors go here
                'dealer.contrib.django.context_processor',
            ],
        },
    }
]

# See: http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(APPS_DIR.path('static')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'micorr.pptrprint.authentication.TokenAuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Some really nice defaults
ACCOUNT_AUTHENTICATION_METHOD = 'username'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'

ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)
ACCOUNT_ADAPTER = 'micorr.users.adapters.AccountAdapter'
SOCIALACCOUNT_ADAPTER = 'micorr.users.adapters.SocialAccountAdapter'

ACCOUNT_FORMS = {'signup': 'micorr.users.forms.CaptchaSignupForm',  # default allauth.account.forms.SignupForm
                 }
# https://django-simple-captcha.readthedocs.io/en/latest/advanced.html#configuration-toggles
CAPTCHA_FONT_SIZE = 28 # default 22
CAPTCHA_LENGTH = 5 # default 4

# Custom user app defaults
# Select the correct user model
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = 'users:redirect'
LOGIN_URL = 'account_login'

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'


# django-compressor
# ------------------------------------------------------------------------------


# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = 'admin/'


# Your common stuff: Below this line define 3rd party library settings
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch7_backend.Elasticsearch7SearchEngine',
        'URL': 'http://elasticsearch:9200/',
        'INDEX_NAME': 'haystack',
        #'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
}
# cause haystack/elasticsearch index to be updated in realtime (inside request/response cycle) on indexed model save/delete
HAYSTACK_SIGNAL_PROCESSOR = 'artefacts.haystack_signals.PublishedOnlySignalProcessor'

NODE_BASE_URL = env('NODE_BASE_URL', default='http://node:8080/')

PUPPETEER_BASE_URL = env('PUPPETEER_BASE_URL', default='http://puppeteer:3000/')

NEO4J_AUTH = env('NEO4J_AUTH', default='neo4j:secret') #no default provided as it must be set in environment variable for direct use by py2neo
NEO4J_HOST = env('NEO4J_HOST', default='neo4j')
NEO4J_PROTOCOL = env('NEO4J_PROTOCOL', default='bolt')

NOTEBOOK_ARGUMENTS = [ '--notebook-dir', 'notebooks']

IPYTHON_ARGUMENTS = [
    '--ext', 'django_extensions.management.notebook_extension',
    '--debug',
]

# Terms template filter settings
TERMS_ENABLED = False
TERMS_REPLACE_FIRST_ONLY = False

#DJANGO_TINYMCE
# see http://django-tinymce.readthedocs.io/en/latest/installation.html#configuration
# and https://www.tinymce.com/docs/configure/integration-and-setup/
TINYMCE_JS_URL = os.path.join(STATIC_URL, "js/tinymce/tinymce.min.js")
TINYMCE_DEFAULT_CONFIG = {"theme": "modern", "relative_urls": False, "statusbar": False, "plugins": "table autoresize",
                          "menubar":"file edit insert view format table tools help",
                          "autoresize_min_height":100, "autoresize_max_height": 500, "autoresize_bottom_margin":10, "autoresize_on_init":True }
TINYMCE_COMPRESSOR = False
TINYMCE_JS_ROOT = os.path.join(STATIC_URL, "js/tinymce")

# django-avatar settings
# default image generated when  there no local avatar image nor gravatar image associated with user's email
# https://en.gravatar.com/site/implement/images/#default-image
AVATAR_GRAVATAR_DEFAULT = 'retro'  # mp, identicon, monsterid, wavatar, retro, robohash, blank
AVATAR_AUTO_GENERATE_SIZES = [80, 35]
# settings AVATAR_THUMB_FORMAT to PNG actually fixes no resize issue with png upload and still allows jpg
# see https://github.com/grantmcconnaughey/django-avatar/issues/153 and comment in https://github.com/grantmcconnaughey/django-avatar/pull/180
AVATAR_THUMB_FORMAT = "PNG"

# ifompat_manager  django rest settings
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_AUTHENTICATION_CLASSES': [
        # 'rest_framework.authentication.SessionAuthentication',
        'ifompat_manager.fake_auth.CsrfExemptSessionAuthentication',
        'ifompat_manager.fake_auth.FakeAuthentication'
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissions'
    ],
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend']
}
from corsheaders.defaults import default_headers

CORS_ALLOW_HEADERS = list(default_headers) + [
    "x-username"
]

CORS_ALLOWED_ORIGINS = [
    "http://127.0.0.1:4200",
    "http://localhost:4200",
]

# default wagtail image rendition filter for images displayed in ifompat_v2 / search by visual inspection
# https://docs.wagtail.org/en/stable/advanced_topics/images/renditions.html
VISUAL_INSPECTION_IMAGE_FILTER = env('VISUAL_INSPECTION_IMAGE_FILTER', default='width-500|jpegquality-80')


# used by wagtail dynamic image server views
# https://docs.wagtail.org/en/stable/advanced_topics/images/image_serve_view.html#image-serve-view-sendfile
# requires django_sendfile2 https://github.com/moggers87/django-sendfile2
SENDFILE_BACKEND='django_sendfile.backends.nginx'
SENDFILE_ROOT = '/app/micorr/media'
SENDFILE_URL = '/resized'

# Gitlab issues
GITLAB_ACCESS_TOKEN = env('GITLAB_ACCESS_TOKEN')
GITLAB_FEEDBACK_API_ENDPOINT = env('GITLAB_FEEDBACK_API_ENDPOINT')
