// min sample test script
const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  // https://micorr.org/artefacts/979/'
  //'http://host.docker.internal:8000/artefacts/979/'
  await page.goto('https://micorr.org/artefacts/979/', {
    waitUntil: 'networkidle2',
  });
  await page.pdf({path: './output/artefact.pdf',format:'a4', printBackground: true, landscape:false}); //  preferCSSPageSize:true,

  await browser.close();
})();
