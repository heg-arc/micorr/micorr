// minimal node js service rendering html pages using puppeteer -> chrome headless
// based on extracts from index.js at https://github.com/cheeaun/puppetron

const http = require('http');
const {URL} = require('url');
const puppeteer = require('puppeteer');
const pTimeout = require('p-timeout');

const {DEBUG, HEADFUL, CHROME_BIN, PORT} = process.env;
const hostname = '0.0.0.0';
const port = 3000;


const blocked = require('./blocked.json');
const blockedRegExp = new RegExp('(' + blocked.join('|') + ')', 'i');

const truncate = (str, len) => str.length > len ? str.slice(0, len) + '…' : str;

let browser;

const server = http.createServer(async (req, res) => {

    if (req.url === '/') {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('usage /action/url  (action = pdf|screenshot|render)\n');
        return;
    }
    const [_, action, url] = req.url.match(/^\/(screenshot|render|pdf)?\/?(.*)/i) || ['', '', ''];

    if (!url) {
        res.writeHead(400, {
            'content-type': 'text/plain',
        });
        res.end('Something is wrong. Missing URL.');
        return;
    }
    let page, pageURL, data;
    try {
        if (req.method=='GET' && !/^https?:\/\//i.test(url)) {
            throw new Error('Invalid URL');
        }

        const {origin, hostname, port, pathname, searchParams} = new URL(url);
        const path = decodeURIComponent(pathname);
        console.log(`⏳ checking url hostname=${hostname} port:${port} path=${path} params=${searchParams}`);
        // check if url serves a html page
        if (req.method=='GET')
        await new Promise((resolve, reject) => {
            const req = http.request({
                method: 'HEAD',
                host: hostname,
                port:port,
                path,
            }, ({statusCode, headers}) => {
                if (!headers || (statusCode == 200 && !/text\/html/i.test(headers['content-type']))) {
                    reject(new Error('Not a HTML page'));
                } else {
                    resolve();
                }
            });
            req.on('error', reject);
            req.end();
        });
        else {
            await new Promise((resolve, reject) => {
                data = '';
                req.on('data', chunk => {
                    console.log(`⬇ POST data ${chunk.toString().length} bytes received`);
                    data += chunk.toString()
                });
                req.on('end', () => {
                    console.log(`✅ POST end ${data.length} total bytes received`);
                    console.log(`➡️ ${truncate(data, 100)}...`);
                    resolve();
                });
            });
        }
        pageURL = origin + path;
        let actionDone = false;
        const width = parseInt(searchParams.get('width'), 10) || 1024;
        const height = parseInt(searchParams.get('height'), 10) || 768;
        const token = searchParams.get('Token') || null;

        if (!page) {
            if (!browser) {
                console.log('🚀 Launch browser!');
                const config = {
                    ignoreHTTPSErrors: true,
                    args: [
                        '--no-sandbox',
                        '--disable-setuid-sandbox',
                        '--disable-dev-shm-usage',
                        '--enable-features=NetworkService',
                        '-—disable-dev-tools'
                    ],
                    devtools: false,
                };
                if (DEBUG) config.dumpio = true;
                if (HEADFUL) {
                    config.headless = false;
                    config.args.push('--auto-open-devtools-for-tabs');
                }
                if (CHROME_BIN) config.executablePath = CHROME_BIN;
                browser = await puppeteer.launch(config);
            }

            if (token) {
                // when we receive a token to connect back to server page on behalf of user
                // we use incognito browser to avoid storage of authentication in session and  possible
                // subsequent authentication without token
                const context = await browser.createIncognitoBrowserContext()
                page = await context.newPage();
                await page.setExtraHTTPHeaders({'Authorization': `Token ${token}`})
                console.log(`✅ Added Authorization Token:${token}`)
            }
            else
               page = await browser.newPage();


            const nowTime = +new Date();
            let reqCount = 0;
            await page.setRequestInterception(true);
            page.on('request', (request) => {
                const url = request.url();
                const method = request.method();
                const resourceType = request.resourceType();

                // Skip data URIs
                if (/^data:/i.test(url)) {
                    request.continue();
                    return;
                }

                const seconds = (+new Date() - nowTime) / 1000;
                const shortURL = truncate(url, 70);
                const otherResources = /^(manifest|other)$/i.test(resourceType);
                // Abort requests that exceeds 15 seconds
                // Also abort if more than 100 requests
                if (seconds > 15 || reqCount > 100 || actionDone) {
                    console.log(`❌⏳ ${method} ${shortURL}`);
                    request.abort();
                } else if (blockedRegExp.test(url) || otherResources) {
                    console.log(`❌ ${method} ${shortURL}`);
                    request.abort();
                } else {
                    console.log(`✅ ${method} ${shortURL}`);
                    request.continue();
                    reqCount++;
                }
            });

            let responseReject;
            const responsePromise = new Promise((_, reject) => {
                responseReject = reject;
            });
            page.on('response', ({headers}) => {
                const location = headers['location'];
                if (location && location.includes(host)) {
                    responseReject(new Error('Possible infinite redirects detected.'));
                }
            });

            await page.setViewport({
                width,
                height,
            });
            if (req.method=='POST' && data.length)
                pageURL = `data:text/html;charset=UTF-8,${encodeURIComponent(data)}`;
            console.log('⬇️ Fetching ' + truncate(pageURL,180));
            await Promise.race([
                responsePromise,
                page.goto(pageURL, {
                    waitUntil: 'networkidle2',
                    timeout: 60000 //default is 30000
                })
            ]);

            // Pause all media and stop buffering
            page.frames().forEach((frame) => {
                frame.evaluate(() => {
                    document.querySelectorAll('video, audio').forEach(m => {
                        if (!m) return;
                        if (m.pause) m.pause();
                        m.preload = 'none';
                    });
                });
            });
        } else {
            await page.setViewport({
                width,
                height,
            });
        }

        console.log('💥 Perform action: ' + action);
        switch (action) {
            case 'pdf': {
                const format = searchParams.get('format') || null;
                const pageRanges = searchParams.get('pageRanges') || '';
                const displayHeaderFooter = searchParams.get('displayHeaderFooter')==='true';
                const printBackground = searchParams.get('printBackground')==='true';


                const pdf = await pTimeout(page.pdf({
                    format,
                    pageRanges,
                    margin: {
                        top: '50px',
                        bottom: '64px',
                        right: '30px',
                        left: '30px',
                      },
                    displayHeaderFooter,
                    printBackground,
                    headerTemplate: '<span></span>',
                    footerTemplate:`
                            <div style="width:100%; background: yellow; padding-top:4px; padding-bottom:4px;  font-size:8px;color:black; margin-top:4px; display: flex; flex-wrap:wrap; flex-direction: row; align-items: center; justify-content: space-evenly;">
                                <span class="title"></span>
                                <div>
                                    <span>P.</span><span class="pageNumber"></span>/<span class="totalPages"></span>
                                </div>
                                <div style="width:100%; font-size:8px;text-align:center">This work is licensed under
                                    <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1" target="_blank"
                                       rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-ND 4.0
                                    </a>
                                    <img style="height:11px!important;margin-left:3px;vertical-align:text-bottom;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxMy4wLjIsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDE0OTQ4KSAgLS0+DQo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMC8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9UUi8yMDAxL1JFQy1TVkctMjAwMTA5MDQvRFREL3N2ZzEwLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMCIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4IiB2aWV3Qm94PSI1LjUgLTMuNSA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyA1LjUgLTMuNSA2NCA2NCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Y2lyY2xlIGZpbGw9IiNGRkZGRkYiIGN4PSIzNy43ODUiIGN5PSIyOC41MDEiIHI9IjI4LjgzNiIvPg0KCTxwYXRoIGQ9Ik0zNy40NDEtMy41YzguOTUxLDAsMTYuNTcyLDMuMTI1LDIyLjg1Nyw5LjM3MmMzLjAwOCwzLjAwOSw1LjI5NSw2LjQ0OCw2Ljg1NywxMC4zMTQNCgkJYzEuNTYxLDMuODY3LDIuMzQ0LDcuOTcxLDIuMzQ0LDEyLjMxNGMwLDQuMzgxLTAuNzczLDguNDg2LTIuMzE0LDEyLjMxM2MtMS41NDMsMy44MjgtMy44Miw3LjIxLTYuODI4LDEwLjE0Mw0KCQljLTMuMTIzLDMuMDg1LTYuNjY2LDUuNDQ4LTEwLjYyOSw3LjA4NmMtMy45NjEsMS42MzgtOC4wNTcsMi40NTctMTIuMjg1LDIuNDU3cy04LjI3Ni0wLjgwOC0xMi4xNDMtMi40MjkNCgkJYy0zLjg2Ni0xLjYxOC03LjMzMy0zLjk2MS0xMC40LTcuMDI3Yy0zLjA2Ny0zLjA2Ni01LjQtNi41MjQtNy0xMC4zNzJTNS41LDMyLjc2Nyw1LjUsMjguNWMwLTQuMjI5LDAuODA5LTguMjk1LDIuNDI4LTEyLjINCgkJYzEuNjE5LTMuOTA1LDMuOTcyLTcuNCw3LjA1Ny0xMC40ODZDMjEuMDgtMC4zOTQsMjguNTY1LTMuNSwzNy40NDEtMy41eiBNMzcuNTU3LDIuMjcyYy03LjMxNCwwLTEzLjQ2NywyLjU1My0xOC40NTgsNy42NTcNCgkJYy0yLjUxNSwyLjU1My00LjQ0OCw1LjQxOS01LjgsOC42Yy0xLjM1NCwzLjE4MS0yLjAyOSw2LjUwNS0yLjAyOSw5Ljk3MmMwLDMuNDI5LDAuNjc1LDYuNzM0LDIuMDI5LDkuOTEzDQoJCWMxLjM1MywzLjE4MywzLjI4NSw2LjAyMSw1LjgsOC41MTZjMi41MTQsMi40OTYsNS4zNTEsNC4zOTksOC41MTUsNS43MTVjMy4xNjEsMS4zMTQsNi40NzYsMS45NzEsOS45NDMsMS45NzENCgkJYzMuNDI4LDAsNi43NS0wLjY2NSw5Ljk3My0xLjk5OWMzLjIxOS0xLjMzNSw2LjEyMS0zLjI1Nyw4LjcxMy01Ljc3MWM0Ljk5LTQuODc2LDcuNDg0LTEwLjk5LDcuNDg0LTE4LjM0NA0KCQljMC0zLjU0My0wLjY0OC02Ljg5NS0xLjk0My0xMC4wNTdjLTEuMjkzLTMuMTYyLTMuMTgtNS45OC01LjY1NC04LjQ1OEM1MC45ODQsNC44NDQsNDQuNzk1LDIuMjcyLDM3LjU1NywyLjI3MnogTTM3LjE1NiwyMy4xODcNCgkJbC00LjI4NywyLjIyOWMtMC40NTgtMC45NTEtMS4wMTktMS42MTktMS42ODUtMmMtMC42NjctMC4zOC0xLjI4Ni0wLjU3MS0xLjg1OC0wLjU3MWMtMi44NTYsMC00LjI4NiwxLjg4NS00LjI4Niw1LjY1Nw0KCQljMCwxLjcxNCwwLjM2MiwzLjA4NCwxLjA4NSw0LjExM2MwLjcyNCwxLjAyOSwxLjc5MSwxLjU0NCwzLjIwMSwxLjU0NGMxLjg2NywwLDMuMTgxLTAuOTE1LDMuOTQ0LTIuNzQzbDMuOTQyLDINCgkJYy0wLjgzOCwxLjU2My0yLDIuNzkxLTMuNDg2LDMuNjg2Yy0xLjQ4NCwwLjg5Ni0zLjEyMywxLjM0My00LjkxNCwxLjM0M2MtMi44NTcsMC01LjE2My0wLjg3NS02LjkxNS0yLjYyOQ0KCQljLTEuNzUyLTEuNzUyLTIuNjI4LTQuMTktMi42MjgtNy4zMTNjMC0zLjA0OCwwLjg4Ni01LjQ2NiwyLjY1Ny03LjI1N2MxLjc3MS0xLjc5LDQuMDA5LTIuNjg2LDYuNzE1LTIuNjg2DQoJCUMzMi42MDQsMTguNTU4LDM1LjQ0MSwyMC4xMDEsMzcuMTU2LDIzLjE4N3ogTTU1LjYxMywyMy4xODdsLTQuMjI5LDIuMjI5Yy0wLjQ1Ny0wLjk1MS0xLjAyLTEuNjE5LTEuNjg2LTINCgkJYy0wLjY2OC0wLjM4LTEuMzA3LTAuNTcxLTEuOTE0LTAuNTcxYy0yLjg1NywwLTQuMjg3LDEuODg1LTQuMjg3LDUuNjU3YzAsMS43MTQsMC4zNjMsMy4wODQsMS4wODYsNC4xMTMNCgkJYzAuNzIzLDEuMDI5LDEuNzg5LDEuNTQ0LDMuMjAxLDEuNTQ0YzEuODY1LDAsMy4xOC0wLjkxNSwzLjk0MS0yLjc0M2w0LDJjLTAuODc1LDEuNTYzLTIuMDU3LDIuNzkxLTMuNTQxLDMuNjg2DQoJCWMtMS40ODYsMC44OTYtMy4xMDUsMS4zNDMtNC44NTcsMS4zNDNjLTIuODk2LDAtNS4yMDktMC44NzUtNi45NDEtMi42MjljLTEuNzM2LTEuNzUyLTIuNjAyLTQuMTktMi42MDItNy4zMTMNCgkJYzAtMy4wNDgsMC44ODUtNS40NjYsMi42NTgtNy4yNTdjMS43Ny0xLjc5LDQuMDA4LTIuNjg2LDYuNzEzLTIuNjg2QzUxLjExNywxOC41NTgsNTMuOTM4LDIwLjEwMSw1NS42MTMsMjMuMTg3eiIvPg0KPC9nPg0KPC9zdmc+DQo=">
                                    <img style="height:11px!important;margin-left:3px;vertical-align:text-bottom;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxMy4wLjIsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDE0OTQ4KSAgLS0+DQo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMC8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9UUi8yMDAxL1JFQy1TVkctMjAwMTA5MDQvRFREL3N2ZzEwLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMCIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4IiB2aWV3Qm94PSI1LjUgLTMuNSA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyA1LjUgLTMuNSA2NCA2NCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Y2lyY2xlIGZpbGw9IiNGRkZGRkYiIGN4PSIzNy42MzciIGN5PSIyOC44MDYiIHI9IjI4LjI3NiIvPg0KCTxnPg0KCQk8cGF0aCBkPSJNMzcuNDQzLTMuNWM4Ljk4OCwwLDE2LjU3LDMuMDg1LDIyLjc0Miw5LjI1N0M2Ni4zOTMsMTEuOTY3LDY5LjUsMTkuNTQ4LDY5LjUsMjguNWMwLDguOTkxLTMuMDQ5LDE2LjQ3Ni05LjE0NSwyMi40NTYNCgkJCUM1My44NzksNTcuMzE5LDQ2LjI0Miw2MC41LDM3LjQ0Myw2MC41Yy04LjY0OSwwLTE2LjE1My0zLjE0NC0yMi41MTQtOS40M0M4LjY0NCw0NC43ODQsNS41LDM3LjI2Miw1LjUsMjguNQ0KCQkJYzAtOC43NjEsMy4xNDQtMTYuMzQyLDkuNDI5LTIyLjc0MkMyMS4xMDEtMC40MTUsMjguNjA0LTMuNSwzNy40NDMtMy41eiBNMzcuNTU3LDIuMjcyYy03LjI3NiwwLTEzLjQyOCwyLjU1My0xOC40NTcsNy42NTcNCgkJCWMtNS4yMiw1LjMzNC03LjgyOSwxMS41MjUtNy44MjksMTguNTcyYzAsNy4wODYsMi41OSwxMy4yMiw3Ljc3LDE4LjM5OGM1LjE4MSw1LjE4MiwxMS4zNTIsNy43NzEsMTguNTE0LDcuNzcxDQoJCQljNy4xMjMsMCwxMy4zMzQtMi42MDcsMTguNjI5LTcuODI4YzUuMDI5LTQuODM4LDcuNTQzLTEwLjk1Miw3LjU0My0xOC4zNDNjMC03LjI3Ni0yLjU1My0xMy40NjUtNy42NTYtMTguNTcxDQoJCQlDNTAuOTY3LDQuODI0LDQ0Ljc5NSwyLjI3MiwzNy41NTcsMi4yNzJ6IE00Ni4xMjksMjAuNTU3djEzLjA4NWgtMy42NTZ2MTUuNTQyaC05Ljk0NFYzMy42NDNoLTMuNjU2VjIwLjU1Nw0KCQkJYzAtMC41NzIsMC4yLTEuMDU3LDAuNTk5LTEuNDU3YzAuNDAxLTAuMzk5LDAuODg3LTAuNiwxLjQ1Ny0wLjZoMTMuMTQ0YzAuNTMzLDAsMS4wMSwwLjIsMS40MjgsMC42DQoJCQlDNDUuOTE4LDE5LjUsNDYuMTI5LDE5Ljk4Niw0Ni4xMjksMjAuNTU3eiBNMzMuMDQyLDEyLjMyOWMwLTMuMDA4LDEuNDg1LTQuNTE0LDQuNDU4LTQuNTE0czQuNDU3LDEuNTA0LDQuNDU3LDQuNTE0DQoJCQljMCwyLjk3MS0xLjQ4Niw0LjQ1Ny00LjQ1Nyw0LjQ1N1MzMy4wNDIsMTUuMywzMy4wNDIsMTIuMzI5eiIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K">
                                    <img style="height:11px!important;margin-left:3px;vertical-align:text-bottom;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxMy4wLjIsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDE0OTQ4KSAgLS0+DQo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMC8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9UUi8yMDAxL1JFQy1TVkctMjAwMTA5MDQvRFREL3N2ZzEwLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMCIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4IiB2aWV3Qm94PSI1LjUgLTMuNSA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyA1LjUgLTMuNSA2NCA2NCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Y2lyY2xlIGZpbGw9IiNGRkZGRkYiIGN4PSIzNy40NyIgY3k9IjI4LjczNiIgcj0iMjkuNDcxIi8+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0zNy40NDItMy41YzguOTksMCwxNi41NzEsMy4wODUsMjIuNzQzLDkuMjU2QzY2LjM5MywxMS45MjgsNjkuNSwxOS41MDksNjkuNSwyOC41YzAsOC45OTItMy4wNDgsMTYuNDc2LTkuMTQ1LDIyLjQ1OA0KCQkJQzUzLjg4LDU3LjMyLDQ2LjI0MSw2MC41LDM3LjQ0Miw2MC41Yy04LjY4NiwwLTE2LjE5LTMuMTYyLTIyLjUxMy05LjQ4NUM4LjY0NCw0NC43MjgsNS41LDM3LjIyNSw1LjUsMjguNQ0KCQkJYzAtOC43NjIsMy4xNDQtMTYuMzQzLDkuNDI5LTIyLjc0M0MyMS4xLTAuNDE0LDI4LjYwNC0zLjUsMzcuNDQyLTMuNXogTTEyLjcsMTkuODcyYy0wLjk1MiwyLjYyOC0xLjQyOSw1LjUwNS0xLjQyOSw4LjYyOQ0KCQkJYzAsNy4wODYsMi41OSwxMy4yMiw3Ljc3LDE4LjRjNS4yMTksNS4xNDQsMTEuMzkxLDcuNzE1LDE4LjUxNCw3LjcxNWM3LjIwMSwwLDEzLjQwOS0yLjYwOCwxOC42My03LjgyOQ0KCQkJYzEuODY3LTEuNzksMy4zMzItMy42NTcsNC4zOTgtNS42MDJsLTEyLjA1Ni01LjM3MWMtMC40MjEsMi4wMi0xLjQzOSwzLjY2Ny0zLjA1Nyw0Ljk0MmMtMS42MjIsMS4yNzYtMy41MzUsMi4wMTEtNS43NDQsMi4yDQoJCQl2NC45MTVoLTMuNzE0di00LjkxNWMtMy41NDMtMC4wMzYtNi43ODItMS4zMTItOS43MTQtMy44MjdsNC40LTQuNDU3YzIuMDk0LDEuOTQyLDQuNDc2LDIuOTEzLDcuMTQzLDIuOTEzDQoJCQljMS4xMDQsMCwyLjA0OC0wLjI0NiwyLjgzLTAuNzQzYzAuNzgtMC40OTQsMS4xNzItMS4zMTIsMS4xNzItMi40NTdjMC0wLjgwMS0wLjI4Ny0xLjQ0OC0wLjg1OC0xLjk0M2wtMy4wODUtMS4zMTVsLTMuNzcxLTEuNzE1DQoJCQlsLTUuMDg2LTIuMjI5TDEyLjcsMTkuODcyeiBNMzcuNTU3LDIuMjE0Yy03LjI3NiwwLTEzLjQyOCwyLjU3MS0xOC40NTcsNy43MTRjLTEuMjU4LDEuMjU4LTIuNDM5LDIuNjg2LTMuNTQzLDQuMjg3TDI3Ljc4NiwxOS43DQoJCQljMC41MzMtMS42NzYsMS41NDItMy4wMTksMy4wMjktNC4wMjhjMS40ODQtMS4wMDksMy4yMTgtMS41NzEsNS4yLTEuNjg2VjkuMDcxaDMuNzE1djQuOTE1YzIuOTM0LDAuMTUzLDUuNiwxLjE0Myw4LDIuOTcxDQoJCQlsLTQuMTcyLDQuMjg2Yy0xLjc5My0xLjI1Ny0zLjYxOS0xLjg4NS01LjQ4Ni0xLjg4NWMtMC45OTEsMC0xLjg3NiwwLjE5MS0yLjY1NiwwLjU3MWMtMC43ODEsMC4zODEtMS4xNzIsMS4wMjktMS4xNzIsMS45NDMNCgkJCWMwLDAuMjY3LDAuMDk1LDAuNTMzLDAuMjg1LDAuOGw0LjA1NywxLjgzbDIuOCwxLjI1N2w1LjE0NCwyLjI4NWwxNi4zOTcsNy4zMTRjMC41MzUtMi4yNDgsMC44MDEtNC41MzMsMC44MDEtNi44NTcNCgkJCWMwLTcuMzUzLTIuNTUyLTEzLjU0My03LjY1Ni0xOC41NzNDNTEuMDA1LDQuNzg1LDQ0LjgzMSwyLjIxNCwzNy41NTcsMi4yMTR6Ii8+DQoJPC9nPg0KPC9nPg0KPC9zdmc+DQo=">
                                    <img style="height:11px!important;margin-left:3px;vertical-align:text-bottom;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxMy4wLjIsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDE0OTQ4KSAgLS0+DQo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgd2lkdGg9IjY0LjAwMDk3N3B4IiBoZWlnaHQ9IjY0cHgiIHZpZXdCb3g9IjAgMCA2NC4wMDA5NzcgNjQiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDY0LjAwMDk3NyA2NCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Y2lyY2xlIGZpbGw9IiNGRkZGRkYiIGN4PSIzMi4wNjQ0NTMiIGN5PSIzMS43ODgwODYiIHI9IjI5LjAxMjY5NSIvPg0KCTxnPg0KCQk8cGF0aCBkPSJNMzEuOTQzODQ4LDBDNDAuODk2NDg0LDAsNDguNDc2NTYyLDMuMTA1NDY5LDU0LjY4NzUsOS4zMTQ0NTNDNjAuODk0NTMxLDE1LjQ4NjMyOCw2NC4wMDA5NzcsMjMuMDQ1ODk4LDY0LjAwMDk3NywzMg0KCQkJcy0zLjA0ODgyOCwxNi40NTcwMzEtOS4xNDU1MDgsMjIuNTEzNjcyQzQ4LjQxNzk2OSw2MC44Mzc4OTEsNDAuNzc5Mjk3LDY0LDMxLjk0Mjg3MSw2NA0KCQkJYy04LjY0ODkyNiwwLTE2LjE1MjgzMi0zLjE0MjU3OC0yMi41MTM2NzItOS40Mjk2ODhDMy4xNDQwNDMsNDguMjg2MTMzLDAsNDAuNzYxNzE5LDAsMzIuMDAwOTc3DQoJCQljMC04LjcyMzYzMywzLjE0NDA0My0xNi4yODUxNTYsOS40MjkxOTktMjIuNjg0NTdDMTUuNjQwMTM3LDMuMTA1NDY5LDIzLjE0NTAyLDAsMzEuOTQzODQ4LDB6IE0zMi4wNjA1NDcsNS43NzE0ODQNCgkJCWMtNy4yNzUzOTEsMC0xMy40Mjk2ODgsMi41NzAzMTItMTguNDU4NDk2LDcuNzE0ODQ0QzguMzgxODM2LDE4Ljc4MzIwMyw1Ljc3Mjk0OSwyNC45NTQxMDIsNS43NzI5NDksMzINCgkJCWMwLDcuMTI1LDIuNTg5ODQ0LDEzLjI1NjgzNiw3Ljc3MDAyLDE4LjQwMDM5MWM1LjE4MTE1Miw1LjE4MTY0MSwxMS4zNTIwNTEsNy43NzA1MDgsMTguNTE1NjI1LDcuNzcwNTA4DQoJCQljNy4xMjMwNDcsMCwxMy4zMzIwMzEtMi42MDgzOTgsMTguNjI2OTUzLTcuODI4MTI1QzU1LjcxMzg2Nyw0NS40NjY3OTcsNTguMjI4NTE2LDM5LjM1MzUxNiw1OC4yMjg1MTYsMzINCgkJCWMwLTcuMzEyNS0yLjU1MzcxMS0xMy40ODQzNzUtNy42NTYyNS0xOC41MTM2NzJDNDUuNTA0ODgzLDguMzQxNzk3LDM5LjMzMzk4NCw1Ljc3MTQ4NCwzMi4wNjA1NDcsNS43NzE0ODR6IE00NC4xMTcxODgsMjQuNDU2MDU1DQoJCQl2NS40ODUzNTJIMjAuODU5ODYzdi01LjQ4NTM1Mkg0NC4xMTcxODh6IE00NC4xMTcxODgsMzQuNzQzMTY0djUuNDgxNDQ1SDIwLjg1OTg2M3YtNS40ODE0NDVINDQuMTE3MTg4eiIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K">
                                </div>
                            </div>`
                }), 60 * 1000, 'PDF timed out');

                res.writeHead(200, {
                    'content-type': 'application/pdf',
                    'cache-control': 'public,max-age=31536000',
                });
                res.end(pdf, 'binary');
                break;
            }
        }
        actionDone = true;
        console.log('💥 Done action: ' + action);
    }
    catch (e) {
        console.error(e);
        if (!DEBUG && page) {
            console.log('💔 Force close ' + truncate(pageURL,180));
            page.removeAllListeners();
            await page.close();
        }
        //cache.del(pageURL);
        const {message = ''} = e;
        res.writeHead(400, {
            'content-type': 'text/plain',
        });
        res.end('Oops. Something is wrong.\n\n' + message);

        // Handle websocket not opened error
        if (/not opened/i.test(message) && browser) {
            console.error('🕸 Web socket failed');
            try {
                await browser.close();
                browser = null;
            } catch (err) {
                console.warn(`Chrome could not be killed ${err.message}`);
                browser = null;
            }
        }
    }
    if (page) {
        page.removeAllListeners();
        await page.close();
        page = null
    }
    if (browser) {
        await browser.close();
        browser = null;
    }
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

process.on('SIGINT', () => {
    if (browser) browser.close();
    process.exit();
});
process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at:', p, 'reason:', reason);
});
