#!/bin/sh
python /app/manage.py migrate --noinput

# now collectstatic is done while building production image
# python /app/manage.py collectstatic --noinput

/usr/local/bin/gunicorn config.wsgi -t 60 -w 4 -b 0.0.0.0:5000 --chdir=/app
