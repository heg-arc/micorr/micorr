import {Injectable, NgModule} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterModule,
  RouterStateSnapshot,
  Routes
} from '@angular/router';
import {ReviewerComponent} from "./reviewer/reviewer.component";
import {EditorComponent} from "./editor/editor.component";
import {EditorReviewComponent} from "./editor-review/editor-review.component";
import {PeerReviewComponent} from "./peer-review/peer-review.component";
import {AuthorComponent} from "./author/author.component";
import {AuthorReviewComponent} from "./author-review/author-review.component";
import {DataService, getUserString} from "./data.service";


@Injectable({providedIn: 'root'})
export class EditorGuard implements CanActivate {
  constructor(private dataService: DataService, private router: Router) {
  }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    await this.dataService.getCurrentUser()
    return this.dataService.hasEditorAccess()
  }
}

const routes: Routes = [
  {path: 'editor', component: EditorComponent, canActivate: [EditorGuard]},
  {path: 'reviewer', component: ReviewerComponent},
  {path: 'author', component: AuthorComponent},
  {path: 'editor-review/:id', component: EditorReviewComponent, canActivate: [EditorGuard]},
  {path: 'peer-review/:id', component: PeerReviewComponent},
  {path: 'author-review/:id', component: AuthorReviewComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
