import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {merge, Observable, OperatorFunction, Subject} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, map} from "rxjs/operators";
import {NgbTypeahead} from "@ng-bootstrap/ng-bootstrap";
import {User} from "../data-types";

@Component({
  selector: 'app-typeahead',
  templateUrl: './typeahead.component.html',
  styleUrls: ['./typeahead.component.css']
})
export class TypeaheadComponent implements OnInit {
	model: any;
  @Input() names: string[] = ['un','deux','trois']
  @Input() users: User[] | null = null
  @Output() userSelected: EventEmitter<User|null> = new EventEmitter<User|null>()

	@ViewChild('instance', { static: true }) instance!: NgbTypeahead
  focus$ = new Subject<string>();
	click$ = new Subject<string>();

	search: OperatorFunction<string, readonly User[]> = (text$: Observable<string>) => {
		const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged())
		const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()))
		const inputFocus$ = this.focus$;

		return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
			map((term) =>
				(term === '' ? this.users! : this.users!.filter((v) => v.username.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10),
			),
		)
	}
  // formatter = (u: { username: string }) => u.username;
  formatter(u:User)
  {
    return u.username
  }

  constructor() { }

  ngOnInit(): void {
    if (this.users) {
      this.names = this.users.map(u => u.username)
    }
  }
  onModelChange() {
    this.userSelected.emit(this.model);
  }
}
