import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import {DataService} from "../data.service";
import {Alert} from "../data-types";

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  constructor(private dataService: DataService ) {
  }

  private _success = new Subject<string>();

  successMessage = '';

  @ViewChild('selfClosingAlert', {static: false}) selfClosingAlert!: NgbAlert;

  ngOnInit(): void {
    this._success.subscribe((message) => (this.successMessage = message));
    this._success.pipe(debounceTime(5000)).subscribe(() => {
      if (this.selfClosingAlert) {
        this.selfClosingAlert.close();
      }
    });
  }

  public changeSuccessMessage() {
    // single success selfClosingAlert from ngb documentation not used yet
    this._success.next(`${new Date()} - Message successfully changed.`);
  }
  getAlerts():Alert[] {
    return this.dataService.getAlerts()
  }

  close(alert: Alert) {
		this.dataService.deleteAlert(alert)
	}

}
