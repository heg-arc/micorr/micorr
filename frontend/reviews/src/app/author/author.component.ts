import {Component, Input, OnInit} from '@angular/core';
import {EditorReview, ReviewStatus, User} from "../data-types";
import {DataService} from "../data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {
  @Input() user: User | null = null
  public editor_reviews: EditorReview [] = []

  constructor(private dataService: DataService, public router: Router) {
  }

  ngOnInit(): void {
    this.dataService.getCurrentUser().then(user => {
      this.user = user
      if (this.user) {
        this.dataService.getAll<EditorReview[]>('editor_reviews', {publication__artefact__object__user: this.user.id.toString()}).subscribe(reviews => {
          this.editor_reviews = reviews
        })
      }
    })
  }

  getStatusKey(editorReview: EditorReview) {
    let status = editorReview.review_logs.at(0)?.status as ReviewStatus
    return status ? this.dataService.getStatusKey(status) : 'None'
  }
}
