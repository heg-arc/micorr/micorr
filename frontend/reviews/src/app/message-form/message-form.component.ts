import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {PeerReview, ReviewStatus, User} from "../data-types";
import {getUserString} from "../data.service";

export interface MessageFormValue {
  text?: string | null,
  fileName?: string | null,
  file?: File | null,
  to?: User | null,
  reports?: boolean[],
  status?:ReviewStatus
}

function requiredFileType(type: string) {
  return function (control: FormControl) {
    const fileName = control.value;
    if (fileName) {
      const extension = fileName.split('.').at(-1).toLowerCase();
      if (type.toLowerCase() !== extension.toLowerCase()) {
        return {
          requiredFileType: true
        };
      }

      return null;
    }

    return null;
  };
}

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css']
})
export class MessageFormComponent implements OnInit {

  @Output() value = new EventEmitter<MessageFormValue>();
  @Input() attachFile = false
  @Input() attachReports = false
  @Input() selectRecipient = false
  @Input() reviews:PeerReview[] = []
  @Input() recipient: User | null | undefined
  file: File | null = null
  to: User | null = null

  constructor() {

  }

  messageForm = new FormGroup({
    text: new FormControl('', Validators.required),
    fileName: new FormControl('', this.attachFile ? [Validators.required, requiredFileType('pdf')]:null),
    to: new FormControl('', this.selectRecipient ? [Validators.required, Validators.email] : null),
    reports: new FormArray([])
  })

  get reports() {
    return this.messageForm.get('reports') as FormArray;
  }
  ngOnInit(): void {
    if (!this.attachFile) {
      this.messageForm.controls.fileName.removeValidators([Validators.required, requiredFileType('pdf')])
    }
    if (this.reviews.length) {
      for (let review of this.reviews) {
        this.reports.push(new FormControl({value:review.selected_for_author, disabled:!review.report}))
      }
    }
    if (this.recipient) {
      // if recipient input was set initialize with this user
      // could be overwritten by setUser if selectRecipient is true
      this.to = this.recipient
    }
  }

  onFileSelected(event: any) {

    const file: File = event.target.files[0];

    if (file) {
      // save the file object for upload from parent component on submit
      this.file = file
      // we could also direcly upload / preview the file in the form
      // const formData = new FormData();
      // formData.append("thumbnail", file);
      // const upload$ = this.http.post("/api/thumbnail-upload", formData);
      // upload$.subscribe();
    }
  }

  setUser(user: User | null) {
    if (user) {
      this.to = user
      //this.messageForm.patchValue({to:user.email})
      this.messageForm.controls.to.patchValue(user.email)
    }
  }

  onSubmit() {
    console.warn(this.messageForm.value)
    this.value.emit({...this.messageForm.value, file: this.file, to:this.to})
  }
  getUserString(user:User|null) {
     return getUserString(user)
   }
}
