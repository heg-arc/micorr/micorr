import { Component, OnInit } from '@angular/core';
import {EditorReview, ReviewStatus, User} from "../data-types";
import {DataService} from "../data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
  private users : User[] = []
  public editor_reviews: EditorReview [] = []

  constructor(private dataService:DataService, public router:Router) {

  }

  ngOnInit(): void {
    this.dataService.getAll<EditorReview[]>('editor_reviews').subscribe(reviews => {
      this.editor_reviews = reviews;
    })
  }

  getStatusKey(editorReview: EditorReview) {
    let status = editorReview.review_logs.at(0)?.status as ReviewStatus
    return status ? this.dataService.getStatusKey(status) : ''
  }
}
