import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditorComponent } from './editor/editor.component';
import { ReviewerComponent } from './reviewer/reviewer.component';
import {DataService} from "./data.service";
import {HttpClientModule} from "@angular/common/http";
import { EditorReviewComponent } from './editor-review/editor-review.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ReviewLogComponent } from './review-log/review-log.component';
import { ReviewersComponent } from './reviewers/reviewers.component';
import { ModalUsersComponent } from './modal-users/modal-users.component';
import { TypeaheadComponent } from './typeahead/typeahead.component';
import { AlertComponent } from './alert/alert.component';
import { PeerReviewComponent } from './peer-review/peer-review.component';
import { MessageFormComponent } from './message-form/message-form.component';
import { PeerReviewSummaryComponent } from './peer-review-summary/peer-review-summary.component';
import { AuthorComponent } from './author/author.component';
import { AuthorReviewComponent } from './author-review/author-review.component';

@NgModule({
  declarations: [
    AppComponent,
    EditorComponent,
    ReviewerComponent,
    EditorReviewComponent,
    ReviewLogComponent,
    ReviewersComponent,
    ModalUsersComponent,
    TypeaheadComponent,
    AlertComponent,
    PeerReviewComponent,
    MessageFormComponent,
    PeerReviewSummaryComponent,
    AuthorComponent,
    AuthorReviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
