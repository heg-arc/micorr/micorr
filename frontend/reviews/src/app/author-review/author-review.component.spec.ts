import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorReviewComponent } from './author-review.component';

describe('AuthorReviewComponent', () => {
  let component: AuthorReviewComponent;
  let fixture: ComponentFixture<AuthorReviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthorReviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AuthorReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
