import {Component, Input, OnInit} from '@angular/core';
import {switchMap, tap} from "rxjs/operators";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {EditorReview, PeerReview, ReviewLog, ReviewStatus, User} from "../data-types";
import {DataService} from "../data.service";
import {getUserString} from "../data.service"
import {Observable} from "rxjs";
import {MessageFormValue} from "../message-form/message-form.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-author-review',
  templateUrl: './author-review.component.html',
  styleUrls: ['./author-review.component.css']
})
export class AuthorReviewComponent implements OnInit {
  @Input() editor_review?: EditorReview | null;
  editorReview$!: Observable<EditorReview>
  editorReview: EditorReview | undefined
  peerReviews: PeerReview[] = []

  author: User | null = null
  status: ReviewStatus | undefined;
  Status = ReviewStatus
  public modalTitle = 'Send message'

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dataService: DataService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.editorReview$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.dataService.getEditorReview(Number(params.get('id'))))
      , tap(items => {
        this.editorReview = items
        this.author = this.editorReview.publication['artefact'].object.user;
        if (this.editorReview.review_logs.length)
          this.status = this.editorReview.review_logs.at(0)?.status as ReviewStatus
        this.dataService.getPeerReviews(this.editorReview.id, undefined, true).subscribe(peerReviews => {
          this.peerReviews = peerReviews
        })
      })
    )
  }

  getUserString(user: User | null | undefined) {
    return getUserString(user)
  }

  getStatusKey(status: ReviewStatus | undefined) {
    return status ? this.dataService.getStatusKey(status) : ''
  }

  getFileName(url: string | undefined) {
    return url ? url.split('/').pop() : ''
  }

  addLog(status: ReviewStatus, comment: string, to: User | null | undefined) {
    let currentUserId = this.dataService.getUser()?.id
    if (this.editorReview) {
      if (!this.editorReview.user?.id && currentUserId) {
        // Current editor takes ownership of the editorReview on first access
        this.dataService.patch<EditorReview>('editor_reviews', this.editorReview.id, {user_id: currentUserId}).subscribe(
          (result) => {
            this.editorReview = result
          }
        )
      }
      let reviewLog: ReviewLog = {
        id: 0,
        user_id: currentUserId,
        modified: new Date,
        resource: 0,
        resource_id: this.editorReview.id,
        resource_content_type: this.editorReview.content_type,
        order: -1,
        comment: comment,
        status: status
      }
      this.dataService.post<ReviewLog>('review_logs', reviewLog).subscribe((result) => {
        this.editorReview!.review_logs.splice(0, 0, result)
        if (to) {
          this.dataService.put<any>('review_logs', `${result.id}/email_to`, {email: to.email}).subscribe((sent) => {
            console.log(sent)
          })
        }
      })
      this.status = status
    }
  }

  do(modalContent: any, fnAction: ((value: MessageFormValue) => void), title?: string) {
    // open the modalContent dialog then call the fnAction on closure (By onMessage() )
    this.modalTitle = title ? title : 'Send message'
    this.modalService.open(modalContent, {size: 'lg'}).result.then((value: MessageFormValue) => {
      fnAction.apply(this, [value])
    })
  }

  // on messageForm submit
  onMessage(message: MessageFormValue, modal: any) {
    if (message)
      if (message.text) {
        if (modal)
          modal.close(message)
      }
  }

  cancel(value: MessageFormValue) {
    this.addLog(ReviewStatus.CANCELED, `Request for Publication cancelled\n${value.text ? value.text : ''}`, value.to)
  }

  authorize(value: MessageFormValue) {
    this.addLog(ReviewStatus.AUTHORIZED, `Publication approved - authorization requested\n${value.text ? value.text : ''}`, value.to)
  }
}
