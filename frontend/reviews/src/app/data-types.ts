export interface User {
  id: number;
  username: string;
  email:string;
  first_name: string;
  last_name: string;
  groups: string[];
}

export interface CannedTextType {
    id : number;
    name: string;
}

export interface CannedText {
    id : number;
    type: number
    content: string;
}

export interface ReviewLog {
    id : number;
    user? : User;
    user_id?: number;
    modified: Date;
    resource: number;
    resource_content_type: number;
    resource_id: number;
    order: number;
    comment: string;
    status: string;
}

export interface EditorReview {
  id: number
  modified: Date;
  created: Date;
  user? : User;
  content_type: number;
  publication : Record<string, any>
  artefact_snapshot: string
  review_logs: ReviewLog[]
}


export interface PeerReview {
  id?: number
  modified?: Date;
  created?: Date;
  editor_review: number
  user? : User
  user_id: number
  content_type?: number;
  publication? : Record<string, any>
  report?: string
  selected_for_author: boolean
  review_logs: ReviewLog[]
}

export enum ReviewStatus {
  SENT = 'SNT',
  RECEIVED = 'RVD',
  REVIEW_ACCEPTED = 'ACD',
  REJECTED = 'RJD',
  COMPLETED = 'CPD',
  MINOR_REVISION_REQUESTED = 'MIN',
  MAJOR_REVISION_REQUESTED = 'MAJ',
  PUBLICATION_APPROVED = 'APD',
  AUTHORIZED = 'AUD',
  PUBLISHED = 'PUD',
  CANCELED = 'CCD'
}

export interface User {
  id: number,
  email: string,
  username: string,
  first_name: string,
  last_name: string,
  name: string
}

export interface Alert {
	type: string;
	message: string;
}

export interface  Choice {
  value : string;
  display_name: string;
}
