import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { User } from '../data-types';
import {DataService} from "../data.service";


@Component({
  selector: 'app-modal-users',
  templateUrl: './modal-users.component.html',
  styleUrls: ['./modal-users.component.css']
})
export class ModalUsersComponent implements OnInit {
  @Output() user: EventEmitter<User|null> = new EventEmitter<User|null>()
  closeResult = '';
  selectedUser:User|null = null
  users$ : Observable<User[]> | null = null

  constructor(private modalService: NgbModal, private dataService:DataService) {}

	open(content:any) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
         this.user.emit(this.selectedUser)
				this.closeResult = `Closed with: ${this.selectedUser?.username}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}
  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
  ngOnInit(): void {
       this.users$ = this.dataService.getAll<User[]>('reviewers')
  }

  onUserSelect(user:User|null) {
    this.selectedUser = user
  }


}
