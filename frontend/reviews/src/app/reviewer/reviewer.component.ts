import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {EditorReview, PeerReview, ReviewStatus, User} from "../data-types";
import {switchMap, tap} from "rxjs/operators";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {DataService} from "../data.service";

@Component({
  selector: 'app-reviewer',
  templateUrl: './reviewer.component.html',
  styleUrls: ['./reviewer.component.css']
})
export class ReviewerComponent implements OnInit {
  user : User | null = null
  peerReviews:PeerReview[] = []

  constructor(private route: ActivatedRoute,
  public router: Router,
  private dataService: DataService ) { }

  ngOnInit(): void {
    this.dataService.getCurrentUser().then(user => {
      this.user = user
      this.dataService.getAll<PeerReview[]>('peer_reviews', {user: this.user.id.toString()}).subscribe(reviews => {
        this.peerReviews = reviews;
      })
    })
  }

  getStatusKey(peerReview: PeerReview) {
    let status = peerReview.review_logs.at(0)?.status as ReviewStatus
    return status ? this.dataService.getStatusKey(status) : ''
  }
}
