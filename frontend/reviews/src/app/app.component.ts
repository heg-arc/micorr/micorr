import {Component, OnInit} from '@angular/core';
import {PeerReview, User} from "./data-types";
import {DataService} from "./data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  title = 'reviews'
  user : User | null = null

  constructor(private dataService:DataService) {
  }

  ngOnInit(): void {
    this.dataService.getCurrentUser().then(user => this.user = user)
    this.dataService.getReviewStatusChoices()
  }

  hasAuthorAccess() {
    return this.dataService.hasAuthorAccess()
  }

  hasEditorAccess() {
    return this.dataService.hasEditorAccess()
  }

  hasReviewerAccess() {
    return this.dataService.hasReviewerAccess()
  }
}
