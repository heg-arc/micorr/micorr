import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest} from '@angular/common/http';
import {concatAll, lastValueFrom, map, observable, Observable, of, share, tap, throwError} from 'rxjs';
import {environment} from "../environments/environment";
import {Alert, Choice, EditorReview, PeerReview, ReviewLog, ReviewStatus, User} from "./data-types";

let fakeAuthHeaders:HttpHeaders
if (!environment.production)
  fakeAuthHeaders = new HttpHeaders({'x-username': 'bletourmy'});
else
  fakeAuthHeaders = new HttpHeaders();
const httpOptions = {headers:fakeAuthHeaders}


@Injectable({
  providedIn: 'root'
})

export class DataService {

  private REST_API_SERVER = environment.apiUrl;
  private reviewers:User[]=[]
  private editor_reviews:EditorReview[]=[]
  private user: User | null = null
  private statusChoices : Choice[] = []

  // alert-component data support
  private alerts: Alert[] = []

  public getAlerts():Alert[] {
    return this.alerts
  }
  public deleteAlert(alert:Alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  private reportErrors(observable: Observable<HttpEvent<any>>) {
    observable.subscribe({
      error: (error) => {
        let message = `${error.message}<br>${JSON.stringify(error.error)}`
        this.alerts.push({type: 'danger', message: message})
      }
    })
    return (observable)
  }

  constructor(private httpClient: HttpClient) { }
  public get<T>(table : String, pk : number|string): Observable<T>
  {
    return this.httpClient.get<T>(this.REST_API_SERVER + `/${table}/${pk}/`,httpOptions);
  }

  public put<T>(table : String, pk: string, object : T): Observable<T>
  {
    return this.httpClient.put<T>(this.REST_API_SERVER + `/${table}/${pk}/`, object, httpOptions);
  }

  public putForm(table : String, pk: number,formData:FormData): Observable<HttpEvent<any>> {
  const req = new HttpRequest('PUT', `${this.REST_API_SERVER}/${table}/${pk}/`, formData, {
      reportProgress: true,
      responseType: 'json',
      headers: httpOptions.headers
    });
    let observable = this.httpClient.request(req).pipe(share())
    return this.reportErrors(observable)
  }

  public patchForm(table: String, pk: number, formData: FormData): Observable<HttpEvent<any>> {
    const req = new HttpRequest('PATCH', `${this.REST_API_SERVER}/${table}/${pk}/`, formData, {
      ...httpOptions,
      reportProgress: true,
      responseType: 'json'
    });
    let observable = this.httpClient.request(req)
    this.reportErrors(observable)
    return observable
  }

  public post<T>(table : String, object : T): Observable<T>
  {
    let observable = this.httpClient.post<T>(this.REST_API_SERVER + `/${table}/`, object, httpOptions).pipe(share())
    // caution as we do subscribe to the observable internally below
    // we had to transform the cold observable into hot one using share to avoid double POST (execution on each subscribe)
    observable.subscribe({
      error: (error) => {
        let message = `${error.message}\n${JSON.stringify(error.error)}`
        this.alerts.push({type: 'danger', message: message})
      },
      complete: () => this.alerts.push({type: 'success', message: `POST ${table} successfully completed`})
    })
    return observable
  }
  public patch<T>(table: String, pk: number, object : any): Observable<T>
  {
    return this.httpClient.patch<T>(this.REST_API_SERVER +`/${table}/${pk}/`, object, httpOptions);
  }
  public patch_multiple<T>(table: String, object : T[]): Observable<T> {
    return this.httpClient.patch<T>(this.REST_API_SERVER + `/${table}/`, object, httpOptions);
  }
  public delete<T>(table : String, pk: number): Observable<T>
  {
    return this.httpClient.delete<T>(this.REST_API_SERVER + `/${table}/${pk}`, httpOptions);
  }

public getAll<T>(table : String, filter?:string|Record<string,string>): Observable<T>
  {
    let querystring = ''
    if (filter) {
      if (typeof filter == "string") {

      } else {
        let urlSearchParams = new URLSearchParams(filter)
        querystring = urlSearchParams.toString()
      }
    }
    let obs = this.httpClient.get<T>(this.REST_API_SERVER + `/${table}/${querystring ? '?'+querystring : ''}`, httpOptions)
    // @ts-ignore
    return obs.pipe(tap( items => this[table]=items));

  }

  public getEditorReview(pk: number) : Observable<EditorReview> {
    return this.get<EditorReview>('editor_reviews', pk)
  }

  public getPeerReview(pk: number) : Observable<PeerReview> {
    return this.get<PeerReview>('peer_reviews', pk)
  }

  public async getCurrentUser() : Promise<User>
  {
    this.user = this.user ? this.user : await lastValueFrom(this.get<User>('reviewers','current_user'))
    return this.user
  }

  public async getReviewStatusChoices():Promise<Choice[]> {
      if (this.statusChoices.length)
        return this.statusChoices;
      let review_logs_options =  await lastValueFrom(this.httpClient.options<any>(this.REST_API_SERVER + `/review_logs/`,httpOptions));
      this.statusChoices = review_logs_options['actions']['POST']['status']['choices']
      return this.statusChoices
  }

  public getUser():User|null
  {
    return this.user
  }

  hasEditorAccess() {
    return this.user ? this.user.groups.includes('Main administrator') : false
  }

  hasReviewerAccess() {
    return this.user ? this.user.groups.includes('Peer Reviewers') : false
  }

  hasAuthorAccess() {
    return this.user ? this.user.groups.includes('MiCorr Editors') : false
  }

  public  getPeerReviews(editorReviewId?:number, userId?:number, selected_for_author?:boolean) : Observable<PeerReview[]> {
    let filter: Record<string,string> = {}
    if (editorReviewId)
      filter['editor_review'] = editorReviewId.toString()
    if (userId)
      filter['user'] = userId.toString()
    if (selected_for_author !== undefined)
      filter['selected_for_author'] = selected_for_author.toString()

    return this.getAll<PeerReview[]>('peer_reviews',filter)
  }

  public postPeerReviewLog(review: PeerReview, status: ReviewStatus, comment: string): Observable<ReviewLog> {
      let reviewLog: ReviewLog = {
        id: 0,
        user_id: this.user?.id,
        modified: new Date,
        resource: 0,
        resource_id: review.id!,
        resource_content_type: review.content_type ? review.content_type : 0,
        order: -1,
        comment: comment,
        status: status
      }
      return this.post<ReviewLog>('review_logs', reviewLog)
    }

    getStatusKey(status:ReviewStatus):string {
      const indexOfStatus = Object.values(ReviewStatus).indexOf(status)
      return Object.keys(ReviewStatus)[indexOfStatus]
    }
}


export function getUserString(user: User | null | undefined)
{
  return user ? `${user.username} <${user?.email}> ${user?.first_name} ${user?.last_name}` : ''
}
