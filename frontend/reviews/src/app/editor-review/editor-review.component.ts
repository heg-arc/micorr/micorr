import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {EditorReview, PeerReview, ReviewLog, ReviewStatus, User} from "../data-types";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {DataService} from "../data.service";
import {getUserString} from "../data.service"
import {Observable} from "rxjs";
import {switchMap, tap} from "rxjs/operators";
import {MessageFormValue} from "../message-form/message-form.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ReviewersComponent} from "../reviewers/reviewers.component";

@Component({
  selector: 'app-editor-review',
  templateUrl: './editor-review.component.html',
  styleUrls: ['./editor-review.component.css']
})
export class EditorReviewComponent implements OnInit {
  @Input() editor_review?: EditorReview | null;
  editorReview$!: Observable<EditorReview>;
  editorReview: EditorReview | undefined;
  peerReviews: PeerReview [] = []
  author: User | null = null
  status: ReviewStatus | undefined;
  selectedId = 0;
  Status = ReviewStatus
  public modalTitle = 'Send message'
  active = 1;

  @ViewChild(ReviewersComponent)
  private reviewersComponent!: ReviewersComponent;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dataService: DataService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.editorReview$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.dataService.getEditorReview(Number(params.get('id'))))
      , tap(items => {
        this.editorReview = items
        this.author = this.editorReview.publication['artefact'].object.user;
        if (this.editorReview.review_logs.length)
          this.status = this.editorReview.review_logs.at(0)?.status as ReviewStatus
      }));

  }

  addLog(status: ReviewStatus, comment: string, to: User | null) {
    let currentUserId = this.dataService.getUser()?.id
    if (this.editorReview) {
      if (!this.editorReview.user?.id && currentUserId) {
        // Current editor takes ownership of the editorReview on first access
        this.dataService.patch<EditorReview>('editor_reviews', this.editorReview.id, {user_id: currentUserId}).subscribe(
          (result) => {
            this.editorReview = result
          }
        )
      }
      let reviewLog: ReviewLog = {
        id: 0,
        user_id: currentUserId,
        modified: new Date,
        resource: 0,
        resource_id: this.editorReview.id,
        resource_content_type: this.editorReview.content_type,
        order: -1,
        comment: comment,
        status: status
      }
      this.dataService.post<ReviewLog>('review_logs', reviewLog).subscribe((result) => {
        this.editorReview!.review_logs.splice(0, 0, result)
        if (to) {
          this.dataService.put<any>('review_logs', `${result.id}/email_to`, {email: to.email}).subscribe((sent) => {
            console.log(sent)
          })
        }
      })
      this.status = status
    }
  }

  getLastStatus() {
    if (this.editorReview) {

    }
  }

  do(modalContent: any, fnAction: ((value: MessageFormValue) => void), title?: string) {
    // open the modalContent dialog then call the fnAction on closure (By onMessage() )
    this.modalTitle = title ? title : 'Send message'
    this.modalService.open(modalContent, {size: 'lg'}).result.then((value: MessageFormValue) => {
      fnAction.apply(this, [value])
    })
  }

  // on messageForm submit
  onMessage(message: MessageFormValue, modal: any) {
    if (message)
      if (message.text) {
        if (modal)
          modal.close(message)
      }
  }

  accept(value: MessageFormValue) {
    this.addLog(ReviewStatus.REVIEW_ACCEPTED, `submission accepted\n${value.text ? value.text : ''}`, this.author)
  }

  requestMinorRevision(value: MessageFormValue) {
    this.updatePeerReviewsReports(value);
    this.addLog(ReviewStatus.MINOR_REVISION_REQUESTED, `Requested minor revision to author\n${value.text ? value.text : ''}`, this.author)
  }

  requestMajorRevision(value: MessageFormValue) {
    this.updatePeerReviewsReports(value);
    this.addLog(ReviewStatus.MAJOR_REVISION_REQUESTED, `Requested major revision to author\n${value.text ? value.text : ''}`, this.author)
  }

  private updatePeerReviewsReports(value: MessageFormValue) {
    if (value.reports) {
      for (let i = 0; i < this.peerReviews.length; i++) {
        let pk = this.peerReviews[i].id || 0
        if (this.peerReviews[i].selected_for_author != value.reports[i] && pk) {
          this.dataService.patch<PeerReview>('peer_reviews', pk, {selected_for_author: value.reports[i]}).subscribe((review) => {
            this.peerReviews[i] = review
          })
        }
      }
    }
  }

  approve(value: MessageFormValue) {
    this.addLog(ReviewStatus.PUBLICATION_APPROVED, `Publication approved - authorization requested\n${value.text ? value.text : ''}`, this.author)
  }

  publish(value: MessageFormValue) {
    this.addLog(ReviewStatus.PUBLISHED, `Published\n${value.text ? value.text : ''}`, this.author)
  }

  reject(value: MessageFormValue) {
    this.addLog(ReviewStatus.REJECTED, `Rejected submission\n${value.text ? value.text : ''}`, this.author)
  }

  getUserString(user: User | null) {
    return getUserString(user)
  }

  getStatusKey(status: ReviewStatus | undefined) {
    return status ? this.dataService.getStatusKey(status) : ''
  }

  sendForReview(value: MessageFormValue) {
    if (value.to) {
      let message = value.text ? value.text : ''
      this.addLog(ReviewStatus.REVIEW_ACCEPTED, `Sent for review to ${value.to.username}<${value.to.email}>\n${message}`, null)
      // here message will be emailed to reviewer as part of first Peer ReviewLog creation
      this.reviewersComponent.addPeerReview(value.to, message)
    }
  }

  onPeerReviewsChange(peerReviews: PeerReview[]) {
    this.peerReviews = peerReviews.filter(r => r.report)
  }
}
