import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EditorReview, PeerReview, ReviewLog, ReviewStatus, User} from "../data-types";
import {DataService} from "../data.service";
import {lastValueFrom, Observable} from "rxjs";
import {MessageFormValue} from "../message-form/message-form.component";

@Component({
  selector: 'app-reviewers',
  templateUrl: './reviewers.component.html',
  styleUrls: ['./reviewers.component.css']
})
export class ReviewersComponent implements OnInit {

  @Input() editor_review?: EditorReview | null;
  @Output() reviews = new EventEmitter<PeerReview[]>()

  peerReviews: PeerReview[] = []
  peerReviews$: Observable<PeerReview[]> = new Observable<PeerReview[]>()

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.peerReviews$ = this.dataService.getAll<PeerReview[]>('peer_reviews')
    if (this.editor_review?.id) {
      this.peerReviews$ = this.dataService.getPeerReviews(this.editor_review.id)
      this.peerReviews$.subscribe(reviews => {
        this.peerReviews = reviews
        this.reviews.emit(this.peerReviews)
      })
    }
  }

  public async addPeerReview(user: User, message:string) {
    if (this.editor_review?.id) {
      let peerReview: PeerReview = {editor_review: this.editor_review.id, user_id: user.id, selected_for_author: false, review_logs: []}
      // we create a new PeerReview and get back its id and content_type
      let review = await lastValueFrom(this.dataService.post<PeerReview>('peer_reviews', peerReview))
      let log = await lastValueFrom(this.dataService.postPeerReviewLog(review, ReviewStatus.SENT, message))
      await lastValueFrom(this.dataService.put<any>('review_logs', `${log.id}/email_to`, {email: user.email}))
      review.review_logs.splice(0,0, log)
      this.peerReviews.push(review)
      this.reviews.emit(this.peerReviews)
    }
  }

  public async onPeerReviewMessage(review: PeerReview, value: MessageFormValue) {
    let status :ReviewStatus
    if (value.status)
      status = value.status
    else
      status = review.review_logs.at(0)!.status as ReviewStatus
    let log = await lastValueFrom(this.dataService.postPeerReviewLog(review, status, `new message\n${value.text ? value.text : ''}`))
    review.review_logs.splice(0, 0, log)
    if (review.user?.email)
      await lastValueFrom(this.dataService.put<any>('review_logs', `${log.id}/email_to`, {email: review.user.email}))
  }

  getPeerReviewStatus(review: PeerReview) {
    if (review?.review_logs && review.review_logs.length) {
      let status = review.review_logs.at(0)!.status as ReviewStatus
      return this.dataService.getStatusKey(status)
    }
    return ''
  }
}
