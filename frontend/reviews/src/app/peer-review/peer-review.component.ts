import {Component, Input, OnInit, Injector, Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {EditorReview, PeerReview, ReviewLog, ReviewStatus} from "../data-types";
import {switchMap, tap} from "rxjs/operators";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {DataService} from "../data.service";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {MessageFormValue} from "../message-form/message-form.component";
import {HttpEventType} from "@angular/common/http";

@Component({
  selector: 'app-peer-review',
  templateUrl: './peer-review.component.html',
  styleUrls: ['./peer-review.component.css']
})
export class PeerReviewComponent implements OnInit {
  peerReview$!: Observable<PeerReview>
  peerReview: PeerReview | undefined
  editorReview: EditorReview | undefined
  status: ReviewStatus | undefined;
  Status = ReviewStatus
  public modalTitle='Send message'

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dataService: DataService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.peerReview$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.dataService.getPeerReview(Number(params.get('id'))))
      , tap(items => {
        this.peerReview = items
        if (this.peerReview.review_logs.length)
          this.status = this.peerReview.review_logs.at(0)?.status as ReviewStatus
        this.dataService.getEditorReview(this.peerReview.editor_review).subscribe(
          (editorReview) => this.editorReview = editorReview
        )
      }),
    );
  }

  addLog(status: ReviewStatus, comment: string) {
    if (this.peerReview) {
      let reviewLog: ReviewLog = {
        id: 0,
        user_id:this.dataService.getUser()?.id,
        modified: new Date,
        resource: 0,
        resource_id: this.peerReview.id!,
        resource_content_type: this.peerReview.content_type!,
        order: -1,
        comment: comment,
        status: status
      }
      this.dataService.post<ReviewLog>('review_logs', reviewLog).subscribe((result) => {
        this.peerReview!.review_logs.splice(0, 0, result)
        this.dataService.put<any>('review_logs', `${result.id}/email_to`, {email: this.editorReview?.user?.email}).subscribe((sent) => {
          console.log(sent)
        })
      })
      this.status = status
    }
  }

  accept(value: MessageFormValue) {
    this.addLog(ReviewStatus.REVIEW_ACCEPTED, `Accepted request for review\n${value.text ? value.text : ''}`)
  }

  reject(value: MessageFormValue) {
    this.addLog(ReviewStatus.REJECTED, `Rejected request for review\n${value.text ? value.text : ''}`)
  }

  do(modalContent:any, fnAction:((value: MessageFormValue, modalRef:NgbModalRef) => void), title?:string) {
     // open the modalContent dialog then call the fnAction on closure (By onMessage() )
    this.modalTitle =  title ? title : 'Send message'
    let modalRef = this.modalService.open(modalContent, {size:'lg'})
    modalRef.result.then((value: MessageFormValue) => {
      fnAction.apply(this, [value, modalRef])
    },
      (reason) => {
				console.log(`Dismissed reason ${reason}`);
			}
      )
    }

  onMessage(message: MessageFormValue, modal: any) {
    if (message.text) {
      if (modal)
        modal.close(message)
    }
  }

  uploadReport(file: File) {
    if (this.peerReview) {
      // report File is a field of PearReview model.
      // so we update existing record sending FormData with File object as report as PATCH request
      const formData = new FormData()
      formData.append('report', file);
      this.dataService.patchForm('peer_reviews', this.peerReview.id!, formData).subscribe(
        (result) => {
          if (result.type == HttpEventType.Response) {
            this.peerReview = result.body;
          }
          else if (result.type==HttpEventType.UploadProgress) {
            if (result.total) {
                console.log(`UploadProgress ${100 * result.loaded / result.total}%`)
            }
          }
          console.log(result)
        }
      )
      // PUT version
      // formData.append('id', this.peerReview.id!.toString())
      // formData.append('editor_review', this.peerReview.editor_review!.toString())
      // formData.append('user_id', this.peerReview.user_id.toString())
      // this.dataService.putForm('peer_reviews', this.peerReview.id!, formData).subscribe(
      //   (result) => console.log(result)
      // )
    }
  }
  complete(message:MessageFormValue, modal:any) {
    if (message.file) {
      this.uploadReport(message.file)
    }
    this.addLog(ReviewStatus.COMPLETED, `Completed review ${message.text}`)
  }
  getFileName(url:string) {
    return url.split('/').pop()
  }
  getStatusKey(status:ReviewStatus|undefined) {
    return status ? this.dataService.getStatusKey(status): ''
  }
}
