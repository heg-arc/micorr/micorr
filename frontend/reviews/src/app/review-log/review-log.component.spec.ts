import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewLogComponent } from './review-log.component';

describe('ReviewLogComponent', () => {
  let component: ReviewLogComponent;
  let fixture: ComponentFixture<ReviewLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewLogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReviewLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
