import {Component, Input, OnInit} from '@angular/core';
import {ReviewLog, Choice} from "../data-types";
import {DataService} from "../data.service";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'app-review-log',
  templateUrl: './review-log.component.html',
  styleUrls: ['./review-log.component.css']
})
export class ReviewLogComponent implements OnInit {
  choices: Choice[] = []
  @Input() object!: ReviewLog

  constructor(private dataService: DataService) {
  }


  ngOnInit(): void {
    this.dataService.getReviewStatusChoices().then(choices => {
      this.choices = choices
    })
  }

}
