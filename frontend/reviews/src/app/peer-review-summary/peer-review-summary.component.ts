import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PeerReview, ReviewStatus} from "../data-types";
import {DataService} from "../data.service";
import {MessageFormValue} from "../message-form/message-form.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-peer-review-summary',
  templateUrl: './peer-review-summary.component.html',
  styleUrls: ['./peer-review-summary.component.css']
})
export class PeerReviewSummaryComponent implements OnInit {

  @Input() review!: PeerReview
  @Output() value = new EventEmitter<MessageFormValue>();
  public modalTitle='Send message'

  constructor(private dataService: DataService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
  }

  getStatusKey(review:PeerReview|undefined):string {
    if (review?.review_logs.length) {
      let rs:ReviewStatus = review.review_logs[0].status as ReviewStatus
      return this.dataService.getStatusKey(rs)
    }
    return 'None'
  }
  getStatusDate(review:PeerReview|undefined):any {
    if (review?.review_logs.length) {
      return review.review_logs[0].modified
    }
    if (review)
      return review.modified
  }

  do(modalContent: any, fnAction: ((value: MessageFormValue) => void), title?: string) {
    // open the modalContent dialog then call the fnAction on closure (By onMessage() )
    this.modalTitle = title ? title : 'Send message'
    this.modalService.open(modalContent, {size: 'lg'}).result.then((value: MessageFormValue) => {
      fnAction.apply(this, [value])
    })
  }

  // on messageForm submit
  onMessage(message: MessageFormValue, modal: any) {
    if (message)
      if (message.text) {
        if (modal)
          modal.close(message)
      }
  }

  message(value: MessageFormValue) {
    this.value.emit(value)
  }

  cancel(value: MessageFormValue) {
    this.value.emit({...value, status: ReviewStatus.CANCELED})
  }
}
