import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeerReviewSummaryComponent } from './peer-review-summary.component';

describe('PeerReviewSummaryComponent', () => {
  let component: PeerReviewSummaryComponent;
  let fixture: ComponentFixture<PeerReviewSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PeerReviewSummaryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PeerReviewSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
