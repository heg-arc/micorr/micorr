export const environment = {
  production: true,
  apiUrl: '/reviews/api',
  mediaUrl: '/media/original_images/'
};
